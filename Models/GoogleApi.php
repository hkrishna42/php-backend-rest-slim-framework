<?php
class GoogleApi
{
	public function cityCordinates($cityName)
	{
		$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$cityName."&key=AIzaSyAeiX3pFuqfXHwBXravPSLsmtU7ILM0Y6A";
    		$data = @file_get_contents($url);
		$jsondata = json_decode($data,true);
		$lat = $jsondata['results'][0]['geometry']['location']['lat'];
		$long = $jsondata['results'][0]['geometry']['location']['lng'];
		//$result = array('lat'=>$lat,'lng'=>$long);
		//$jsonResponse["lat"] = $lat;
		//$jsonResponse["long"] =$long;
		//echo json_encode($jsonResponse);
		$result = $lat.",".$long;
		return $result;
	}
	public function areaOfCity($cityName,$term)
	{
 		$googleApiObject = new GoogleApi;
		$latlong = $googleApiObject->cityCordinates($cityName);
		$url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=".$term."&types=geocode&location=".$latlong."&radius=500&key=AIzaSyBb-Zzu6w4AC2bbpNvmDhgMWpPFHt0kEg4";
		$data = @file_get_contents($url);
		$jsondata = json_decode($data,true);
		//$area = $jsondata['predictions'][0]['description'];
		$area = array();
		$count = count($jsondata['predictions']);
		$k = 0;
		while($k < $count)
		{
			$split = $jsondata['predictions'][$k]['description'];
			$x = split(",",$split,3);
			$area[] = $x[0];

			$k++;
		}
		return $area;
	}
}
?>
