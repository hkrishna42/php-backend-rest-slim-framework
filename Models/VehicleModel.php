<?php
class VehicleModel
{

	public function updateVehicleModel($vehicleModelId,$vehicleBrandName,$vehicleCategoryName,$vehicleModelName)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			$vehicleBrandObject = new VehicleBrand;
                        $vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);
                        $vehicleCategoryObject = new VehicleCategory;
                        $vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);

                        $updateVehicleModelQuery = mysql_query("update tblVehicleModel set vehicleBrandId = '$vehicleBrandId', vehicleCategoryId = '$vehicleCategoryId', vehicleModelName = '$vehicleModelName' where vehicleModelId = '$vehicleModelId'");
                        if($updateVehicleModelQuery)
                        {
                                return true;
                        }
			else
			{
				return false;
			}
                }
	}

	 public function selectVehicleModel()
         {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $selectVehicleModelQuery = mysql_query("select tblVehicleModel.vehicleModelId, tblVehicleModel.vehicleModelName, tblVehicleBrand.vehicleBrandName,
			tblVehicleCategory.vehicleCategoryName, tblVehicleModel.vehicleModelStatus
			from tblVehicleModel
			inner join tblVehicleBrand on tblVehicleModel.vehicleBrandId = tblVehicleBrand.vehicleBrandId
			inner join tblVehicleCategory on tblVehicleModel.vehicleCategoryId  = tblVehicleCategory.vehicleCategoryId where tblVehicleModel.vehicleModelStatus = 'Y'");
                        while($row = mysql_fetch_assoc($selectVehicleModelQuery))
                        {
                                $returnValue[] = $row;
                        }
                        return $returnValue;
                }
         }

	 public function selectVehicleModelId($vehicleModelName,$vehicleBrandId)
         {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $selectVehicleModelQuery = mysql_query("select vehicleModelId from tblVehicleModel where
			vehicleModelName = '$vehicleModelName' AND vehicleBrandId = '$vehicleBrandId'");
                        $row = mysql_fetch_array($selectVehicleModelQuery);
                        $returnValue = $row[0];
			return $returnValue;
                }
         }

        public function selectVehicleModelForVehicleBrand($vehicleBrandName)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> dataBaseConnection();
                if($connection)
                {
                        $vehicleBrandObject = new VehicleBrand;
                        $vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);
                        $selectVehicleModelQuery = mysql_query("select vehicleModelName,vehicleModelId from tblVehicleModel where vehicleBrandId = '$vehicleBrandId' and vehicleModelStatus = 'Y'");
                        while($row = mysql_fetch_assoc($selectVehicleModelQuery))
                        {
                                $returnValue[] = $row;
                        }
                        return $returnValue;
                }
        }

	public function insertVehicleModel($vehicleBrandId,$vehicleCategoryName,$vehicleModelName)
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> dataBaseConnection();
		if($connection)
		{
			//$vehicleBrandObject = new VehicleBrand;
			//$vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);

			$vehicleCategoryObject = new VehicleCategory;
			$vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);

			$insertVehicleModelQuery = mysql_query("insert into tblVehicleModel (vehicleModelName,vehicleBrandId,vehicleCategoryId,vehicleModelStatus) 
			values ('$vehicleModelName','$vehicleBrandId','$vehicleCategoryId','Y')");

			if($insertVehicleModelQuery)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function updateVehicleModeltatus($vehicleModelId,$vehicleModelStatus)
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> dataBaseConnection();
		if($connection)
		{
			$deactivateVehicleModelQuery = mysql_query("update tblVehicleModel set vehicleModelStatus = '$vehicleModelStatus' where vehicleModelId = '$vehicleModelId'");

			if($deactivateVehicleModelQuery)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function selectVehicleModelByKey($key)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                $data = array();
                if($connection)
                {
                        $selectVehicleModelQuery = mysql_query("select tblVehicleModel.vehicleModelName,tblVehicleModel.vehicleModelId,tblVehicleBrand.vehicleBrandName,
			tblVehicleCategory.vehicleCategoryName from tblVehicleModel inner join tblVehicleBrand on tblVehicleModel.vehicleBrandId = tblVehicleBrand.vehicleBrandId
			inner join tblVehicleCategory on tblVehicleModel.vehicleCategoryId = tblVehicleCategory.vehicleCategoryId where tblVehicleModel.vehicleModelName Like '{$key}%'");
                        while($row = mysql_fetch_assoc($selectVehicleModelQuery))
                        {
                                $data[]=$row;
                        }
                }
                return $data;
        }
}
?>
