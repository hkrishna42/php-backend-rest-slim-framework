<?php
class ServiceCentre
{

	public function getLatLong($address)
	{
		if(!empty($address))
		{
			//Formatted address
        		$formattedAddr = str_replace(' ','+',$address);
        		//Send request and receive json data by address
        		$geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
        		$output = json_decode($geocodeFromAddr);
        		//Get latitude and longitute from json data
        		$data['latitude']  = $output->results[0]->geometry->location->lat; 
        		$data['longitude'] = $output->results[0]->geometry->location->lng;
        		//Return latitude and longitude of the given address
        		if(!empty($data))
			{
        			return $data;
        		}
			else
			{
        			return false;
        		}
		}
		else
		{
    			return false;   
		}
	}

	public function updateServiceCentreStatus($serviceCentreId,$serviceCentreStatus)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $updateServiceCentreQuery = mysql_query("update tblServiceCentre set serviceCentreStatus = '$serviceCentreStatus' where serviceCentreId = '$serviceCentreId'");
                        if($updateServiceCentreQuery)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
        }

	 public function updateDeactiveServiceCentreStatus($serviceCentreId,$serviceCentreStatus)
         {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $updateServiceCentreQuery = mysql_query("update tblServiceCentre set serviceCentreStatus = '$serviceCentreStatus' where serviceCentreId = '$serviceCentreId'");
                        if($updateServiceCentreQuery)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
         }
	public function selectServiceCentre()
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> databaseConnection();
		if($connection)
		{
			$jsonResponse = array();

			$selectServiceCentreQuery = mysql_query("select tblServiceCentre.serviceCentreId,tblServiceCentre.serviceCentreName,tblServiceCentre.serviceCentreContact,
			tblServiceCentre.serviceCentreEmail,tblServiceCentre.serviceCentreAddress,tblServiceCentre.serviceCentreLat,tblServiceCentre.serviceCentreLong,tblServiceCentre.serviceCentrePincode,tblServiceCentre.serviceCentreLandmark,tblServiceCentre.serviceCentreSpeciality,tblServiceCentre.serviceCentreStatus,tblCity.cityName,tblArea.areaName
			From tblServiceCentre
			inner join tblCity on tblServiceCentre.cityId = tblCity.cityId
			inner join tblArea on tblServiceCentre.areaId = tblArea.areaId");

			while($row = mysql_fetch_assoc($selectServiceCentreQuery))
			{
				$jsonResponse[] = $row;
			}
			return $jsonResponse;
		}
	}


	public function selectDeactiveServiceCentre()
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $jsonResponse = array();

                        $selectServiceCentreQuery = mysql_query("select tblServiceCentre.serviceCentreId,tblServiceCentre.serviceCentreName,tblServiceCentre.serviceCentreContact,
                        tblServiceCentre.serviceCentreEmail,tblServiceCentre.serviceCentreAddress,tblServiceCentre.serviceCentreLat,tblServiceCentre.serviceCentreLong,tblServiceCentre.serviceCentrePincode,tblServiceCentre.serviceCentreLandmark,tblServiceCentre.serviceCentreSpeciality,tblServiceCentre.serviceCentreStatus,tblCity.cityName,tblArea.areaName
                        From tblServiceCentre
                        inner join tblCity on tblServiceCentre.cityId = tblCity.cityId
                        inner join tblArea on tblServiceCentre.areaId = tblArea.areaId
			where tblServiceCentre.serviceCentreStatus = 'N'");

                        while($row = mysql_fetch_assoc($selectServiceCentreQuery))
                        {
                                $jsonResponse[] = $row;
                        }
                        return $jsonResponse;
                }
        }


	public function selectServiceCentreId($serviceCentreContact)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $selectServiceCentreIdQuery = mysql_query("select serviceCentreId from tblServiceCentre where serviceCentreContact = '$serviceCentreContact' and serviceCentreStatus = 'Y'");
                        $row = mysql_fetch_array($selectServiceCentreIdQuery);
                        $returnValue = $row[0];
                	return $returnValue;
                }

        }

	public function checkServiceCentre($serviceCentreName,$serviceCentreContact)
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> databaseConnection();

		if($connection)
		{
			$selectServiceCentre = mysql_query("select serviceCentreId from tblServiceCentre where serviceCentreName = '$serviceCentreName' and serviceCentreContact = '$serviceCentreContact'");
			$row = mysql_fetch_array($selectServiceCentre);
			$serviceCentreId = $row[0];

			if($serviceCentreId)
			{
                        	$otp = mt_rand(100000,999999);
                        	$insertOtp = mysql_query("update tblServiceCentre set serviceCentreOtp = '$otp' where serviceCentreId = '$serviceCentreId'");

	                        if($insertOtp)
				{
					$registerObj = new Register;
					$sendSms = $registerObj -> requestSms($otp,$serviceCentreContact);
					if($sendSms)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public function checkServiceCentreOtp($loginOtp,$serviceCentreContact)
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> databaseConnection();

		if($connection)
		{
			$getOtp = mysql_query("select serviceCentreOtp from tblServiceCentre where serviceCentreContact = '$serviceCentreContact'");
			$row = mysql_fetch_array($getOtp);
			$otpFromDb = $row[0];

			if($otpFromDb == $loginOtp)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function selectServiceCentresByArea($cityName,$areaName,$makeName,$modelName)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			$jsonResponse = array();
			$selectBrandIdObject = new VehicleBrand;
			$selectBrandId = $selectBrandIdObject -> selectVehicleBrandId($makeName);

			$selectVehicleModelIdObject = new VehicleModel;
			$selectVehicleModelId = $selectVehicleModelIdObject -> selectVehicleModelId($modelName,$selectBrandId);

			$selectVehicleCategoryObject = new VehicleCategory;
			$vehicleCategoryId = $selectVehicleCategoryObject -> selectVehicleCategoryIdByModelId($selectVehicleModelId);

			$selectServiceCentresQuery = mysql_query("select distinct(serviceCentreId) from tblServicesAtServiceCentre where vehicleCategoryId =' $vehicleCategoryId'");
			$jsonResponse = array();
			while($row = mysql_fetch_assoc($selectServiceCentresQuery))
			{
				$jsonResponse[] = $row;
			}

			$selectCityIdObject = new City;
			$selectCityId = $selectCityIdObject -> selectCityId($cityName);

			$selectAreaIdObject = new Area;
                        $selectAreaId = $selectAreaIdObject -> selectAreaId($selectCityId,$areaName);

			$jsonResponse1 = array();
			foreach($jsonResponse as $key => $obj)
			{
				$serviceCentreId = $obj['serviceCentreId'];
				$selectServiceCentreNamesQuery = mysql_query("select serviceCentreName,serviceCentreAddress,serviceCentreContact,serviceCentreLat,serviceCentreLong from tblServiceCentre where cityId = '$selectCityId' and areaId = '$selectAreaId' and serviceCentreId = '$serviceCentreId' and serviceCentreStatus = 'Y'");
				while($row1 = mysql_fetch_assoc($selectServiceCentreNamesQuery))
				{
					$jsonResponse1[] = $row1;
				}
			}
			return $jsonResponse1;
		}
	}

	public function selectServiceCentresByAreaForPuncture($cityName,$areaName,$makeName,$modelName)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $selectBrandIdObject = new VehicleBrand;
                        $selectBrandId = $selectBrandIdObject -> selectVehicleBrandId($makeName);

                        $selectVehicleModelIdObject = new VehicleModel;
                        $selectVehicleModelId = $selectVehicleModelIdObject -> selectVehicleModelId($modelName,$selectBrandId);

			$selectVehicleCategoryObject = new VehicleCategory;
                        $vehicleCategoryId = $selectVehicleCategoryObject -> selectVehicleCategoryIdByModelId($selectVehicleModelId);

			$serviceName = "Puncture";
			$servicesObject = new Services;
			$serviceId = $servicesObject -> selectServicesId($serviceName);

                        $selectServiceCentresQuery = mysql_query("select serviceCentreId,servicesAtServiceCentrePrice from tblServicesAtServiceCentre where vehicleCategoryId =' $vehicleCategoryId' and serviceId = '$serviceId'");
                        $jsonResponse = array();
                        while($row = mysql_fetch_assoc($selectServiceCentresQuery))
                        {
                                $jsonResponse[] = $row;
                        }

                        $selectCityIdObject = new City;
                        $selectCityId = $selectCityIdObject -> selectCityId($cityName);

                        $selectAreaIdObject = new Area;
                        $selectAreaId = $selectAreaIdObject -> selectAreaId($selectCityId,$areaName);

                        $jsonResponse1 = array();
                        foreach($jsonResponse as $key => $obj)
			{
                                $serviceCentreId = $obj['serviceCentreId'];
				$serviceCentrePrice = $obj['servicesAtServiceCentrePrice'];
                                $selectServiceCentreNamesQuery = mysql_query("select serviceCentreName,serviceCentreAddress,serviceCentreContact,serviceCentreLat,serviceCentreLong from tblServiceCentre where areaId = '$selectAreaId' and serviceCentreId = '$serviceCentreId' and serviceCentreStatus ='Y'");
                                while($row1 = mysql_fetch_assoc($selectServiceCentreNamesQuery))
                                {
                                        $jsonResponse1[] = array("serviceCentreName" => $row1['serviceCentreName'],"serviceCentreAddress" => $row1['serviceCentreAddress'],"serviceCentreContact" => $row1['serviceCentreContact'],"serviceCentreLat" => $row1['serviceCentreLat'],"serviceCentreLong" => $row1['serviceCentreLong'],"serviceCentrePrice" => $serviceCentrePrice);
                                }
                        }
			return $jsonResponse1;
                }
        }

	public function selectServiceCentresByAreaPackage($cityName,$areaName)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			$selectCityIdObject = new City;
                        $selectCityId = $selectCityIdObject -> selectCityId($cityName);

                        $selectAreaIdObject = new Area;
                        $selectAreaId = $selectAreaIdObject -> selectAreaId($selectCityId,$areaName);
			$jsonResponse = array();
			$selectServiceCentreNamesQuery = mysql_query("select serviceCentreName,serviceCentreAddress,serviceCentreContact,serviceCentreLat,serviceCentreLong from tblServiceCentre where cityId = '$selectCityId' and areaId = '$selectAreaId' and serviceCentreStatus ='Y'");
                        while($row = mysql_fetch_assoc($selectServiceCentreNamesQuery))
                        {
  	                      $jsonResponse[] = $row;
                        }
			return $jsonResponse;
		}
	}
	public function selectServiceCentreContactFromTransactionServiceId($transactionServiceId)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			//$serviceCentreDetails = array();
                        $selectUserIdQuery = mysql_query("select serviceCentreId from tblTransactionService where transactionServiceId = '$transactionServiceId'");
                        $row = mysql_fetch_array($selectUserIdQuery);
                        $serviceCentreId = $row[0];
                        $selectUserContactQuery = mysql_query("select serviceCentreContact,serviceCentreName from tblServiceCentre where serviceCentreId = '$serviceCentreId'");
                        $row1 = mysql_fetch_array($selectUserContactQuery);
                        $serviceCentreDetails["serviceCentreContact"] = $row1[0];
			$serviceCentreDetails["serviceCentreName"] = $row1[1];
                        return $serviceCentreDetails;
                }

        }
	public function insertServiceCentre($cityId,$areaId,$serviceCentreContact,$serviceCentreName,$serviceCentreEmail,$serviceCentreAddress,$serviceCentreLandmark,$serviceCentrePincode,$serviceCentreLat,$serviceCentreLong,$serviceCentreSpeciality,$serviceCentreStatus)
	{

		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        //$cityObject = new City;
                        //$cityId = $cityObject -> selectCityId($cityName);
                        $insertServiceCentreQuery = mysql_query("insert into tblServiceCentre
			(cityId,areaId,serviceCentreContact,serviceCentreName,serviceCentreEmail,serviceCentreAddress,serviceCentreLandmark,serviceCentrePincode,serviceCentreLat,serviceCentreLong,serviceCentreSpeciality,serviceCentreStatus) 
			values ('$cityId','$areaId','$serviceCentreContact','$serviceCentreName','$serviceCentreEmail','$serviceCentreAddress','$serviceCentreLandmark','$serviceCentrePincode','$serviceCentreLat','$serviceCentreLong','$serviceCentreSpeciality','$serviceCentreStatus')");
                        if($insertServiceCentreQuery)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
	}
	public function updateServiceCentre($cityId,$areaId,$serviceCentreContact,$serviceCentreName,$serviceCentreEmail,$serviceCentreAddress,$serviceCentreLandmark,$serviceCentrePincode,$serviceCentreLat,$serviceCentreLong,$serviceCentreSpeciality,$serviceCentreId)
	{
		$databaseObject = new DbConnection();
		$connection =$databaseObject->databaseConnection();
		if($connection)
		{
			$updateServiceCentreQuery = mysql_query("update tblServiceCentre set cityId='$cityId',areaId='$areaId',serviceCentreContact='$serviceCentreContact',
			serviceCentreName='$serviceCentreName',serviceCentreEmail='$serviceCentreEmail',serviceCentreAddress='$serviceCentreAddress',
			serviceCentreLandmark='$serviceCentreLandmark',serviceCentrePincode='$serviceCentrePincode',serviceCentreLat='$serviceCentreLat',
			serviceCentreLong='$serviceCentreLong',serviceCentreSpeciality='$serviceCentreSpeciality' where serviceCentreId='$serviceCentreId'");
			if($updateServiceCentreQuery)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

}
?>
