<?php
class BookingDetails
{
	public function selectBookingDetails($serviceCentreId)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
		$returnValue = array();
                if($connection)
                {
                        $selectBookingDetailsQuery = mysql_query("select * from viewBookingDetail where serviceCentreId = '$serviceCentreId' and transactionServiceStatus = 'N'");
                        while($row = mysql_fetch_assoc($selectBookingDetailsQuery))
			{
                        	$returnValue[] = $row;
			}
                        return $returnValue;
                }
		mysql_close();
	}

	public function selectConfirmedBookingDetails($serviceCentreId)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                $returnValue = array();
                if($connection)
                {
			$selectBookingDetailsQuery = mysql_query("select * from viewBookingDetail where str_to_date(transactionServiceBookingDate, '%Y/%m/%d') >= curdate() and serviceCentreId = '$serviceCentreId' and transactionServiceStatus = 'Y'");
                        while($row = mysql_fetch_assoc($selectBookingDetailsQuery))
                        {
                                $returnValue[] = $row;
                        }
                        return $returnValue;
                }
                mysql_close();
        }

	public function selectServiceBooking($transactionId)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
		$serviceArray = array();
		if($connection)
		{
			$selectServiceIdQuery= mysql_query("select serviceId from tblServiceBooking where transactionServiceId = '$transactionId'");
			while($row = mysql_fetch_assoc($selectServiceIdQuery))
			{
				$serviceId = $row['serviceId'];
				$selectServiceNameQuery = mysql_query("select serviceName from tblService where serviceId = '$serviceId'");
				while($row1 = mysql_fetch_assoc($selectServiceNameQuery))
				{
					$serviceArray[] = $row1;
				}
			}

                        $selectServiceCentreIdQuery = mysql_query("select serviceCentreId from tblTransactionService where transactionServiceId = '$transactionId'");
                        $row1 = mysql_fetch_array($selectServiceCentreIdQuery);
                        $serviceCentreId = $row1[0];

                        $selectServiceCentreDetailsQuery = mysql_query("select serviceCentreName,serviceCentreAddress,serviceCentreContact from tblServiceCentre where serviceCentreId = '$serviceCentreId'");
                        $serviceCentreDetails = mysql_fetch_array($selectServiceCentreDetailsQuery);
                        $serviceCentreName = $serviceCentreDetails[0];
                        $serviceCentreAddress = $serviceCentreDetails[1];
                        $serviceCentreContact = $serviceCentreDetails[2];

                        $finalServiceArray = array("serviceName" => $serviceArray, "serviceCentreName" => $serviceCentreName, "serviceCentreAddress" => $serviceCentreAddress, "serviceCentreContact" => $serviceCentreContact);
                        return $finalServiceArray;
			//return $serviceCentreName;
		}
	}

	public function selectUserBookingDetails($userId)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
		$returnValue = array();
                if($connection)
                {
                        $selectBookingDetailsQuery = mysql_query("select * from viewBookingDetail where str_to_date(transactionServiceBookingDate, '%Y/%m/%d') >= curdate() and userId = '$userId' and transactionServiceStatus = 'Y'");
                        while($row = mysql_fetch_assoc($selectBookingDetailsQuery))
                        {
                                $returnValue[] = $row;
                        }
                        return $returnValue;
                }
                mysql_close();
        }
}

?>
