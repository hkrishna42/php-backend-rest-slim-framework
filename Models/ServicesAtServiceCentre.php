<?php
class ServicesAtServiceCentre
{
        public function insertServicesAtServiceCentre($jsonValueServices,$vehicleCategoryName,$serviceCentreContact)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $vehicleCategoryObject = new VehicleCategory;
                        $vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);

			$serviceCentreObject = new ServiceCentre;
                        $serviceCentreId = $serviceCentreObject -> selectServiceCentreId($serviceCentreContact);

			$serviceObject = new Services;
			$insertServicesAtServiceCentreQuery;

			$jsonValue = array();
			$jsonValue = json_decode($jsonValueServices,true);

			foreach($jsonValue['addService'] as $key => $obj)
			{
				$serviceName = $obj['serviceName'];
				$serviceId = $serviceObject -> selectServicesId($serviceName);
				$servicePrice = $obj['servicePrice'];

				$searchService = mysql_query("select * from tblServicesAtServiceCentre where serviceId='$serviceId' and vehicleCategoryId = '$vehicleCategoryId' and serviceCentreId = '$serviceCentreId'");
				$result = mysql_fetch_array($searchService);

				if(!$result)
				{
					$insertServicesAtServiceCentreQuery = mysql_query("insert into tblServicesAtServiceCentre(vehicleCategoryId,serviceId,serviceCentreId,servicesAtServiceCentrePrice,status) values ('$vehicleCategoryId','$serviceId','$serviceCentreId','$servicePrice','Y')");
				}
				else
				{
					$insertServicesAtServiceCentreQuery = "";
				}
			}

                        if($insertServicesAtServiceCentreQuery)
                        {
                                //$returnValue["success"] = true;
                                return true;
                        }
                        else
                        {
                                //$returnValue["success"] = false;
                                return false;
                        }
                }
        }

	public function updateServices($jsonValueEncoaded,$serviceCentreContact)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $serviceCentreObj = new ServiceCentre;
                        $serviceCentreId = $serviceCentreObj -> selectServiceCentreId($serviceCentreContact);
                        $updatePrices = "";
                        $jsonValue = json_decode($jsonValueEncoaded,true);

                        foreach($jsonValue['updateService'] as $key => $obj)
                        {
                                $servicesAtServiceCentrePrice = $obj['servicePrice'];
                                $vehicleCategoryName = $obj['serviceCategory'];
                                $serviceName = $obj['serviceName'];

                                $vehicleCategoryObject = new VehicleCategory;
                                $vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);

                                $servicesObj = new Services;
                                $serviceId = $servicesObj -> selectServicesId($serviceName);

                                $servicePrice = mysql_query("select servicesAtServiceCentrePrice from tblServicesAtServiceCentre where
                                serviceCentreId = '$serviceCentreId' and vehicleCategoryId = '$vehicleCategoryId' and serviceId = '$serviceId'");
                                $row = mysql_fetch_array($servicePrice);
                                $servicePriceFromDB = $row[0];

                                if($servicesAtServiceCentrePrice != $servicePriceFromDB)
                                {
                                        $checkPrice = mysql_query("select updateServiceServicePrice from tblUpdatedServicePrices where
                                        updateServiceServiceCentreId = '$serviceCentreId' and updateServiceCategoryId = '$vehicleCategoryId' and
                                        updateServiceServiceId = '$serviceId'");
                                        $row1 = mysql_fetch_array($checkPrice);
                                        $comparePrice = $row1[0];

                                        if(!$comparePrice)
                                        {
                                                $updatePrices = mysql_query("insert into tblUpdatedServicePrices (updateServiceCategoryId,updateServiceServiceId,
                                                updateServiceServiceCentreId,updateServiceServicePrice) values ('$vehicleCategoryId',
                                                '$serviceId','$serviceCentreId','$servicesAtServiceCentrePrice')");
                                        }
                                }
                        }
                        if($updatePrices)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
        }

	public function getServicesAtServiceCentre($serviceCentreContact)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			$result = array();
			$serviceName = array();
			$servicePrice = array();
                        $getServicesQuery = mysql_query("SELECT tblService.serviceName,
tblServicesAtServiceCentre.servicesAtServiceCentrePrice,
tblVehicleCategory.vehicleCategoryName
FROM tblServicesAtServiceCentre
INNER JOIN tblService ON tblServicesAtServiceCentre.serviceId = tblService.serviceId
INNER JOIN tblServiceCentre ON tblServicesAtServiceCentre.serviceCentreId = tblServiceCentre.serviceCentreId
INNER JOIN tblVehicleCategory ON tblVehicleCategory.vehicleCategoryId = tblServicesAtServiceCentre.vehicleCategoryId
WHERE tblServiceCentre.serviceCentreContact = '$serviceCentreContact'");
			while($row = mysql_fetch_assoc($getServicesQuery))
                        {
				$result[] = $row;
                        }
                        return $result;
		}
	}

	public function getServicesAtServiceCentreAddOn($serviceCentreContact,$vehicleModelName,$transactionServiceId)
	{
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $serviceName = array();
                        $servicePrice = array();
			$availedServices = array();
			$result = array();
			$finalServices = array();

			$getAvailedServices = mysql_query("select tblService.serviceName from tblServiceBooking inner join tblService on tblServiceBooking.serviceId = tblService.serviceId where transactionServiceId = '$transactionServiceId'");
			while($row = mysql_fetch_assoc($getAvailedServices))
			{
				$availedServices[] = $row;
			}

			$categoryIdQuery = mysql_query("select vehicleCategoryId from tblVehicleModel where vehicleModelName = '$vehicleModelName'");
			$row = mysql_fetch_array($categoryIdQuery);
			$vehicleCategoryId = $row[0];

                        $getServicesQuery = mysql_query("SELECT tblService.serviceName,tblServicesAtServiceCentre.servicesAtServiceCentrePrice,
tblVehicleCategory.vehicleCategoryName 
FROM tblServicesAtServiceCentre 
INNER JOIN tblService ON tblServicesAtServiceCentre.serviceId = tblService.serviceId 
INNER JOIN tblServiceCentre ON tblServicesAtServiceCentre.serviceCentreId = tblServiceCentre.serviceCentreId 
INNER JOIN tblVehicleCategory ON tblVehicleCategory.vehicleCategoryId = tblServicesAtServiceCentre.vehicleCategoryId 
WHERE tblServiceCentre.serviceCentreContact = '$serviceCentreContact' and tblServicesAtServiceCentre.vehicleCategoryId = '$vehicleCategoryId'");

                        while($row2 = mysql_fetch_assoc($getServicesQuery))
                        {
                                $result[] = $row2;
                        }
                        //return $result;
			foreach($result as $key => $obj)
			{
				foreach($availedServices as $key1 => $obj1)
				{
					if($obj['serviceName'] != $obj1['serviceName'])
					{
						$finalServices[] = array("serviceName" => $obj['serviceName'], "servicesAtServiceCentrePrice" => $obj['servicesAtServiceCentrePrice'], "vehicleCategoryName" => $obj['vehicleCategoryName']);
					}
				}
			}
			return $finalServices;
                }
        }

        public function selectServicesAtServiceCentre($jsonValueServices)
	{
		$data = json_decode($jsonValueServices,true);
                $vehicleBrandName = $data['make'];
                $vehicleModelName = $data['model'];
                $cityName = $data['city'];
                $areaName = $data['area'];

                $cityObject = new City;
                $cityId = $cityObject -> selectCityId($cityName);

                $areaObject = new Area;
                $areaId = $areaObject -> selectAreaId($cityId,$areaName);

                $vehicleBrandObject = new VehicleBrand;
                $vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);

                $vehicleModelObject = new VehicleModel;
                $vehicleCategoryId = $vehicleModelObject -> selectVehicleModelId($vehicleModelName,$vehicleBrandId);

                $serviceObject = new Services;
                $intersect = array();
                $servicesPrice = array();
                $serviceCentrePrice = array();
                $servicesName = array();
                foreach($data['serviceName'] as $service)
                {
                        //$serviceObject = new Services;
                        $serviceCentreId = array();
                        $serviceId = $serviceObject -> selectServicesId($service);
                        $query=mysql_query("select serviceCentreId,servicesAtServiceCentrePrice from
                        tblServicesAtServiceCentre where vehicleCategoryId =
                        '$vehicleCategoryId' and serviceId = '$serviceId'");
                        if(mysql_num_rows($query))
                        {
                                while($row = mysql_fetch_assoc($query))
                                {
                                        $serviceCentreId[] = $row['serviceCentreId'];
                                        $serviceCentrePrice[] = $row['servicesAtServiceCentrePrice'];
                                        //$serviceName[] = $service;
                                }
			}
			if(empty($serviceCentreId)){$intersect[] = "-1";}
                        else
                        {
                                if(!empty($intersect))
                                {
                                        $intersect = array_intersect($intersect,$serviceCentreId);
                                        //echo json_encode($intersect);
                                }
                                else
                                {
                                        $intersect = $serviceCentreId;
                                }
                        }
//			return $intersect;
                }
return $intersect;

		/*$newServiceCentreId = array();
		$resultNew = mysql_query("select serviceCentreId from tblServiceCentre where areaId = '$areaId'");
		while($rowNew = mysql_fetch_assoc($resultNew))
		{
			$newServiceCentreId[] = $rowNew['serviceCentreId'];
		}

		$intersectNew = array_intersect($newServiceCentreId,$intersect);

		$serviceCentreDetails = array();
		foreach($intersectNew as $serviceCentreIdNew)
		{
			$result1 = mysql_query("select serviceCentreName,
			serviceCentreAddress,serviceCentreContact,
			serviceCentreLat,serviceCentreLong
			from tblServiceCentre where serviceCentreId = '$serviceCentreIdNew'");

			while($row1 = mysql_fetch_assoc($result1))
			{
				$serviceCentreDetails[] = $row1;
			}

			foreach($data['serviceName'] as $service)
			{
				$servicesName[] = $service;
			}
		}

		$servicesPrice = array();
		foreach($intersectNew as $serviceCentreIdNew)
		{
			foreach($data['serviceName'] as $service)
			{
				$serviceId = $serviceObject -> selectServicesId($service);
				$result = mysql_query("select servicesAtServiceCentrePrice
				from tblServicesAtServiceCentre
				where serviceCentreId = '$serviceCentreIdNew'
				and serviceId = '$serviceId'");
				$row = mysql_fetch_array($result);

				$servicesPrice[] = $row[0];
			}
		}


		$finalResponse = array();
		$finalResponse = array("serviceCentreDetails" => $serviceCentreDetails, "services" => $servicesName, "servicesPrice" => $servicesPrice);*/
		//$finalResponse[] = array("servicePrice" => $serviceCentrePrice['servicesAtServiceCentrePrice'],"serviceCentreName" => $serviceCentreDetails['serviceCentreName'],"serviceCentreAddress" => $serviceCentreDetails['serviceCentreAddress'],"serviceCentreContact" => $serviceCentreDetails['serviceCentreContact'],"serviceCentreLat" => $serviceCentreDetails['serviceCentreLat'],"serviceCentreLong" => $serviceCentreDetails['serviceCentreLong'],"serviceCentrePrice" => $servicePrice);
	//	return $intersect;
		//return $finalResponse;
	}
}
?>
