<?php
class ServiceBooking
{
	public function insertServiceBooking($userContact,$userAddress,$userLandmark,$userPincode,$userMake,$userModel,$userVehicleNumber,$userMode,$userBookingDate,$userTodayDate,$serviceCentreName,$serviceCentreAddress,$serviceCentreContact,$servicesJsonValue,$packageName,$packagePrice,$tag)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
			$selectBrandIdObject = new VehicleBrand;
                        $selectBrandId = $selectBrandIdObject -> selectVehicleBrandId($userMake);

                        $selectVehicleModelIdObject = new VehicleModel;
                        $selectVehicleModelId = $selectVehicleModelIdObject -> selectVehicleModelId($userModel,$selectBrandId);

			$selectUserIdObject = new User;
			$selectUserId = $selectUserIdObject -> selectUserId($userContact);

			if($tag == "service")
			{
				$serviceCentreObject = new ServiceCentre;
				$serviceCentreId = $serviceCentreObject -> selectServiceCentreId($serviceCentreContact);
				$data = json_decode($servicesJsonValue,true);

				$updateUserDetailsQuery = mysql_query("update tblUser set userAddress = '$userAddress',userLandmark = '$userLandmark',userPincode = '$userPincode' where userId = '$selectUserId'");
				if($updateUserDetailsQuery)
				{
					$insertTransactionDetailsQuery = mysql_query("insert into tblTransactionService (transactionServiceDate,transactionServiceBookingDate,transactionServiceType,vehicleModelId,transactionServiceVehicleNo,serviceCentreId,userId,transactionServiceStatus) values ('$userTodayDate','$userBookingDate','$userMode','$selectVehicleModelId','$userVehicleNumber','$serviceCentreId','$selectUserId','N')");
					if($insertTransactionDetailsQuery)
					{
						$selectTransactionIdObject = new Transaction;
		                                $selectTransactionId = $selectTransactionIdObject -> selectTransactionId($selectUserId);

						$insertBookingQuery;
						$serviceObject = new Services;
						foreach($data['serviceName'] as $service)
                                		{
		                                        $serviceId = $serviceObject -> selectServicesId($service);
                		                        $insertBookingQuery = mysql_query("insert into tblServiceBooking (serviceId,transactionServiceId) values ('$serviceId','$selectTransactionId')");
                                		}
						if($insertBookingQuery)
        		                        {
	                	                        return true;
                                		}
		                                else
                		                {
                                		        return false;
		                                }
					}
				}
			}
		}


	}
	public function updateTransactionService($employeeId,$transactionServiceId,$transactionServicePickUpTime,$transactionServiceDropTime)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
		if($connection)
		{
			$updateTransactionService = mysql_query("update tblTransactionService set employeeId = '$employeeId',
			transactionServicePickUpTime = '$transactionServicePickUpTime',
			transactionServiceDropTime = '$transactionServiceDropTime',
			transactionServiceStatus = 'Y' 
			where transactionServiceId = '$transactionServiceId'");
			if($updateTransactionService)
			{
				$userObject = new User;
				$userContact = $userObject -> selectUserContactFromTransactionServiceId($transactionServiceId);
				$serviceCentreObject = new ServiceCentre;
				$serviceCentreDetails = $serviceCentreObject -> selectServiceCentreContactFromTransactionServiceId($transactionServiceId);
				$serviceCentreContact = $serviceCentreDetails["serviceCentreContact"];
				$serviceCentreName = $serviceCentreDetails["serviceCentreName"];
				$code = mt_rand(10000,99999);

				if($serviceCentreName)
				{
					$authKey = "132726A7Vsc6ILVpw58416009";

	                                //Multiple mobiles numbers separated by comma
	                                //Sender ID,While using route4 sender id should be 6 characters long.
	                                $senderId = "DCHAKI";

	                                $userMessage = "Booking confirmed by ".$serviceCentreName."(".$serviceCentreContact.").Employee code : ".$code." Please verify the code verbally with employee for employee authentication.";
        	                        //Your message to send, Add URL encoding here.
	                                $message = urlencode("$userMessage");

        	                        //Define route
                	                $route = "4";
                        	        //Prepare you post parameters
                                	$postData = array(
                                        	'authkey' => $authKey,
	                                        'mobiles' => $userContact,
        	                                'message' => $message,
                	                        'sender' => $senderId,
                        	                'route' => $route
                                	);

	                                //API URL
        	                        $url="https://control.msg91.com/api/sendhttp.php";

                        	        // init the resource
                	                $ch = curl_init();
                                	curl_setopt_array($ch, array(
                                        	CURLOPT_URL => $url,
	                                        CURLOPT_RETURNTRANSFER => true,
        	                                CURLOPT_POST => true,
                	                        CURLOPT_POSTFIELDS => $postData
                        	                //,CURLOPT_FOLLOWLOCATION => true
                                	));

	                                //Ignore SSL certificate verification
        	                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	                                //get response
        	                        $output = curl_exec($ch);
					curl_close($ch);

                        	        if($output)
	                                {
        	                                return true;
                	                }
                        	        else
                                	{
                                        	return false;
	                                }
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public function insertIntoServiceBooking($serviceId,$transactionServiceId)
	{
		$databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
		if($connection)
		{
			$insertIntoServiceBookingQuery = mysql_query("insert into tblServiceBooking (serviceId,transactionServiceId)
			values ('$serviceId','$transactionServiceId')");
			if($insertIntoServiceBookingQuery)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
?>
