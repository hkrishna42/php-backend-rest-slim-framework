<?php
class RequestedService
{
	public function addService($serviceCentreContact,$serviceName,$servicePrice,$vehicleCategoryName)
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> databaseConnection();

		if($connection)
		{
			$serviceCentreObject = new ServiceCentre;
			$serviceCentreId = $serviceCentreObject -> selectServiceCentreId($serviceCentreContact);

			$vehicleCategoryObject = new VehicleCategory;
			$vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);

			$insertService = mysql_query("insert into tblService(serviceName,serviceStatus) values ('$serviceName','N')");
			if($insertService)
			{
				$servicesObject = new Services;
				$serviceId = $servicesObject -> selectServicesId($serviceName);

				$insertServiceInRequestedService = mysql_query("insert into tblRequestedService(vehicleCategoryId,serviceId,serviceCentreId,servicePrice) values ('$vehicleCategoryId','$serviceId','$serviceCentreId','$servicePrice')");

				if($insertServiceInRequestedService)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public function showRequestedServices()
	{
		$databaseObject = new DbConnection();
		$connection = $databaseObject -> databaseConnection();

		$selectRequestedServicesQuery = mysql_query("select tblService.serviceName,tblServiceCentre.serviceCentreName,tblServiceCentre.serviceCentreContact,
		tblVehicleCategory.vehicleCategoryName,tblRequestedService.servicePrice,tblRequestedService.requestedServiceId
		from tblRequestedService
		inner join tblService on tblRequestedService.serviceId = tblService.serviceId
		inner join tblServiceCentre on tblRequestedService.serviceCentreId = tblServiceCentre.serviceCentreId
		inner join tblVehicleCategory on tblRequestedService.vehicleCategoryId = tblVehicleCategory.vehicleCategoryId");

		$jsonResponse = array();
		while($row = mysql_fetch_assoc($selectRequestedServicesQuery))
		{
			$jsonResponse[] = $row;
		}
		return $jsonResponse;
	}
}
?>
