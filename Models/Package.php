<?php
class Package
{
	public function selectPackage($vehicleBrandName,$vehicleModelName)
	{
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();

		if($connection)
		{
			$vehicleBrandObject = new VehicleBrand;
			$vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);

			$vehicleModelObject = new VehicleModel;
			$vehicleCategoryId = $vehicleModelObject -> selectVehicleModelId($vehicleModelName,$vehicleBrandId);

			$result = array();
			$selectPackageQuery = mysql_query("select * from tblPackage where categoryId = '$vehicleCategoryId'");
			while($row = mysql_fetch_assoc($selectPackageQuery))
			{
				$result[] = $row;
			}
			return $result;
		}
	}

        public function selectPackageAndroid($makeName,$modelName)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();

                if($connection)
                {
                        $selectBrandIdObject = new VehicleBrand;
                        $selectBrandId = $selectBrandIdObject -> selectVehicleBrandId($makeName);

                        $selectVehicleCategoryIdObject = new VehicleModel;
                        $selectVehicleCategoryId = $selectVehicleCategoryIdObject -> selectVehicleModelId($modelName,$selectBrandId);

                        $result = array();
                        $selectPackageQuery = mysql_query("select packageId from tblPackage where categoryId = '$selectVehicleCategoryId'");
                        while($row = mysql_fetch_assoc($selectPackageQuery))
                        {
                                $result[] = $row;
                        }
                        return $result;
                }
        }

	public function selectPackageId($makeName,$modelName,$packageName,$packagePrice)
        {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();

                if($connection)
                {
                        $selectBrandIdObject = new VehicleBrand;
                        $selectBrandId = $selectBrandIdObject -> selectVehicleBrandId($makeName);

                        $selectVehicleCategoryIdObject = new VehicleModel;
                        $selectVehicleCategoryId = $selectVehicleCategoryIdObject -> selectVehicleModelId($modelName,$selectBrandId);

                        $selectPackageQuery = mysql_query("select packageId from tblPackage where categoryId = '$selectVehicleCategoryId' and packageName = '$packageName' and packagePrice = '$packagePrice'");
 			$row = mysql_fetch_array($selectPackageQuery);
                        $returnValue = $row[0];
               		return $returnValue;
                }
        }

	 public function insertPackage($packageName,$packagePrice,$packageEstimatedPrice,$packagePeriod,$vehicleCategoryName)
         {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        //$cityObject = new City;
                        //$cityId = $cityObject -> selectCityId($cityName);
			$vehicleCategoryObject = new vehicleCategory;
			$categoryId = $vehicleCategoryObject -> selectVehicleCategoryId($vehicleCategoryName);
                        $insertPackageQuery = mysql_query("insert into tblPackage (packageName,packagePrice,packageEstimatedPrice,
			packagePeriod,categoryId) values ('$packageName','$packagePrice','$packageEstimatedPrice','$packagePeriod',
			'$categoryId')");
                        if($insertPackageQuery)
                        {
                                //$returnValue["success"] = true;
                                return true;
                        }
                        else
                        {
                                //$returnValue["success"] = false;
                                return false;
                        }
                  }
         }

	   public function updatePackage($packageId,$packageName)
           {
                $databaseObject = new DbConnection();
                $connection = $databaseObject -> databaseConnection();
                if($connection)
                {
                        $updatePackageQuery = mysql_query("update tblPackage set packageName = '$packageName' where
                                                        packageId = '$packageId'");
                        if($updatePackageQuery)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
           }
	 public function selectPackageVehicleCategory()
	 {
		 $databaseObject = new DbConnection();
                 $connection = $databaseObject -> databaseConnection();
		 $result = array();
                 if($connection)
                 {
 			$selectPackageQuery = mysql_query("select tblPackage.packageId,tblPackage.packageName,tblPackage.packagePrice,
			tblPackage.packageEstimatedPrice,tblPackage.packagePeriod,tblVehicleCategory.vehicleCategoryName from tblPackage
			inner join tblVehicleCategory on tblPackage.categoryId = tblVehicleCategory.vehicleCategoryId");
			while($row = mysql_fetch_assoc($selectPackageQuery))
                        {
                                $result[] = $row;
                        }
                        return $result;

          	 }
	 }
}

?>
