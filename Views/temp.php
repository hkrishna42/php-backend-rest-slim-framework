<?php

?>

<!DOCTYPE html>

<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Google Maps Example</title>
        <style type="text/css">
            body { font: normal 14px Verdana; }
            h1 { font-size: 24px; }
            h2 { font-size: 18px; }
            #sidebar { float: right; width: 30%; }
            #main { padding-right: 15px; }
            .infoWindow { width: 220px; }
        </style>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

        <script type="text/javascript">

        /*var map;

        // Ban Jelacic Square - Center of Zagreb, Croatia
        var center = new google.maps.LatLng(18.4999, 73.8570);

        function init()
	{
		var mapOptions =
		{
			zoom: 13,
			center: center,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		map = new google.maps.Map(document.getElementById("map"), mapOptions);

		makeRequest('/get_locations', function(data)
		{
			var data = JSON.parse(data.responseText);

			for (var i = 0; i < data.length; i++)
			{
				displayLocation(data[i]);
			}
		});
	}

	function displayLocation(location)
	{
		if (parseInt(location.lat) == 0)
		{
			geocoder.geocode( { 'address': location.serviceCentreAddress }, function(results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{
					var marker = new google.maps.Marker(
					{
						map: map,
						position: results[0].geometry.location,
						title: location.name
					});

					google.maps.event.addListener(marker, 'click', function()
					{
						infowindow.content(content);
						infowindow.open(map,marker);
					});
				}
			});
		}
		else
		{
			var position = new google.maps.LatLng(parseFloat(location.serviceCentreLat), parseFloat(location.serviceCentreLong));
			//var icon= 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
			var html = "<b>" + location.serviceCentreName + "</b> <br/>";
			var infoWindow = new google.maps.InfoWindow;
			var marker = new google.maps.Marker(
			{
				map: map,
				position: position,
				title: location.serviceCentreName,
				//icon: icon
			});
			bindInfoWindow(marker, map, infoWindow, html);
		}
	}

	function makeRequest(url, callback)
	{
		var request = window.ActiveXObject ?
		new ActiveXObject('Microsoft.XMLHTTP') :
		new XMLHttpRequest;

		request.onreadystatechange = function()
		{
			if (request.readyState == 4)
			{
				request.onreadystatechange = doNothing;
				callback(request, request.status);
			}
		};

		request.open('GET', url, true);
		request.send(null);
	}

	function doNothing() {}

	function bindInfoWindow(marker, map, infoWindow, html)
	{
		google.maps.event.addListener(marker, 'mouseover', function()
		{
			infoWindow.setContent(html);
			infoWindow.open(map, marker);
			//window.open('/index','_self',false);
		});
		google.maps.event.addListener(marker, 'click', function()
		{
			window.open('/index','_self',false);
		});
	}*/
	</script>
<body>
<table>
  <tr>
    <td style="padding-right:10px">
      <label for="input-3" class="control-label">Likes</label>
    </td>
    <td>
      <input id="input-3" value="3" class="rating-loading">
    </td>
  </tr>
</table>
<script>
$(document).on('ready', function(){
    $('#input-3').rating({displayOnly: true, step: 0.5});
});
</script>
</body>
<!--<body onload="init();">
	<div id="map" style="width: 70%; height: 500px;"></div>
</body>-->
</html>
