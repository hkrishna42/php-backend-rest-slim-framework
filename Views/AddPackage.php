<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <title>Doochaki Admin</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href = "mdl/style.css">

    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">

    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

</head>

<body>

<!-- Uses a header that scrolls with the text, rather than staying

  locked at the top -->

<div class="mdl-layout mdl-js-layout">

  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">

    <div class="mdl-layout__header-row">

      <!-- Title -->

      <span class="mdl-layout-title">Doochaki</span>

      <!-- Add spacer, to align navigation to the right -->

     </header>

  <div class="mdl-layout__drawer">

    <span class="mdl-layout-title">Title</span>

    <nav class="mdl-navigation">

      <a class="mdl-navigation__link" href="/admindashboard">City</a>

      <a class="mdl-navigation__link" href="/addarea">Area</a>

      <a class="mdl-navigation__link" href="/addservice">Service</a>

      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>

      <a class="mdl-navigation__link" href="/addvehiclecategory">Vehicle Category</a>

      <a class="mdl-navigation__link" href="/addpackage">Package</a>

    </nav>

  </div>

  <main class="mdl-layout__content">
    <div class="page-content">

   	<?php
                                echo" <form action='/insertpackage' method='post'>
                                <div id='insertPackage' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                                <input id='insertPackageText' class='mdl-textfield__input' type='text' name='packageName'>
                                <label class='mdl-textfield__label' for='insertPackageText'>Package Name</label>
				</div>
				<div id='insertPackage1' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
				<input id='insertPackageText1' class='mdl-textfield__input' type='text' name='packagePrice'>
                                <label class='mdl-textfield__label' for='insertPackageText1'>Package Price</label>
				</div>
				<div id='insertPackage2' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
				<input id='insertPackageText2' class='mdl-textfield__input' type='text' name='packageEstimatedPrice'>
                                <label class='mdl-textfield__label' for='insertPackageText2'>Package Estimated Price</label>
				</div>
				<div id='insertPackage3' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
				<input id='insertPackageText3' class='mdl-textfield__input' type='text' name='packagePeriod'>
                                <label class='mdl-textfield__label' for='insertPackageText3'>Package Period</label>
				</div>
                                <!-- Accent-colored raised button -->
				<div id='insertPackageDropdown' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <select id='category' class='mdl-textfield__input' name='vehicleCategoryName'>
                 <option value=''></option>";

$url = "http://23.95.95.88/selectvehiclecategory";
$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $vehicleCategory = curl_exec($ch);
                curl_close($ch);
$vehicleCategory1 = json_decode($vehicleCategory,true);
foreach($vehicleCategory1 as $key => $vehicleCategoryName)
{
               echo" <option value=".$vehicleCategoryName['vehicleCategoryName'].">".$vehicleCategoryName['vehicleCategoryName']."</option>";
}
?>
            </select>

            <label class="mdl-textfield__label is-dirty" for="category">Select Vehicle Category</label>
         </div> 

<button type='Submit' id='insertPackageButton' class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-color--indigo-600'>
  Add
</button>
</form>



	<table id ="packageTable" class="mdl-data-table mdl-js-data-table  mdl-shadow--3dp">
  <thead>
    <tr>
        <th class="mdl-data-table__cell--non-numeric">Package Name</th>
        <th class="mdl-data-table__cell--non-numeric">Package Price</th>
	<th class="mdl-data-table__cell--non-numeric">Package Estimated</th>
	<th class="mdl-data-table__cell--non-numeric">Package Period</th>
	<th class="mdl-data-table__cell--non-numeric">Vehicle Category</th>
      <th>Edit</th>
      <th>Deactivate</th>
      <th>Add</th>
    </tr>
</thead>
  <tbody>
  <?php
$url = "http://23.95.95.88/selectpackage";
$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
//echo $result;
$package = json_decode($result,true);
foreach($package as $key => $packageName)
{
        //$serviceName1 = str_replace( ' ','%20',$serviceName['serviceName']);
        echo"<tr>

                <td class='mdl-data-table__cell--non-numeric'>"
                .$packageName['packageName'].
                "</td>
		<td class='mdl-data-table__cell--non-numeric'>"
                .$packageName['packagePrice'].
                "</td>
		<td class='mdl-data-table__cell--non-numeric'>"
                .$packageName['packageEstimatedPrice'].
                "</td>
		<td class='mdl-data-table__cell--non-numeric'>"
                .$packageName['packagePeriod'].
                "</td>
		<td class='mdl-data-table__cell--non-numeric'>"
                .$packageName['vehicleCategoryName'].
                "</td>
		<td>
                        <a href = '/editpackage?packageName=".$packageName['packageName']."
&packagePrice=".$packageName['packagePrice']."&packageEstimatedPrice=".$packageName['packageEstimatedPrice']."
&packagePeriod=".$packageName['packagePeriod']."&vehicleCategoryName=".$packageName['vehicleCategoryName']."&functionality=edit
&packageId=".$packageName['packageId']."'><i class = 'material-icons mdl-color-text--indigo-600'>edit</i></a>
                </td>
                <td>
                     <a href = /admindashboard> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a>
               </td>
		 <td>
                     <a href = /admindashboard> <i class = 'material-icons mdl-color-text--indigo-600'>add</i></a>
                </td>


        </tr>";
}
?>
  </tbody>
</table>
    </div>

  </main>

</div>
</body>
</html>


