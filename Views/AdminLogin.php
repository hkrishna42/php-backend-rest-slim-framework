<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
body { background:#34abee;    font-family: "Helvetica Neue", Helvetica, Arial;  margin: 0 auto; }
h4 {    color:#fff;    font-size:2em;	text-align:center;	}
.header { width: 100%; height:100px; background-color:#f5f5f5;  margin: 0 auto; }
.logo{  width:150px; height:100px; font-family: "Helvetica Neue", Helvetica, Arial; color: #fff; text-align:center;  font-size: 18px; font-weight:bold;  background-color:#333; margin-left:23%;   }
.container {    width: 406px;    max-width: 406px;    margin: 0 auto;}

#signup {    padding: 0px 25px 25px;    background: #fff;    box-shadow: 0px 0px 0px 5px rgba( 255,255,255,0.4 ), 0px 4px 20px rgba( 0,0,0,0.33 );    -moz-border-radius: 5px;    -webkit-border-radius: 5px;    border-radius: 5px;    display: table;    position: static;}
#signup .header-nav {    margin-bottom: 20px;}
#signup .header-nav h3 {    color: #333333;    font-size: 24px;    font-weight: bold;    margin-bottom: 5px;}
#signup .header-nav p {    color: #8f8f8f;    font-size: 14px;    font-weight: 300;}
#signup .sep {    height: 1px;    background: #e8e8e8;    width: 406px;    margin: 0px -25px;}
#signup .inputs {    margin-top: 25px;}
#signup .inputs label {    color: #8f8f8f;    font-size: 12px;    font-weight: 300;    letter-spacing: 1px;    margin-bottom: 7px;    display: block;}
.input::-webkit-input-placeholder {    color:    #b5b5b5;}
.input:-moz-placeholder {    color:    #b5b5b5;}
#signup .inputs input[type=text], input[type=password] {  background: #f5f5f5;    font-size: 0.8rem;    -moz-border-radius: 3px;    -webkit-border-radius: 3px;    border-radius: 3px;    border: none;    padding: 13px 10px;    width: 330px;    margin-bottom: 20px;    box-shadow: inset 0px 2px 3px rgba( 0,0,0,0.1 );    clear: both;}
#signup .inputs input[type=text]:focus, input[type=password]:focus {    background: #fff;    box-shadow: 0px 0px 0px 2px #294072, inset 0px 2px 3px rgba( 0,0,0,0.2 ), 0px 5px 5px rgba( 0,0,0,0.15 );    outline: none;   }
#signup .inputs label.terms {    float: left;    font-size: 14px;    font-style: italic;}
#signup .inputs #submit { width: 100%; margin-top: 10px; padding: 15px 0; color: #f2f8fd; font-size: 14px; font-weight: 500; letter-spacing: 1px;   text-align: center;  text-decoration: none;    
 background: -moz-linear-gradient( top, #b9c5dd 0%, #294072); background: -webkit-gradient( linear, left top, left bottom,  from(#b9c5dd), to(#294072)); -moz-border-radius: 5px; -webkit-border-radius: 5px;   
 border-radius: 5px; border: 1px solid #737b8d; -moz-box-shadow:  0px 5px 5px rgba(000,000,000,0.1), inset 0px 1px 0px rgba(255,255,255,0.5); -webkit-box-shadow: 0px 5px 5px rgba(000,000,000,0.1), inset 0px 1px 0px rgba(255,255,255,0.5);   box-shadow:    0px 5px 5px rgba(000,000,000,0.1) inset 0px 1px 0px rgba(255,255,255,0.5); text-shadow: 0px 1px 3px rgba(000,000,000,0.3), 0px 0px 0px rgba(255,255,255,0);   
 display: table;    position: static; }
#signup .inputs #submit:hover {    background: -moz-linear-gradient(top, #a4b0cb 0%, #34abee);    background: -webkit-gradient( linear, left top, left bottom, from(#a4b0cb), to(#34abee));}
</style>
</head>

<body>

<div class="header">
<div class="logo">Logo</div>
</div>

<div class="container">
<h4>Administrative Panel</h4>
<form id="signup" action='/checkAdminLogin' method='POST'>
<div class="header-nav">
<h3>Enter Your Login Details</h3>            
<p>You want to fill out this form</p>
</div>

<div class="sep"></div>

<div class="inputs">

<input type="text" placeholder="User Name" name="userName"autofocus />
<input type="password" placeholder="Password" name="userPassword"/>
<button type="submit" id="submit" name="Submit">Login</button>
</div>
</form>
</div>
​
</body>
</html>
