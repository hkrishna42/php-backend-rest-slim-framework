<?php
	session_start();
	unset($_SESSION['cityName']);
	unset($_SESSION['areaName']);
	unset($_SESSION['vehicleBrandName']);
	unset($_SESSION['vehicleModelName']);

	echo "<script type='text/javascript'>window.location.href = '/index';</script>";
?>
