<?php
session_start();
if(empty($_SESSION['userName']))
{
        echo "<script type='text/javascript'>alert('Please Login');</script>";
        echo "<script type='text/javascript'>window.location.href ='/AdminLogin';</script>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--<meta http-equiv="refresh" content="20">-->
    <title>Doochaki - Edit Service Centre</title>
    <link rel="stylesheet" type="text/css" href="admindoochaki/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="admindoochaki/css/layout.css" media="screen" />
     <script src="/admindoochaki/js/jquery-1.8.0.min.js" type="text/javascript"></script>
   <script src="/admindoochaki/js/script.js" ></script>
   
<style type="text/css"> 
table {width:80%;margin-top:10px;}
table, th, td {border-collapse: collapse;}
th, td {padding: 5px;text-align: left; vertical-align:middle;}
table#t01 tr:nth-child(even) {background-color: #eee;}
table#t01 tr:nth-child(odd) {background-color:#fff;}
table#t01 th    {background-color: #2d4956;color: white;}
#label1 {font-size:14px;font-weight:bold; padding:10px;}
#btn1 {width:170px;height:40px;font-size:18px;background-color:#489c22;color:#fff;font-weight:bold;font-family:Arial, Helvetica,sans-serif;border-radius:10px;}


.btn {  font-size: 3vmin;  padding: 0.75em 1.5em;    color: #333; border:none; outline:none;  text-decoration: none;  display: inline;  border-radius: 4px; }
.btn:hover {   -webkit-transition: background-color 1s ease;  -moz-transition: background-color 1s ease;  transition: background-color 1s ease;}
.btn-small {  padding: .75em 1em;  font-size: 0.8em;}
.modal-box {  display: none;  position: absolute;  z-index: 1000;  width: 60%;  background: white;  border-bottom: 1px solid #aaa;  border-radius: 4px;  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);  border: 1px solid rgba(0, 0, 0, 0.1);  background-clip: padding-box;}
@media (min-width: 32em) {
.modal-box { width: 70%; }}
.modal-box header,
.modal-box .modal-header {  padding: 1.25em 1.5em;  border-bottom: 1px solid #ddd;}
.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin: 0; }
.modal-box .modal-body { padding: 2em 1.5em; }
.modal-box footer,
.modal-box .modal-footer {  padding: 1em;  border-top: 1px solid #ddd;  background: rgba(0, 0, 0, 0.02);  text-align: right;}
.modal-overlay {  opacity: 0;  filter: alpha(opacity=0);  position: absolute;  top: 0;  left: 0;  z-index: 900;  width: 100%;  height: 100%;  background: rgba(0, 0, 0, 0.3) !important;}
a.close {  line-height: 1;  font-size: 1.5em;  position: absolute;  top: 5%;  right: 2%;  text-decoration: none;  color: #bbb;}
a.close:hover {  color: #222;  -webkit-transition: color 1s ease;  -moz-transition: color 1s ease;  transition: color 1s ease;}

table {  }
table, th, td  { }
th, td {    padding: 5px;    text-align: left;}
</style>

<!-- File For Ajax -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<script type="text/javascript">
        $(document).ready(function()
        {
                $(".cityName").change(function()
                {
                        var id=$(this).val();
                        var dataString = 'id='+ id;

                        $.ajax
                        ({
                                type: "POST",
                                url: "/getareaweb",
                                data: dataString,
                                cache: false,
                                success: function(html)
                                {
                                        $(".areaName").html(html);
					alert(html);
                                }
                        });
                });

        });
</script>

</head>
<body>
<div class="container_12">
        <div class="grid_12 header-repeat">
                <div id="branding">
                        <div class="floatleft">
				<?php echo"<h1 style='color:white;'>Hello,".$_SESSION['userName']." </h1>";
                         ?>
                        </div>
                        <div class="floatright">
                                <div class="floatleft">
                                </div>
                                <div class="floatleft marginleft10">
                                        <ul class="inline-ul floatleft">
                                                <li><a href="/adminlogout">Logout</a></li>
                                        </ul>
                                        <br/>
                                </div>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">   
        </div>
        <div class="clear">
        </div>
        <div class="grid_2">
                <div class="box sidemenu">
                        <div id='cssmenu'>
                        <ul>
		<li class='has-sub'><a href="/AdminAddCity"><span>Dashboard</span></a></li>
                      <li class='has-sub'><a href="#"><span>Add / Update Details</span></a>
                           <ul>
			<li class='active'><a href="/AdminAddCity"><span>City</span></a></li>
                    <li class='has-sub'><a href="/AdminAddArea"><span>Area</span></a></li>
                    <li class='has-sub'><a href="/AdminAddService"><span>Service</span></a></li>
                     <li class='has-sub'><a href="/AdminAddServiceCentre"><span>Service Centre</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleBrand"><span>Vehicle Brand</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleCategory"><span>Vehicle Category</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleModel"><span>Vehicle Model</span></a></li>

                    <!--<li class='has-sub'><a href="addpackage.html"><span>Package</span></a></li>-->

                           </ul>
                      </li>
              <li class='last'><a href="/AdminViewDeactivatedServiceCentre"><span>Activate Details</span></a></li>
                         </ul>
                        </div>
                        <div class="block" id="section-menu">
                        </div>
                </div>
        </div>

        <div class="grid_10">
   <div class="box round first">
    <h2>Service Centre</h2>
   <div class="block1">  
      <div id="logged-in-home-search">
        <h3>Edit Service Centre</h3>
                <div class="newsletter">
                <?php
                if(isset($_GET['serviceCentreName']) && isset($_GET['cityName']) && isset($_GET['areaName']) && isset($_GET['serviceCentreId']))
                {
                        $cityName = $_GET['cityName'];
                        $areaName = $_GET['areaName'];
			$serviceCentreContact = $_GET['serviceCentreContact'];
			$serviceCentreName = $_GET['serviceCentreName'];
			$serviceCentreEmail= $_GET['serviceCentreEmail'];
			$serviceCentreAddress = $_GET['serviceCentreAddress'];
			$serviceCentreLat = $_GET['serviceCentreLat'];
			$serviceCentreLong = $_GET['serviceCentreLong'];
			$serviceCentreLandmark = $_GET['serviceCentreLandmark'];
			$serviceCentrePincode = $_GET['serviceCentrePincode'];
			$serviceCentreSpeciality=$_GET['serviceCentreSpeciality'];
                        $serviceCentreId = $_GET['serviceCentreId'];

                        echo"<form action ='/updateservicecentre' method='post'>
                                <table width='60%' cellspacing='2' style=' margin-left:100px; margin-top:50px;'>
                                        <tr>
                                        	<td width='10%' align='left' valign='top' >
							<strong style='font-size:14px;'> City :</strong>
						</td>
                                        	<td width='40%' align='left' valign='top' >
                                        		<select type='text' name='cityName' style='width:25%; float:left;' class='cityName' id='cityName'>
                                        			<option value=".$cityName.">".$cityName."</option>";
								$url = 'http://doochaki.com/selectcity';
	                                                        $ch = curl_init();
        	                                                curl_setopt($ch, CURLOPT_URL,$url);
                	                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        	                                $city = curl_exec($ch);
                                	                        curl_close($ch);
                                        	                $city1 = json_decode($city,true);
                                                	        foreach($city1 as $key => $cityName)
                                                        	{
                                                               		echo" <option value=".$cityName['cityName'].">".$cityName['cityName']."</option>";
                                                        	}
							echo "</select>
						</td>
					</tr>
					<tr>
						<td width='10%' align='left' valign='top'>
							<strong style='font-size:14px;'> Area : </strong>
						</td>
						<td width='40%' align='left' valign='top'>
							<select type='text' name='areaName' style='width:25%; float:left;' class='areaName' id='searchArea'>
								<option value='".$areaName."'>".$areaName."</option>"; }?>
							</select>
						</td>
					</tr>
				<?php
					echo"
					<tr>
    						<td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Name : </strong></td>
						<td width='40%' align='left' valign='top'><input type='text' name='serviceCentreName' value='".$serviceCentreName."' class='location11' style='width:25%; min='0' max='10' float:left;' placeholder='Name' /></td>
				        </tr>
				        <tr>
						<td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Contact : </strong></td>
						<td width='40%' align='left' valign='top'><input type='text' name='serviceCentreContact' value='".$serviceCentreContact."' class='location11'style='width:25%; min='0' maxlength='10' float:left;' placeholder='Contact' /></td>
   				        </tr>
   				        <tr>
					        <td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre E-mail : </strong></td>
					        <td width='40%' align='left' valign='top'><input type='text' name='serviceCentreEmail' value='".$serviceCentreEmail."' style='width:25%; float:left;' class='location11' size='50' placeholder='E-mail' /></td>
   				       </tr>
   				       <tr>
					        <td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Address : </strong></td>
						<td width='40%' align='left' valign='top'><input name='serviceCentreAddress' value='".$serviceCentreAddress."' type='text' style='width:25%; float:left;' class='location11' size='50' placeholder='Address' /></td>
				      </tr>
				      <tr>
						<td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Landmark : </strong></td>
						<td width='40%' align='left' valign='top'><input name='serviceCentreLandmark' value='".$serviceCentreLandmark."' type='text' style='width:25%; float:left;' class='location11' size='50' placeholder='Landmark' /></td>
   				 	</tr>
  					<tr>
					       <td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Pincode : </strong></td>
					       <td width='40%' align='left' valign='top'><input name='serviceCentrePincode' value='".$serviceCentrePincode."' type='text' style='width:25%; float:left;' class='location11' size='50' placeholder='Pincode' /></td>
   				   	</tr>
   <tr>
					    <td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Latitude : </strong></td>
					    <td width='40%' align='left' valign='top'><input name='serviceCentreLat' type='text' value='".$serviceCentreLat."' style='width:25%; float:left;' class='location11' size='100' placeholder='Latitude' /></td>
   </tr>
   <tr>
					    <td width='10%' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Longitude : </strong></td>
					    <td width='40%' align='left' valign='top'><input name='serviceCentreLong' type='text' value='".$serviceCentreLong."' style='width:25%; float:left;' class='location11' size='100' placeholder='Longitude' /></td>
   </tr>
					<tr>
    					    <td widt='10' align='left' valign='top'><strong style='font-size:14px;'> Service Centre Speciality : </strong></td>
					    <td width='40%' align='left' valign='top'><input name='serviceCentreSpeciality' value='".$serviceCentreSpeciality."' type='text' style='width:35%; float:left;' class='location11' size='100' placeholder='Speciality' /></td>
   					</tr>
					<input type='hidden' name='serviceCentreId' value=".$serviceCentreId.">";
			 	?>
<br>

					<input type="submit" style="float:left; width:10%; margin-left:15%;" value="Update" id="updateAreaButton">

                               </table>
                      </form>
                </div>
         </div><br><br>

<script src="admindoochaki/js/jquery-1.11.1.min.js"></script> 
</div>
</div>
        </div>

        <div class="clear">
        </div>
</div>
<div class="clear">
</div>

<div id="site_info">
        <p>
                Copyright &copy;<a href="#"> Admin</a> All Rights Reserved.
        </p>
</div>
</body>
</html>

