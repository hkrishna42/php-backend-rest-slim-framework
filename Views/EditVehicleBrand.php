<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="">Link</a>
      <a class="mdl-navigation__link" href="">Link</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
	<?php
        if(isset($_GET['vehicleBrandName']) && isset($_GET['functionality']))
                {
                        $vehicleBrandName = $_GET['vehicleBrandName'];
                        $vehicleBrandId = $_GET['vehicleBrandId'];
                        $functionality = $_GET['functionality'];
                        if($functionality == 'edit')
                        {
                                echo" <form action='/updatevehiclebrand' method='post'>
                                <div id='updateVehicleBrand' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <input id='updateVehicleBrandText1' class='mdl-textfield__input' type='text' name='vehicleBrandName' value='".$vehicleBrandName."'>
                                <label class='mdl-textfield__label' for='updateVehicleBrandText1'>Vehicle Brand</label>
                                <input type='text' value='".$vehicleBrandId."' name='vehicleBrandId' hidden>
                                <!-- Accent-colored raised button -->
                                </div>
<button type='Submit' id='updateVehicleBrandButton' class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-color--indigo-600'>
  Update
</button>
</form>";}}
        ?>

    </div>
  </main>
</div>


</body>
</html>

