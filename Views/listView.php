<?php
	session_start();
	if(!isset($_SESSION['cityName']) && !isset($_SESSION['areaName']) && !isset($_SESSION['vehicleBrandName']) && !isset($_SESSION['vehicleModelName']))
	{
		echo "<script type='text/javascript'>window.location.href = '/index';</script>";
	}
	else
	{
		$cityName = $_SESSION['cityName'];
		$areaName = $_SESSION['areaName'];
		$vehicleBrandName = $_SESSION['vehicleBrandName'];
		$vehicleModelName = $_SESSION['vehicleModelName'];
	}

	if(!empty($_GET['packageName'] ))
	{
		$packageName = $_GET['packageName'];
	}
	else
	{
		$packageName = '';
	}

	if(!empty($_GET['RP'] ))
	{
		$RP = $_GET['RP'];
	}
	else
	{
		$RP = '';
	}

	if(!empty($_GET['services'] ))
	{
		$serviceNames = $_SESSION['servicesArray'];
	}
	else
	{
		$serviceNames = '';
	}

	$baseUrlObj = new BaseUrl;
	$baseUrl = $baseUrlObj -> baseUrl();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Doochaki</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/favicon57x57.png">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

	<script>
	$(document).ready(function()
	{
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>

    </head>

    <body>

        <!-- Top menu -->
	<nav class="navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/index">Doochaki</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="top-navbar-1">
				<img src="assets/img/DoochakiName.png" class="navbar-brand-logo-image"></img>
			</div>
		</div>
	</nav>

	<div class="listMapPageLinkDiv row">
		<?php
			if($RP == 'rsa' && $serviceNames == '')
			{
				echo "<a href='/map?RP=rsa' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='Map View'><i class='fa fa-map-marker'></i></a>";
			}
			else if($RP == 'puncture' && $serviceNames == '')
			{
				echo "<a href='/map?RP=puncture' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='Map View'><i class='fa fa-map-marker'></i></a>";
			}
		?>
			<a href="/removesessionvariables" class="removeSessionVariablesLink" data-toggle="tooltip" data-placement="top" title="Change City/Area/Make/Model"><i class="fa fa-pencil"></i></a>
	</div>

	<div class="container">
		<?php
			if($RP == 'rsa' && $packageName == '' && $serviceNames == '')
			{
				$fields = array("cityName" => $cityName,"areaName" => $areaName,"makeName" => $vehicleBrandName,"modelName" => $vehicleModelName);

				$url = $baseUrl."getservicecentresrsaandroid";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				$centres = curl_exec($ch);
				curl_close($ch);
				$serviceCentres = json_decode($centres,true);

				foreach($serviceCentres as $key => $obj)
				{
					echo "<div class='listContainer'>
						<div class='row'>
							<div class='listImageDiv col-sm-3'>
								<img src='assets/img/DoochakiLogo.png'></img>
							</div>
							<div class='listContent col-sm-6'>
								<p>
									<strong class='serviceCentreName'>".$obj['serviceCentreName']."</strong><br><br>
									<!--<strong class='priceTotal'><i class='fa fa-rupee'></i> 600/-</strong>-->
									<i class='fa fa-phone'></i><strong class='serviceCentreContact'>Phone :</strong>".$obj['serviceCentreContact']."<br><br>
									<i class='fa fa-map-marker'></i><strong class='serviceCentreAddress'>Address :</strong>".$obj['serviceCentreAddress']."<br><br>
									<!--<i class='fa fa-star'></i><strong class='serviceCentreRating'>Rating :</strong><button class='btn1'>3.5</button>
									<strong class='priceTotal'><i class='fa fa-rupee'></i> 600/-</strong>-->
								</p>
							</div>
							<div class='bookBtn col-sm-3'>
								<a href='/getmessage/".$obj['serviceCentreName']."/".$obj['serviceCentreContact']."/R/W'><button type='submit' class='btn btn-default'>Send Message</button></a>
							</div>
						</div>
					</div>";
				}
			}
			else if($RP == 'puncture' && $packageName == '' && $serviceNames == '')
			{
				$fields = array("cityName" => $cityName,"areaName" => $areaName,"makeName" => $vehicleBrandName,"modelName" => $vehicleModelName);

				$url = $baseUrl."getservicecentrespunctureandroid";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				$centres = curl_exec($ch);
				curl_close($ch);
				$serviceCentres = json_decode($centres,true);

				foreach($serviceCentres as $key => $obj)
				{
					echo "<div class='listContainer'>
						<div class='row'>
							<div class='listImageDiv col-sm-3'>
								<img src='assets/img/DoochakiLogo.png'></img>
							</div>
							<div class='listContent col-sm-6'>
								<p>
									<strong class='serviceCentreName'>".$obj['serviceCentreName']."</strong><br><br>
									<!--<strong class='priceTotal'><i class='fa fa-rupee'></i> ".$obj['serviceCentrePrice']."/-</strong>-->
									<i class='fa fa-phone'></i><strong class='serviceCentreContact'>Phone :</strong>".$obj['serviceCentreContact']."<br><br>
									<i class='fa fa-map-marker'></i><strong class='serviceCentreAddress'>Address :</strong>".$obj['serviceCentreAddress']."<br><br>
									<i class='fa fa-rupee'></i><strong class='serviceCentreRating'> Price :</strong>".$obj['serviceCentrePrice']."/-</strong>
									<!--<i class='fa fa-star'></i><strong class='serviceCentreRating'>Rating :</strong><button class='btn1'>3.5</button>
									<strong class='priceTotal'><i class='fa fa-rupee'></i> 600/-</strong>-->
								</p>
							</div>
							<div class='bookBtn col-sm-3'>
								<a href='/getmessage/".$obj['serviceCentreName']."/".$obj['serviceCentreContact']."/P/W'><button type='submit' class='btn btn-default'>Send Message</button></a>
							</div>
						</div>
					</div>";
				}
			}

			else if($RP == '' && $packageName == '' && $serviceNames != '')
			{
				/*foreach($serviceNames as $service)
				{
					echo $service."<br>";
				}
				$jsonValueServices = array("serviceName" => $serviceNames,"city" => $cityName,"area" => $areaName,"make" => $vehicleBrandName,"model" => $vehicleModelName);

				echo json_encode($jsonValueServices);*/
				$servicesToPass = array("serviceName" => $serviceNames);

				$fields = array("serviceName" => $serviceNames,"city" => $cityName,"area" => $areaName,"make" => $vehicleBrandName,"model" => $vehicleModelName);
				$field1 = json_encode($fields);
				$fields_string = array('jsonValueServices' => $field1);

				$url = $baseUrl."getservicecentresservicesandroid";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$centres = curl_exec($ch);
				curl_close($ch);
				$serviceCentres = json_decode($centres,true);

                                $serviceCentresArray = $serviceCentres['serviceCentreDetails'];
                                $servicesNamesArray = $serviceCentres['services'];
                                $servicesPriceArray = $serviceCentres['servicesPrice'];

                                $SCName = array();
                                $SCAddress = array();
                                $SCContact = array();
                                $SCLat = array();
                                $SCLong = array();

				foreach($serviceCentres['serviceCentreDetails'] as $key => $obj)
				{
					$SCName[] = $obj['serviceCentreName'];
					$SCAddress[] = $obj['serviceCentreAddress'];
					$SCContact[] = $obj['serviceCentreContact'];
				}

				$removedName = array();
				$removedAddress = array();
				//$removedContact = array_unique($SCContact);

				$uniqueme = array();
				foreach ($SCContact as $key => $value)
				{
					$uniqueme[$value] = $key;
				}
				$removedContact = array();
				foreach ($uniqueme as $key => $value)
				{
					$removedContact[] = $key;
				}

				foreach(array_values($removedContact) as $i => $value)
				{
					$key = array_search($value,$SCContact);
					$removedName[] = $SCName[$key];
					$removedAddress[] = $SCAddress[$key];
				}

                                if(count($serviceCentresArray) > 0)
                                {
	                                foreach(array_values($removedName) as $i => $value)
        	                        {
                		                $serviceNames1 = array();
                                		$servicePrice = array();
		                                $serviceNameToBook = array();
                		                $servicePriceToBook = array();

		                                $displayName = $removedName[$i];
                		                $displayAddress = $removedAddress[$i];
                                		$displayContact = $removedContact[$i];

		                                foreach(array_values($SCName) as $j => $value1)
                		                {
                                			$name1 = $SCName[$j];
			                                if($displayName == $name1)
                        			        {
								//$serviceNames1[] = $servicesNamesArray[$j] ." : Rs."+$servicesPriceArray[$j]."/-<br>";
                                				//serviceNames.push(services[j]+" : Rs."+servicesPrice[j]+"/-<br>");
				                                $serviceNameToBook[] = $servicesNamesArray[$j];
       	                        				$servicePriceToBook[] = $servicesPriceArray[$j];
			                                }
                        		        }

	                                        echo "<div class='listContainer'>
        	                                        <div class='row'>
                	                                        <div class='listImageDiv col-sm-3'>
                        	                                        <img src='assets/img/DoochakiLogo.png'></img>
                                	                        </div>
                                        	                <div class='listContent col-sm-4'>
                                                	                <p>
                                                        	                <strong class='serviceCentreName'>".$displayName."</strong><br><br>
                                                                	        <i class='fa fa-phone'></i><strong class='serviceCentreContact'>Phone :</strong>".$displayContact."<br><br>
                                                                        	<i class='fa fa-map-marker'></i><strong class='serviceCentreAddress'>Address :</strong>".$displayAddress."<br><br>
                        	                                        </p>
                                	                        </div>
                                        	                <div class='listViewServices col-sm-3'>
									<p><b>Your Services : </b><br>";
                                                	                foreach(array_values($serviceNameToBook) as $k => $value)
                                                        	        {
										if($servicePriceToBook[$k] != null)
										{
                                                                	        	echo $serviceNameToBook[$k]." : Rs.".$servicePriceToBook[$k]."/-<br>";
										}
										else
										{
											echo $serviceNameToBook[$k]." : Not Available<br>";
										}
	                                                                }
									echo "</p>
        	                                                </div>
                	                                        <div class='bookBtn col-sm-2'>
                        	                                        <a href='/bookService?Name=".$displayName."&Address=".$displayAddress."&Contact=".$displayContact."&service=".json_encode($servicesToPass)."'><button type='submit' class='btn btn-default'>Book Service</button></a>
                                	                        </div>
                                        	        </div>
	                                        </div>";
	                                }

                                }
                                else
                                {
                                	alert("No service centres were found for the selected criteria");
                                }

			}

		?>
	</div>

	<!-- About Us Modal -->
                <div class="modal fade" id="aboutUsModal" role="dialog">
                        <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content1">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">About Us</h4>
                                        </div>
                                        <div class="modal-body">
                                                <h5><b>
							Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
							Doochaki was started by a team of engineering professionals from Pune, who wanted to devise intelligent solution for a common problem - service center kaha hai?
							Doochaki helps you find a large number of verified vendors in any location. All vendors are vetted by Doochaki and police-verified, to ensure the safety of you and your vehicle. Furthermore, all vendors receive performance scores for the quality and promptness of their services. The aggregate performance scores help you in selecting the best service vendors for you.
                                                </b></h5>
                                        </div>
                                        <div class="modal-footer">
                                                <!--a href="/map" class="btn btn-default">OK</a>-->
                                                <button type="submit" data-dismiss="modal" class="btn btn-default">CLOSE</button>
                                        </div>
                                </div>
                        </div>
                </div>

	<!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 footer-box wow fadeInUp">
                        <h4>About Us</h4>
                        <div class="footer-box-text">
                                <p><b>
                                        Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
                                        Doochaki was started by a team of engineering professionals from Pune, who wanted...
                                </b></p>
                                <p><b><a href="#aboutUsModal" data-toggle="modal">READ MORE...</a></b></p>
                        </div>
                    </div>
                    <!--<div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Email Updates</h4>
                        <div class="footer-box-text footer-box-text-subscribe">
                                <p>Enter your email and you'll be one of the first to get new updates:</p>
                                <form role="form" action="assets/subscribe.php" method="post">
                                        <div class="form-group">
                                                <label class="sr-only" for="subscribe-email">Email address</label>
                                                <input type="text" name="email" placeholder="Email..." class="subscribe-email" id="subscribe-email">
                                        </div>
                                        <button type="submit" class="btn" disabled>Subscribe</button>
                                    </form>
                                    <p class="success-message"></p>
                                    <p class="error-message"></p>
                        </div>
                    </div>-->
                    <div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Contact Us</h4>
                        <div class="footer-box-text footer-box-text-contact">
                                <p><i class="fa fa-map-marker"></i> Address: Pune</p>
                                <p><i class="fa fa-phone"></i> Phone: 8149129955</p>
                                <p><i class="fa fa-envelope"></i> Email: info@doochaki.com</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-12 wow fadeIn">
                                <div class="footer-border"></div>
                        </div>
                </div>
                <!--<div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                       <!--<p></p>
                    </div>
                    <div class="col-sm-5 footer-social wow fadeIn">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>-->
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
