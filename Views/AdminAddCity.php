<?php
session_start();
if(empty($_SESSION['userName']))
{
	echo "<script type='text/javascript'>alert('Please Login');</script>";
        echo "<script type='text/javascript'>window.location.href ='/AdminLogin';</script>";
}
//$_SESSION["userName"];
$baseUrlObj = new BaseUrl;
$baseUrl = $baseUrlObj -> baseUrl();
?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--<meta http-equiv="refresh" content="20">-->
    <title>Doochaki - City</title>
    <link rel="stylesheet" type="text/css" href="/admindoochaki/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admindoochaki/css/layout.css" media="screen" />

     <script src="/admindoochaki/js/jquery-1.8.0.min.js" type="text/javascript"></script>
     <script src="/admindoochaki/js/script.js" ></script>

<style type="text/css"> 
table {width:80%;margin-top:10px;}
table, th, td {border-collapse: collapse;}
th, td {padding: 5px;text-align: left; vertical-align:middle;}
table#t01 tr:nth-child(even) {background-color: #eee;}
table#t01 tr:nth-child(odd) {background-color:#fff;}
table#t01 th	{background-color: #2d4956;color: white;}
#label1 {font-size:14px;font-weight:bold; padding:10px;}
#btn1 {width:170px;height:40px;font-size:18px;background-color:#489c22;color:#fff;font-weight:bold;font-family:Arial, Helvetica,sans-serif;border-radius:10px;}


.btn {  font-size: 3vmin;  padding: 0.75em 1.5em;    color: #333; border:none; outline:none;  text-decoration: none;  display: inline;  border-radius: 4px; }
.btn:hover {   -webkit-transition: background-color 1s ease;  -moz-transition: background-color 1s ease;  transition: background-color 1s ease;}
.btn-small {  padding: .75em 1em;  font-size: 0.8em;}
.modal-box {  display: none;  position: absolute;  z-index: 1000;  width: 60%;  background: white;  border-bottom: 1px solid #aaa;  border-radius: 4px;  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);  border: 1px solid rgba(0, 0, 0, 0.1);  background-clip: padding-box;}
@media (min-width: 32em) {
.modal-box { width: 70%; }}
.modal-box header,
.modal-box .modal-header {  padding: 1.25em 1.5em;  border-bottom: 1px solid #ddd;}
.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin: 0; }
.modal-box .modal-body { padding: 2em 1.5em; }
.modal-box footer,
.modal-box .modal-footer {  padding: 1em;  border-top: 1px solid #ddd;  background: rgba(0, 0, 0, 0.02);  text-align: right;}
.modal-overlay {  opacity: 0;  filter: alpha(opacity=0);  position: absolute;  top: 0;  left: 0;  z-index: 900;  width: 100%;  height: 100%;  background: rgba(0, 0, 0, 0.3) !important;}
a.close {  line-height: 1;  font-size: 1.5em;  position: absolute;  top: 5%;  right: 2%;  text-decoration: none;  color: #bbb;}
a.close:hover {  color: #222;  -webkit-transition: color 1s ease;  -moz-transition: color 1s ease;  transition: color 1s ease;}

table {  }
table, th, td  { }
th, td {    padding: 5px;    text-align: left;}
</style>

</head>
<body>
<div class="container_12">
	<div class="grid_12 header-repeat">
        	<div id="branding">
                	<div class="floatleft">
                    		<?php echo"<h1 style='color:white;'>Hello,".$_SESSION['userName']." </h1>";
				?>
			</div>
                	<div class="floatright">
                    		<div class="floatleft">
				</div>
                    		<div class="floatleft marginleft10">
                        		<ul class="inline-ul floatleft">
                          			<li><a href="/adminlogout">Logout</a></li>
                        		</ul>
                    	    		<br/>
                    		</div>
                	</div>
                	<div class="clear">
                	</div>
            	</div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">
        </div>
        <div class="clear">
        </div>
        <div class="grid_2">
		<div class="box sidemenu">
			<div id='cssmenu'>
            <ul>
              <li class='has-sub'><a href="/AdminAddCity"><span>Dashboard</span></a></li>
   		      <li class='has-sub'><a href="#"><span>Add / Update Details</span></a>
			   <ul>
                    <li class='active'><a href="/AdminAddCity"><span>City</span></a></li>
                    <li class='has-sub'><a href="/AdminAddArea"><span>Area</span></a></li>
                    <li class='has-sub'><a href="/AdminAddService"><span>Service</span></a></li>
                     <li class='has-sub'><a href="/AdminAddServiceCentre"><span>Service Centre</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleBrand"><span>Vehicle Brand</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleCategory"><span>Vehicle Category</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleModel"><span>Vehicle Model</span></a></li>

                    <!--<li class='has-sub'><a href="addpackage.html"><span>Package</span></a></li>-->
			   </ul>
   		      </li>
              <li class='last'><a href="/AdminViewDeactivatedServiceCentre"><span>Activate Details</span></a></li>
			 </ul>
			</div>
			<div class="block" id="section-menu">
			</div>
		</div>
	</div>

  	<div class="grid_10">
   <div class="box round first">
    <h2>City</h2>
   <div class="block1">  
      <div id="logged-in-home-search">
					<h3>Add City</h3>
					<div class="newsletter">
						<form id="search-form" name="addForm" method="POST" action="/insertcity" onsubmit="return validateForm()">
 							<input type="text" name="cityName"  id="addCityText"  placeholder="City Name"/>
							<input type="submit" value="Add" name="submit2" id="addCityButton">
						</form>
					</div>
				</div><br><br>

				<div id="logged-in-home-search">
					<h3>Search City</h3>
					<div class="newsletter">
						<form id="search-form" method='GET' action='/AdminAddCity'>
 							<input type="text" name="key"  id="searchCityText"  placeholder="Search City"/>
							<input type="submit" id="searchCityButton" value="Submit"> 
						</form>
					</div>
				</div>
<br><br>
<div id="logged-in-home-search"><h3>City Table</h3></div>
				<table id="t01">
  					<tr>
    					<th>Sr. No.</th>
						<th>City Name</th>
						<th>Edit</th>
						<th>Deactivate</th>
  					</tr>
                    <!--<tr>
    					<td>1</td>
						<td>Pune</td>
						<td><a href="/EditCity/"><img src="admindoochaki/img/edit.png" align="middle"/></a></td>
						<td><a href="#"><img src="admindoochaki/img/delete.png" align="middle"/></a></td>
  					</tr>-->

					<tbody>
						<?php
							if(isset($_GET['key']) && !empty($_GET['key']))
							{
								$key = $_GET['key'];
								$url = $baseUrl."selectcitybykey/$key";
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL,$url);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$city = curl_exec($ch);
								curl_close($ch);
								//echo $city;
								$city1 = json_decode($city,true);
								$cityStatus = 'N';
								$countCity = 1;

								if(!empty($city1))
								{
									foreach($city1 as $value => $cityName)
									{
										echo"<tr>
											<td>".$countCity."</td>
											<td>".$cityName['cityName']."</td>
											<td><a href = '/AdminEditCity?cityName=".urlencode($cityName['cityName'])."&cityId=".$cityName['cityId']."'><img src='admindoochaki/img/edit.png' align='middle'/></a></td>
											<td><a href = '/updatecitystatus/".$cityName['cityId']."/".$cityStatus."'><img src='admindoochaki/img/delete.png' align='middle'/></a></td>
										</tr>";
										$countCity = $countCity + 1;
									}
								}
								else
								{
									echo "<tr>
										<td style='font-size:20px;'><strong>Sorry!!! No Data Available.</strong></td>
									</tr>";
								}
							}
							else
							{
								$url = $baseUrl."selectcity";
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL,$url);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$city = curl_exec($ch);
								curl_close($ch);
								$city1 = json_decode($city,true);
								$cityStatus = 'N';
								$countCity = 1;

								if(!empty($city1))
								{
									foreach($city1 as $key => $cityName)
									{
										echo"<tr>
											<td>".$countCity."</td>
											<td>".$cityName['cityName']."</td>
											<td><a href = '/AdminEditCity?cityName=".urlencode($cityName['cityName'])."&cityId=".$cityName['cityId']."'><img src='admindoochaki/img/edit.png' align='middle'/></a></td>
											<td><a href = '/updatecitystatus/".$cityName['cityId']."/".$cityStatus."'> <img src='admindoochaki/img/delete.png' align='middle'/></a></td>
										</tr>";
										$countCity = $countCity + 1;
									}
								}
								else
								{
									echo "<tr>
										<td style='font-size:20px;'><strong>Sorry!!! No Data Available.</strong></td>
									</tr>";
								}
							}
						?>
					</tbody>
				</table>
<script>
function validateForm() {
    var x = document.forms["addForm"]["cityName"].value;
    if (x == "" || x == " ") {
        alert("City name must be filled out");
        return false;
    }
}
</script>
<script src="js/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
                
  			</div>
  		</div> 
 	</div>
       
 	<div class="clear">
	</div>
</div>
<div class="clear">
</div>
<div id="site_info">
	<p>
      		Copyright &copy;<a href="#"> Admin</a> All Rights Reserved.
      	</p>
</div>
</body>
</html>

