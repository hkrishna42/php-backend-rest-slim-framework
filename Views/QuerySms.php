<?php

	$rsaPunctureObject = new RsaPuncture;
	$result = $rsaPunctureObject -> sendQuerySms($serviceCentreContact,$RP,$messageFrom,$message);

	if($result)
	{
		if($MW == 'M')
		{
			$response["success"] = true;
			echo json_encode($response);
		}
		else
		{
			echo "<script type='text/javascript'>alert('Your Message Was Sent Successfully To Specified Service Centre. They Will Call You In A Bit. Thank You');</script>";
			echo "<script type='text/javascript'>window.location.href = '/index';</script>";
		}
	}
	else
	{
		if($MW == 'M')
		{
			$response["success"] = true;
			echo json_encode($response);
		}
		else
		{
			echo "<script type='text/javascript'>alert('The Message Was Unable To Send. Please Try Again.');</script>";
			if($RP == 'P')
			{
				echo "<script type='text/javascript'>window.location.href = '/map?RP=puncture';</script>";
			}
			else
			{
				echo "<script type='text/javascript'>window.location.href = '/map?RP=rsa';</script>";
			}
		}

	}

?>
