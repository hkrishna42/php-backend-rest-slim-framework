<?php
	$dbConnectionObject = new DbConnection;
	$connection = $dbConnectionObject -> databaseConnection();

	$pattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

	if(preg_match($pattern, $userEmail))
	{
		$regObject = new Register;
		$checkIfUserExist = $regObject -> checkIfUserExist($userEmail);

		$response = array();

		if(!$checkIfUserExist)
		{
			$checkUserEmailStatus = $regObject -> checkUserEmailStatus($userEmail);
			if($checkUserEmailStatus)
			{
				$checkUserEmailPassword = $regObject -> compareUserEmailPassword($userEmail,$userPassword);
				if($checkUserEmailPassword)
				{
					$checkUserOtpStatus = $regObject -> checkUserOtpStatus($userEmail);
					if($checkUserOtpStatus)
					{
						$selectUserId = mysql_query("select userId from tblUser where userEmail = '$userEmail' and regUsing = 'E'");
						$row = mysql_fetch_array($selectUserId);
						$userId = $row[0];

						$userDetailsQuery = mysql_query("select userName,userContact,userEmail,regUsing from tblUser where userId = '$userId'");
						$row1 = mysql_fetch_array($userDetailsQuery);

						$userDetails["checkMobile"] = true;
						$userDetails["userName"] = $row1['userName'];
						$userDetails["userContact"] = $row1['userContact'];
						$userDetails["userEmail"] = $row1['userEmail'];
						$userDetails["regUsing"] = $row1['regUsing'];

						if($loginFrom == 'M')
						{
							echo json_encode($userDetails);
						}
						else
						{
							session_start();
							$_SESSION["userName"] = $userDetails["userName"];
							$_SESSION["userContact"] = $userDetails["userContact"];
							$_SESSION["userEmail"] = $userDetails["userEmail"];
							$_SESSION["regUsing"] = $userDetails["regUsing"];

							if($fromPage == 'I')
							{
								echo "<script type='text/javascript'>window.location.href = '/index';</script>";
							}
							else
							{
								$serviceName = $_SESSION["serviceName"];
								$serviceCentreName = $_SESSION["serviceCentreName"];
								$serviceCentreAddress = $_SESSION["serviceCentreAddress"];
								$serviceCentreContact = $_SESSION["serviceCentreContact"];
								echo "<script type='text/javascript'>window.location.href = '/bookService?Name=".$serviceCentreName."&Address=".$serviceCentreAddress."&Contact=".$serviceCentreContact."&service=".json_encode($serviceName)."';</script>";
							}
						}
					}
					else
					{
						if($loginFrom == 'M')
						{
							$response["checkMobile"] = false;
							$userDetails = $regObject -> getNameEmailOnFalse($userEmail);
							$response["userName"] = $userDetails["userName"];
							$response["userEmail"] = $userDetails["userEmail"];
							echo json_encode($response);
						}
						else
						{
							$userDetails = $regObject -> getNameEmailOnFalse($userEmail);

							session_start();
                                                        $_SESSION["userName"] = $userDetails["userName"];
                                                        $_SESSION["userEmail"] = $userDetails["userEmail"];
							$_SESSION["regUsing"] = "E";
							$_SESSION["fromPage"] = $fromPage;

							echo "<script type='text/javascript'>alert('Your mobile number is not verified please re-enter mobile number and verify it.');</script>";
							echo "<script type='text/javascript'>window.location.href = '/getusermobiletrial?frPg=".$fromPage."';</script>";
						}
					}
				}
				else
				{
					if($loginFrom == 'M')
					{
						$response["checkPassword"] = false;
						echo json_encode($response);
					}
					else
					{
						echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=1';</script>";
					}
				}
			}
			else
			{
				if($loginFrom == 'M')
				{
					$response["checkEmailStatus"] = false;
					echo json_encode($response);
				}
				else
				{
					echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=3';</script>";
				}
			}
		}
		else
		{
			if($loginFrom == 'M')
			{
				$response["checkEmail"] = false;
				echo json_encode($response);
			}
			else
			{
				echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=2';</script>";
			}
		}
	}
	else
	{
		$regObject = new Register;
                $checkIfUserExist = $regObject -> checkIfUserExist($userEmail); //here it is mobile number.

                $response = array();

                if(!$checkIfUserExist)
                {
			$checkUserOtpStatus = $regObject -> checkUserOtpStatus($userEmail); //here it is mobile number.
                        //$checkUserEmailStatus = $regObject -> checkUserEmailStatus($userEmail);
			if($checkUserOtpStatus)
                        //if($checkUserEmailStatus)
                        {
                                $checkUserEmailPassword = $regObject -> compareUserEmailPassword($userEmail,$userPassword); //here it is mobile number.
                                if($checkUserEmailPassword)
                                {
                                        //$checkUserOtpStatus = $regObject -> checkUserOtpStatus($userEmail);
					$checkUserEmailStatus = $regObject -> checkUserEmailStatusFromMobile($userEmail);
                                        //if($checkUserOtpStatus)
					if($checkUserEmailStatus)
                                        {
                                                $selectUserId = mysql_query("select userId from tblUser where userContact = '$userEmail'");
                                                $row = mysql_fetch_array($selectUserId);
                                                $userId = $row[0];

                                                $userDetailsQuery = mysql_query("select userName,userContact,userEmail,regUsing from tblUser where userId = '$userId'");
                                                $row1 = mysql_fetch_array($userDetailsQuery);

                                                $userDetails["checkMobile"] = true;
                                                $userDetails["userName"] = $row1['userName'];
                                                $userDetails["userContact"] = $row1['userContact'];
                                                $userDetails["userEmail"] = $row1['userEmail'];
                                                $userDetails["regUsing"] = $row1['regUsing'];

                                                if($loginFrom == 'M')
                                                {
                                                        echo json_encode($userDetails);
                                                }
                                                else
                                                {
                                                        session_start();
                                                        $_SESSION["userName"] = $userDetails["userName"];
                                                        $_SESSION["userContact"] = $userDetails["userContact"];
                                                        $_SESSION["userEmail"] = $userDetails["userEmail"];
                                                        $_SESSION["regUsing"] = $userDetails["regUsing"];

                                                        if($fromPage == 'I')
                                                        {
                                                                echo "<script type='text/javascript'>window.location.href = '/index';</script>";
                                                        }
                                                        else
                                                        {
                                                                $serviceName = $_SESSION["serviceName"];
                                                                $serviceCentreName = $_SESSION["serviceCentreName"];
                                                                $serviceCentreAddress = $_SESSION["serviceCentreAddress"];
                                                                $serviceCentreContact = $_SESSION["serviceCentreContact"];
								echo "<script type='text/javascript'>window.location.href = '/bookService?Name=".$serviceCentreName."&Address=".$serviceCentreAddress."&Contact=".$serviceCentreContact."&service=".json_encode($serviceName)."';</script>";
                                                        }
                                                }
					}
                                        else
                                        {
						if($loginFrom == 'M')
						{
							$response["checkEmailStatus"] = false;
        	                                        echo json_encode($response);
						}
						else
						{
							echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=3';</script>";
						}
                                        }
                                }
                                else
                                {
					if($loginFrom == 'M')
					{
	                                        $response["checkPassword"] = false;
        	                                echo json_encode($response);
					}
					else
					{
						echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=1';</script>";
					}
                                }
                        }
                        else
                        {
				if($loginFrom == 'M')
				{
					$response["checkMobile"] = false;
        	                        echo json_encode($response);
				}
				else
				{
					$userDetails = $regObject -> getNameEmailOnFalse($userEmail);

					session_start();
					$_SESSION["userName"] = $userDetails["userName"];
					$_SESSION["userEmail"] = $userDetails["userEmail"];
					$_SESSION["regUsing"] = "E";
					$_SESSION["fromPage"] = $fromPage;

					echo "<script type='text/javascript'>alert('Your mobile number is not verified please re-enter mobile number and verify it.');</script>";
					echo "<script type='text/javascript'>window.location.href = '/getusermobiletrial?frPg=".$fromPage."';</script>";
				}
                        }
                }
                else
                {
			if($loginFrom == 'M')
			{
	                        $response["checkEmail"] = false;
        	                echo json_encode($response);
			}
			else
			{
				echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."&fail=2';</script>";
			}
                }
	}
?>

