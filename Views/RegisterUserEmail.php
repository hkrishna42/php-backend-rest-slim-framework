<?php

	$registerObj = new Register;
	$regUsing = 'E';
	$result = $registerObj -> insertUserEmailDetails($userName,$userEmail,$userPassword,$regFrom,$regUsing);

	if($result)
	{
		if($regFrom == 'M')
		{
			$response["success"] = true;
			echo json_encode($response);
		}
		else
		{
			session_start();
			$_SESSION['userName'] = $userName;
			$_SESSION['userEmail'] = $userEmail;
			$_SESSION['regUsing'] = "E";
			echo "<script type='text/javascript'>window.location.href = '/getusermobiletrial?frPg=".$fromPage."';</script>";
		}
	}
	else
	{
		if($regFrom == 'M')
		{
			$response["success"] = false;
			echo json_encode($response);
		}
		else
		{
			echo "<script type='text/javascript'>alert('Something Went Wrong Please Try Again!!!');</script>";
			echo "<script type='text/javascript'>window.location.href = '/login?frPg=".$fromPage."';</script>";
		}
	}
?>
