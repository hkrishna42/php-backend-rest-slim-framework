<?php
$cityObject = new City;
$areaObject = new Area;
$cityId=$cityObject -> selectCityId($cityName);
$areaId=$areaObject -> selectAreaId($cityId,$areaName);
$serviceCentreObject = new ServiceCentre;
$updateServiceCentre = $serviceCentreObject->updateServiceCentre($cityId,$areaId,$serviceCentreContact,$serviceCentreName,$serviceCentreEmail,$serviceCentreAddress,$serviceCentreLandmark,$serviceCentrePincode,$serviceCentreLat,$serviceCentreLong,$serviceCentreSpeciality,$serviceCentreId);
if($updateServiceCentre)
{
        echo "<script type='text/javascript'>alert('Service Updated Successfully!');</script>";
        echo "<script type='text/javascript'>window.location.href ='/AdminViewServiceCentre';</script>";
}
else
{
        echo "<script type='text/javascript'>alert('Service Not Updated!');</script>";
        echo "<script type='text/javascript'>window.location.href ='/AdminViewServiceCentre';</script>";
}
?>
