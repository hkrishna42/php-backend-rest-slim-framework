<?php
        session_start();
        if(!isset($_SESSION['cityName']) && !isset($_SESSION['areaName']) && !isset($_SESSION['vehicleBrandName']) && !isset($_SESSION['vehicleModelName']))
        {
                echo "<script type='text/javascript'>window.location.href = '/index';</script>";
        }
        else
        {
                $cityName = $_SESSION['cityName'];
                $areaName = $_SESSION['areaName'];
                $vehicleBrandName = $_SESSION['vehicleBrandName'];
                $vehicleModelName = $_SESSION['vehicleModelName'];
        }

	if(!empty($_GET['packageName'] ))
	{
		$packageName = $_GET['packageName'];
	}
	else
	{
		$packageName = '';
	}

	$jsonValueServices = array();
	if(!empty($_GET['serviceCheckbox'] ))
	{
		$servicesArray = $_GET['serviceCheckbox'];
		$jsonValueServices = array("serviceName" => $servicesArray,"city" => $cityName,"area" => $areaName,"make" => $vehicleBrandName,"model" => $vehicleModelName);
		$services = array("serviceName" => $servicesArray);
		//echo json_encode($servicesArray);
	}
	else
	{
		$services = '';
	}

	if(!empty($_GET['RP']))
	{
		$RP = $_GET['RP'];
	}
	else
	{
		$RP = '';
	}

	$address = $areaName.",".$cityName;
	$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
	$geo = json_decode($geo, true);
	if ($geo['status'] = 'OK')
	{
		$latitude = $geo['results'][0]['geometry']['location']['lat'];
		$longitude = $geo['results'][0]['geometry']['location']['lng'];
	}
	$centreLat = $latitude;
	$centreLong = $longitude;
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Doochaki</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/favicon57x57.png">

	</style>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBB5cwbkmXYZ9i-brN_Yrb3MnZGrG9T21w&sensor=false"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

	<script>
	$(document).ready(function()
	{
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>

	<script type="text/javascript">

	var map;
	var centreLat = '<?php echo $centreLat; ?>';
	var centreLong = '<?php echo $centreLong; ?>';

	var center = new google.maps.LatLng(centreLat, centreLong);

	var cityName = '<?php echo $cityName; ?>';
	var areaName = '<?php echo $areaName; ?>';
	var makeName = '<?php echo $vehicleBrandName; ?>';
	var modelName = '<?php echo $vehicleModelName; ?>';
	var RsaOrPuncture = '<?php echo $RP; ?>';
	var packageName = '<?php echo $packageName; ?>';
	var jsonValueServices = '<?php echo json_encode($jsonValueServices); ?>';
	/*alert(jsonValueServices);*/
	var services = '<?php echo json_encode($services); ?>';

	function init()
	{
		var mapOptions =
		{
			zoom: 10,
			center: center,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		map = new google.maps.Map(document.getElementById("map"), mapOptions);

		if(RsaOrPuncture == 'rsa' && packageName == '' && jsonValueServices.length == 2)
		{
                	$.ajax
                	({
                        	type: "POST",
                        	url: "/getservicecentresrsaandroid",
                        	data: {cityName:cityName, areaName:areaName, makeName:makeName, modelName:modelName},
                        	cache: false,
                        	success:
				function(data)
				{
					var data = $.parseJSON(data);

					if(data != "")
					{
						for (var i = 0; i < data.length; i++)
						{
							displayLocation(data[i]);
						};
					}
					else
					{
						alert("No service centres were found for the selected criteria");
					}
				},
				error: function(ts) { alert(ts.responseText) }
			});
		}
		else if(RsaOrPuncture == 'puncture' && packageName == '' && jsonValueServices.length == 2)
                {
                        $.ajax
                        ({
                                type: "POST",
                                url: "/getservicecentrespunctureandroid",
                                data: {cityName:cityName, areaName:areaName, makeName:makeName, modelName:modelName},
                                cache: false,
                                success:
                                	function(data)
                                	{
                                        	var data = $.parseJSON(data);

						if(data != "")
						{
	                                        	for (var i = 0; i < data.length; i++)
        	                                	{
                	                                	displayLocation(data[i]);
                        	                	};
						}
						else
						{
							alert("No service centres were found for the selected criteria");
						}
                                	},
                                error: function(ts) { alert(ts.responseText) }
                        });
                }
                else if(RsaOrPuncture == '' && packageName != '' && jsonValueServices.length == 2)
                {
			alert("Please note: These service centre are shown according to your selected area! Packages are available in other areas also.");
                        $.ajax
                        ({
                                type: "POST",
                                url: "/getservicecentrespackagesandroid",
                                data: {cityName:cityName, areaName:areaName},
                                cache: false,
                                success:
                                function(data)
                                {
                                        var data = $.parseJSON(data);

                                        if(data != "")
                                        {
                                                for (var i = 0; i < data.length; i++)
                                                {
                                                        displayLocation(data[i]);
                                                };
                                        }
                                        else
                                        {
                                                alert("No service centres were found for the selected criteria");
                                        }
                                },
                                error: function(ts) { alert(ts.responseText) }
                        });
                }
                else if(RsaOrPuncture == '' && packageName == '' && jsonValueServices.length > 2)
                {
                        $.ajax
                        ({
                                type: "POST",
                                url: "/getservicecentresservicesandroid",
                                data: {jsonValueServices:jsonValueServices},
                                cache: false,
                                success:
                                        function(data)
                                        {
                                                var data = JSON.parse(data);
						var serviceCentres = data.serviceCentreDetails;
						var services = data.services;
						var servicesPrice = data.servicesPrice;

						var SCName = [];
						var SCAddress = [];
						var SCContact = [];
						var SCLat = [];
						var SCLong = [];
						var SCSpeciality = [];

						for (var i = 0; i < data.serviceCentreDetails.length; i++)
						{
							SCName.push(data.serviceCentreDetails[i].serviceCentreName);
							SCAddress.push(data.serviceCentreDetails[i].serviceCentreAddress);
							SCContact.push(data.serviceCentreDetails[i].serviceCentreContact);
							SCLat.push(data.serviceCentreDetails[i].serviceCentreLat);
							SCLong.push(data.serviceCentreDetails[i].serviceCentreLong);
							SCSpeciality.push(data.serviceCentreDetails[i].serviceCentreSpeciality);
						}

						var removedName = [];
						var removedAddress = [];
						var removedSpeciality = [];
						var removedContact = remove_duplicates_safe(SCContact);
						var removedLat = remove_duplicates_safe(SCLat);
						var removedLong = remove_duplicates_safe(SCLong);

						for(var i = 0; i < removedContact.length; i++)
						{
							var position = SCContact.indexOf(removedContact[i]);
							removedName.push(SCName[position]);
							removedAddress.push(SCAddress[position]);
							removedSpeciality.push(SCSpeciality[position]);
						}

                                                if(data.serviceCentreDetails != "")
                                                {

							for(var i = 0;i < removedName.length;i++)
							{
								var serviceNames = [];
		                                                var servicePrice = [];
								var serviceNameToBook = [];
								var servicePriceToBook = [];

								var displayName = removedName[i];
								var displayAddress = removedAddress[i];
								var displayContact = removedContact[i];
								var displayLat = removedLat[i];
								var displayLong = removedLong[i];
								var displaySpeciality = removedSpeciality[i];

								for(var j = 0; j < SCName.length; j++)
								{
									var name1 = SCName[j];
									if(displayName == name1)
									{
										if(servicesPrice[j] == null)
										{
											serviceNames.push(services[j]+" : Not Available<br>");
											//servicePrice.push(servicesPrice[j]);
											//serviceNameToBook.push(services[j]);
											//servicePriceToBook.push(servicesPrice[j]);
										}
										else
                                                                                {
                                                                                        serviceNames.push(services[j]+" : Rs."+servicesPrice[j]+"/-<br>");
                                                                                        //servicePrice.push(servicesPrice[j]);
                                                                                        serviceNameToBook.push(services[j]);
                                                                                        servicePriceToBook.push(servicesPrice[j]);
                                                                                }
									}
									//serviceNameToBook.push(services[j]);
									//servicePriceToBook.push(servicesPrice[j]);
								}
								//serviceNameToBook.push(services[i]);
								//servicePriceToBook.push(servicesPrice[i]);
								displayLocationServices(displayName,displayAddress,displayContact,displayLat,displayLong,serviceNames,serviceNameToBook,servicePriceToBook,displaySpeciality);
							}
                                                }
                                                else
                                                {
                                                        alert("No service centres were found for the selected criteria");
							/*window.location.href = '/index';*/
                                                }

                                        },
                                error: function(ts) { alert("failure") }
                        });
                }
	}

	function remove_duplicates_safe(arr)
	{
		var obj = {};
		var arr2 = [];
		for (var i = 0; i < arr.length; i++)
		{
        		if (!(arr[i] in obj))
			{
            			arr2.push(arr[i]);
            			obj[arr[i]] = true;
        		}
    		}
    		return arr2;
	}

	function displayLocation(location)
	{
		if (parseInt(location.lat) == 0)
		{
			geocoder.geocode( { 'address': location.serviceCentreAddress }, function(results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{
					var marker = new google.maps.Marker(
					{
						map: map,
						position: results[0].geometry.location,
						title: location.name
					});

					google.maps.event.addListener(marker, 'click', function()
					{
						infowindow.content(content);
						infowindow.open(map,marker);
					});
				}
			});
		}
		else
		{
			var position = new google.maps.LatLng(parseFloat(location.serviceCentreLat), parseFloat(location.serviceCentreLong));
			//var icon= 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
			if(RsaOrPuncture == 'puncture')
			{
				var html = "<p style='text-align:left'><b>Name: </b>" + location.serviceCentreName + "<br/><b>Address: </b>" + location.serviceCentreAddress + "</b> <br/><b>Contact: </b>" + location.serviceCentreContact + "<br/></p><hr><b>Price: </b>" + location.serviceCentrePrice + "<p><b>To send your query to this service centre click on the marker.</b></p>";
			}
			else
			{
				var html = "<p style='text-align:left'><b>Name: </b>" + location.serviceCentreName + "<br/><b>Address: </b>" + location.serviceCentreAddress + "</b> <br/><b>Contact: </b>" + location.serviceCentreContact + "</p><hr>To send your query to this service centre click on the marker.";
			}

			var address = location.serviceCentreAddress;
			var name = location.serviceCentreName;
			var contact = location.serviceCentreContact;

			var infoWindow = new google.maps.InfoWindow;
			var marker = new google.maps.Marker(
			{
				map: map,
				position: position,
				//title: location.serviceCentreName, default black patti.
				//icon: icon
			});
			bindInfoWindow(marker, map, infoWindow, html, name, address, contact, RsaOrPuncture);
		}
	}

	function displayLocationServices(displayName,displayAddress,displayContact,displayLat,displayLong,serviceNames,serviceNameToBook,servicePriceToBook,displaySpeciality)
	{
		if (parseInt(location.lat) == 0)
		{
			geocoder.geocode( { 'address': displayAddress }, function(results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{
					var marker = new google.maps.Marker(
					{
						map: map,
						position: results[0].geometry.location,
						title: displayName
					});

					google.maps.event.addListener(marker, 'click', function()
					{
						infowindow.content(content);
						infowindow.open(map,marker);
					});
				}
			});
		}
		else
		{
			//alert(serviceNameToBook+servicePriceToBook);
			var position = new google.maps.LatLng(parseFloat(displayLat), parseFloat(displayLong));
			//var icon= 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
			var html = "<p style='text-align:left'><b>Name: </b>" + displayName + "<br/><b>Address: </b>" + displayAddress + "</b> <br/><b>Contact: </b>" + displayContact + "</p><hr><b>Speciality :</b>" + displaySpeciality + "<hr><b>" + serviceNames + "</b><hr><b>Note:</b> Prices mentioned above <br>are <b>ONLY</b> labour charges.<br>These does not include material cost.";

			var address = displayAddress;
			var name = displayName;
			var contact = displayContact;
			var services = serviceNames;

			var infoWindow = new google.maps.InfoWindow;
			var marker = new google.maps.Marker(
			{
				map: map,
				position: position,
				//title: location.serviceCentreName, default black patti.
				//icon: icon
			});
			bindInfoWindowServices(marker, map, infoWindow, html, name, address, contact, serviceNameToBook, servicePriceToBook);
		}
	}

	function doNothing() {}

	function bindInfoWindow(marker, map, infoWindow, html, name, address, contact, RsaOrPuncture)
	{
		google.maps.event.addListener(marker, 'mouseover', function()
		{
			infoWindow.setContent(html);
			infoWindow.open(map, marker);
			//window.open('/index','_self',false);
		});
		google.maps.event.addListener(marker, 'click', function()
		{
			if(RsaOrPuncture == 'puncture')
			{
				//window.open('/querysms/'+name+'/'+contact+'/P/W','_self',false);
				window.open('/getmessage/'+name+'/'+contact+'/P/W','_self',false);
			}
			else
			{
				//window.open('/querysms/'+name+'/'+contact+'/R/W','_self',false);
				window.open('/getmessage/'+name+'/'+contact+'/R/W','_self',false);
			}
		});
		google.maps.event.addListener(marker, 'mouseout', function()
		{
			infoWindow.close();
		});
	}

        function bindInfoWindowServices(marker, map, infoWindow, html, name, address, contact, serviceNameToBook, servicePriceToBook)
        {	//alert(serviceNameToBook+servicePriceToBook);
                google.maps.event.addListener(marker, 'mouseover', function()
                {
                        infoWindow.setContent(html);
                        infoWindow.open(map, marker);
                        //window.open('/index','_self',false);
                });
                google.maps.event.addListener(marker, 'click', function()
                {	//var services = JSON.stringify(serviceNameToBook);
                        window.open('/bookService?Name='+name+'&Address='+address+'&Contact='+contact+'&service='+services+'','_self',false);
                });
                google.maps.event.addListener(marker, 'mouseout', function()
                {
                        infoWindow.close();
                });
        }
	</script>
   </head>

    <body onload="init();">
        <!-- Top menu -->
	<nav class="navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/index">Doochaki</a>
			</div>
			<div class="collapse navbar-collapse" id="top-navbar-1">
				<img src="assets/img/DoochakiName.png" class="navbar-brand-logo-image"></img>
			</div>
		</div>
	</nav>

        <!-- Slider -->
	<div class="listMapPageLinkDiv row">
		<?php
			if($packageName != '' && $services == '' && $RP == '')
			{
				echo "<a href='/listView?packageName=".$packageName."' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='List View'><i class='fa fa-bars'></i></a>";
			}
                        else if($packageName == '' && $services != '' && $RP == '')
                        {
				$_SESSION['servicesArray'] = $servicesArray;
				echo "<a href='/listView?services=".json_encode($servicesArray)."' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='List View'><i class='fa fa-bars'></i></a>";
                        }
                        else if($packageName == '' && $services == '' && $RP != '')
                        {
				echo "<a href='/listView?RP=".$RP."' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='List View'><i class='fa fa-bars'></i></a>";
                        }
			else
                        {
				echo "<a href='/listView' class='listMapPageLink' data-toggle='tooltip' data-placement='top' title='List View'><i class='fa fa-bars'></i></a>";
                        }
		?>
		<a href="/removesessionvariables" class="removeSessionVariablesLink" data-toggle="tooltip" data-placement="top" title="Change City/Area/Make/Model"><i class="fa fa-pencil"></i></a>
		<?php
			//$userName = 'Subodh';
                	//$userContact = '8793376920';
                	/*if(!isset($userName) && !isset($userContact))
                	{
				echo "<a href='/login' class='listMapPageLoginLink'>Login / SignUP</a>";
			}*/
		?>
	</div>

	<div id="map" style="width: 100%; height: 650px;">
	</div>

	<!-- About Us Modal -->
                <div class="modal fade" id="aboutUsModal" role="dialog">
                        <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content1">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">About Us</h4>
                                        </div>
                                        <div class="modal-body">
                                                <h5><b>
							Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
							Doochaki was started by a team of engineering professionals from Pune, who wanted to devise intelligent solution for a common problem - service center kaha hai?
							Doochaki helps you find a large number of verified vendors in any location. All vendors are vetted by Doochaki and police-verified, to ensure the safety of you and your vehicle. Furthermore, all vendors receive performance scores for the quality and promptness of their services. The aggregate performance scores help you in selecting the best service vendors for you.
                                                </b></h5>
                                        </div>
                                        <div class="modal-footer">
                                                <!--a href="/map" class="btn btn-default">OK</a>-->
                                                <button type="submit" data-dismiss="modal" class="btn btn-default">CLOSE</button>
                                        </div>
                                </div>
                        </div>
		</div>

	<!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 footer-box wow fadeInUp">
                        <h4>About Us</h4>
                        <div class="footer-box-text">
                                <p><b>
                                        Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
                                        Doochaki was started by a team of engineering professionals from Pune, who wanted...
                                </b></p>
                                <p><b><a href="#aboutUsModal" data-toggle="modal">READ MORE...</a></b></p>
                        </div>
                    </div>
                    <!--<div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Email Updates</h4>
                        <div class="footer-box-text footer-box-text-subscribe">
                                <p>Enter your email and you'll be one of the first to get new updates:</p>
                                <form role="form" action="assets/subscribe.php" method="post">
                                        <div class="form-group">
                                                <label class="sr-only" for="subscribe-email">Email address</label>
                                                <input type="text" name="email" placeholder="Email..." class="subscribe-email" id="subscribe-email">
                                        </div>
                                        <button type="submit" class="btn" disabled>Subscribe</button>
                                    </form>
                                    <p class="success-message"></p>
                                    <p class="error-message"></p>
                        </div>
                    </div>-->
                    <div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Contact Us</h4>
                        <div class="footer-box-text footer-box-text-contact">
                                <p><i class="fa fa-map-marker"></i> Address: Pune</p>
                                <p><i class="fa fa-phone"></i> Phone: 8149129955</p>
                                <p><i class="fa fa-envelope"></i> Email: info@doochaki.com</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-12 wow fadeIn">
                                <div class="footer-border"></div>
                        </div>
                </div>
                <!--<div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                       <!--<p></p>
                    </div>
                    <div class="col-sm-5 footer-social wow fadeIn">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>-->
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
