<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="/addservice">Service</a>
      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
<form action="/admindashboard" method="GET">
<div id="cityContainer" class="mdl-shadow--3dp">
	 <div id='searchCity' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                                <input id='searchCityText' class='mdl-textfield__input' type='text' name='key'>
                                <label class='mdl-textfield__label' for='searchCityText'>Search City</label>
	</div>
                                <!-- Accent-colored raised button -->
<button type='Submit' id='searchCityButton' class='mdl-button mdl-js-button mdl-button--raised mdl-color--indigo-600 mdl-button--accent'>
                 <i class="material-icons">search</i>
          </button>
</form>
	<table id = "cityTable" class="mdl-data-table mdl-js-data-table">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">City Name</th>
      <th>Edit</th>
      <th>Deactivate</th>
    </tr>
  </thead>
  <tbody>
<?php

	if(isset($_GET['key']) && !empty($_GET['key']))
	{
		$key = $_GET['key'];
		$url = "http://23.95.95.88/selectcitybykey/$key";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $city = curl_exec($ch);
                curl_close($ch);
		//echo $city;
                $city1 = json_decode($city,true);
                $cityStatus = 'N';
		foreach($city1 as $value => $cityName)
                {
                        echo"<tr>

                        <td>"
                        .$cityName['cityName'].
                        "</td>

                        <td>
                             <a href = '/editcity?cityName=".$cityName['cityName']."&cityId=".$cityName['cityId']."'><i class='material-icons mdl-color-text--indigo-600'>mode_edit</i></a>
                        </td>
                        <td>
                                <a href = '/updatecitystatus/".$cityName['cityId']."/".$cityStatus."'> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a>
                        </td>

                        </tr>";
                }

	}

	else
	{
		$url = "http://23.95.95.88/selectcity";
		$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$city = curl_exec($ch);
		curl_close($ch);
		$city1 = json_decode($city,true);
		$cityStatus = 'N';
		foreach($city1 as $key => $cityName)
		{
			echo"<tr>

			<td>"
			.$cityName['cityName'].
			"</td>

			<td>
		             <a href = '/editcity?cityName=".$cityName['cityName']."&cityId=".$cityName['cityId']."'><i class='material-icons mdl-color-text--indigo-600'>mode_edit</i></a>
        	        </td>
			<td>
                     		<a href = '/updatecitystatus/".$cityName['cityId']."/".$cityStatus."'> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a>
			</td>

			</tr>";
		}
	}
?>
  </tbody>
</table>
</div>
   </div>
  </main>
</div>


</body>
</html>


