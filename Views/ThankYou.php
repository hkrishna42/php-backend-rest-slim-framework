<?php
	$baseUrlObj = new BaseUrl;
	$baseUrl = $baseUrlObj -> baseUrl();
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Doochaki</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href='<?php echo $baseUrl;?>assets/ico/favicon.png'>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href='<?php echo $baseUrl;?>assets/ico/favicon144x144.png'>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href='<?php echo $baseUrl;?>assets/ico/favicon114x114.png'>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href='<?php echo $baseUrl;?>assets/ico/favicon72x72.png'>
        <link rel="apple-touch-icon-precomposed" href='<?php echo $baseUrl;?>assets/ico/favicon57x57.png'>
    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar" role="navigation">
                <div class="container">
                        <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="/index">Doochaki</a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="top-navbar-1">
                                <img src="assets/img/DoochakiName.png" class="navbar-brand-logo-image"></img>
                        </div>
                </div>
        </nav>

	<div class="contact-us-container">
                <div class="container">
                        <div class="row">
				<div class="col-sm-8 col-sm-offset-2 thankyou">
					<h2>Thank you for booking service(s), you will receive a confirmation message from selected service centre<br>
					for your booking.</h2><br><br>
					<a href="/index"><button class="btn btn-default">Back</button></a>
				</div>
			</div>
		</div>
	</div>

	<!-- About Us Modal -->
                <div class="modal fade" id="aboutUsModal" role="dialog">
                        <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content1">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">About Us</h4>
                                        </div>
                                        <div class="modal-body">
                                                <h5><b>
							Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
							Doochaki was started by a team of engineering professionals from Pune, who wanted to devise intelligent solution for a common problem - service center kaha hai?
							Doochaki helps you find a large number of verified vendors in any location. All vendors are vetted by Doochaki and police-verified, to ensure the safety of you and your vehicle. Furthermore, all vendors receive performance scores for the quality and promptness of their services. The aggregate performance scores help you in selecting the best service vendors for you.
                                                </b></h5>
                                        </div>
                                        <div class="modal-footer">
                                                <!--a href="/map" class="btn btn-default">OK</a>-->
                                                <button type="submit" data-dismiss="modal" class="btn btn-default">CLOSE</button>
                                        </div>
                                </div>
                        </div>
                </div>

	<!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 footer-box wow fadeInUp">
                        <h4>About Us</h4>
                        <div class="footer-box-text">
                                <p><b>
                                        Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
                                        Doochaki was started by a team of engineering professionals from Pune, who wanted...
                                </b></p>
                                <p><b><a data-toggle="modal" href="#aboutUsModal">READ MORE...</a></b></p>
                        </div>
                    </div>
                    <!--<div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Email Updates</h4>
                        <div class="footer-box-text footer-box-text-subscribe">
                                <p>Enter your email and you'll be one of the first to get new updates:</p>
                                <form role="form" action="assets/subscribe.php" method="post">
                                        <div class="form-group">
                                                <label class="sr-only" for="subscribe-email">Email address</label>
                                                <input type="text" name="email" placeholder="Email..." class="subscribe-email" id="subscribe-email">
                                        </div>
                                        <button type="submit" class="btn" disabled>Subscribe</button>
                                    </form>
                                    <p class="success-message"></p>
                                    <p class="error-message"></p>
                        </div>
                    </div>-->
                    <div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Contact Us</h4>
                        <div class="footer-box-text footer-box-text-contact">
                                <p><i class="fa fa-map-marker"></i> Address: Pune</p>
                                <p><i class="fa fa-phone"></i> Phone: 8149129955</p>
                                <p><i class="fa fa-envelope"></i> Email: info@doochaki.com</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-12 wow fadeIn">
                                <div class="footer-border"></div>
                        </div>
                </div>
                <!--<div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                       <!--<p></p>
                    </div>
                    <div class="col-sm-5 footer-social wow fadeIn">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>-->
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
