<?php
	$baseUrlObj = new BaseUrl;
	$baseUrl = $baseUrlObj -> baseUrl();
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Doochaki</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/favicon57x57.png">

	<script>
		$(document).ready(function ()
		{
        		document.getElementById('userName').value = '';
       	 		document.getElementById('userEmail').value = '';
        		document.getElementById('userContact').value = '';
		});
	</script>
    </head>

    <body class="loginBody">

        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="">Doochaki</a>
				</div>
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<img src="assets/img/DoochakiName.png" class="navbar-brand-logo-image"></img>
				</div>
			</div>
		</nav>

        <!-- Slider -->
	<div class="slider-2-container">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
				</div>
				<?php
				if(!isset($_POST['btnLoginSubmit']))
				{
					echo "<div class='divForm col-sm-4'>
						<fieldset class='fieldsetForm'>
							<div class='row'>
								<div class='col-sm-6'><a id='btnLogin' onclick='disableSignUp();' class='btnLogin'>Login</a>
								</div>
								<div class='col-sm-6'><a id='btnSignUp' onclick='disableLogin();' class='btnSignUp'>SignUp</a>
								</div>
							</div><br><br><br>
							<form name='Form1' method='POST' action='/login' onsubmit='return checkEmail();'>
								<div class='form-group'>
									<input type='text' id='userName' name='userName' placeholder='Name' onkeypress='return isAlpha(event)' class='form-control' style='visibility:hidden;'><br>
								</div>

								<div class='form-group'>
									<input type='text' id='userContact' name='userContact' placeholder='Contact No.' class='form-control' onkeypress='return isNumber(event)' maxlength='10'><br>
								</div>

								<div class='form-group'>
									<input type='text' id='userEmail' name='userEmail' placeholder='Email' class='form-control' style='visibility:hidden;'><br>
								</div>
								<input type='submit' name='btnLoginSubmit' id='btnLoginSubmit' value = 'SUBMIT' class='btnLoginSubmit' />
							</form>
						</fieldset>
					</div>";
				}
				else
				{
					$url = $baseUrl."requestSms";
					$loginIdentifier = "U";
					$loginFrom = "W";
					$loginType;
					if($userName == null && $userEmail == null && $userContact != null)
					{
						$loginType  = "L";
					}
					else
					{
						$loginType = "S";
					}
                                        $fields = array("userName" => $userName,"userContact" => $userContact,"userEmail" => $userEmail,"loginIdentifier" => $loginIdentifier,"type" => $loginType,"loginForm" => $loginFrom);

					$result = curl_init();
                                        curl_setopt($result, CURLOPT_URL,$url);
                                        curl_setopt($result, CURLOPT_POST, true);
                                        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                        curl_setopt($result, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($result, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($result, CURLOPT_POST, sizeof($fields));
                                        curl_setopt($result, CURLOPT_POSTFIELDS, $fields);
                                        $result1 = curl_exec($result);

                                        $result2 = filter_var($result1, FILTER_VALIDATE_BOOLEAN);
 	                                /*echo json_encode($fields);
                                        echo json_encode($result2);*/

					if($loginType == "S")
					{
						if($result2)
						{
							echo "<script type='text/javascript'>window.location.href = '/verification?contact=".$userContact."';</script>";
						}
						else
						{
							echo "<script type='text/javascript'>alert('Hello ".$userName." you are already signed up. Try login instead.');</script>";
							echo "<script type='text/javascript'>window.location.href = '/login';</script>";
						}
					}
					else
					{
						if($result2)
						{
                                        		echo "<script type='text/javascript'>window.location.href = '/verification?contact=".$userContact."';</script>";
                                        	}
                                        	else
                                        	{
                                        		echo "<script type='text/javascript'>alert('You are not signed up. Please sign up first.');</script>";
                                                	echo "<script type='text/javascript'>window.location.href = '/login';</script>";
                                        	}
					}
				}
				?>

				<div class="col-sm-4">
				</div>
			</div>
		</div>
	</div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>
