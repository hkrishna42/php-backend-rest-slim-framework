<?php
$baseUrlObj = new BaseUrl;
$baseUrl = $baseUrlObj -> baseUrl();
session_start();
if(empty($_SESSION['userName']))
{
        echo "<script type='text/javascript'>alert('Please Login');</script>";
        echo "<script type='text/javascript'>window.location.href ='/AdminLogin';</script>";
}
?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--<meta http-equiv="refresh" content="20">-->
    <title>Doochaki - Add Service Centre</title>
    <link rel="stylesheet" type="text/css" href="admindoochaki/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="admindoochaki/css/layout.css" media="screen" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
     <script src="admindoochaki/js/jquery-1.8.0.min.js" type="text/javascript"></script>
   <script src="admindoochaki/js/script.js" ></script>

<style type="text/css">
table {width:90%;margin-top:10px;}
table, th, td {border-collapse: collapse;}
th, td {padding: 5px;text-align: left; vertical-align:middle;}
table#t01 tr:nth-child(even) {background-color: #eee;}
table#t01 tr:nth-child(odd) {background-color:#fff;}
table#t01 th	{background-color: #2d4956;color: white;}
#label1 {font-size:14px;font-weight:bold; padding:10px;}
#btn1 {width:170px;height:40px;font-size:18px;background-color:#489c22;color:#fff;font-weight:bold;font-family:Arial, Helvetica,sans-serif;border-radius:10px;}


.btn {  font-size: 3vmin;  padding: 0.75em 1.5em;    color: #333; border:none; outline:none;  text-decoration: none;  display: inline;  border-radius: 4px; }
.btn:hover {   -webkit-transition: background-color 1s ease;  -moz-transition: background-color 1s ease;  transition: background-color 1s ease;}
.btn-small {  padding: .75em 1em;  font-size: 0.8em;}
.modal-box {  display: none;  position: absolute;  z-index: 1000;  width: 60%;  background: white;  border-bottom: 1px solid #aaa;  border-radius: 4px;  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);  border: 1px solid rgba(0, 0, 0, 0.1);  background-clip: padding-box;}
@media (min-width: 32em) {
.modal-box { width: 70%; }}
.modal-box header,
.modal-box .modal-header {  padding: 1.25em 1.5em;  border-bottom: 1px solid #ddd;}
.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin: 0; }
.modal-box .modal-body { padding: 2em 1.5em; }
.modal-box footer,
.modal-box .modal-footer {  padding: 1em;  border-top: 1px solid #ddd;  background: rgba(0, 0, 0, 0.02);  text-align: right;}
.modal-overlay {  opacity: 0;  filter: alpha(opacity=0);  position: absolute;  top: 0;  left: 0;  z-index: 900;  width: 100%;  height: 100%;  background: rgba(0, 0, 0, 0.3) !important;}
a.close {  line-height: 1;  font-size: 1.5em;  position: absolute;  top: 5%;  right: 2%;  text-decoration: none;  color: #bbb;}
a.close:hover {  color: #222;  -webkit-transition: color 1s ease;  -moz-transition: color 1s ease;  transition: color 1s ease;}

table {  }
table, th, td  { }
th, td {    padding: 5px;    text-align: left;}
</style>

<!-- File For Ajax -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<script type="text/javascript">
        $(document).ready(function()
        {
                $(".cityName").change(function()
                {
                        var id=$(this).val();
                        var dataString = 'id='+ id;

                        $.ajax
                        ({
                                type: "POST",
                                url: "/getareaweb",
                                data: dataString,
                                cache: false,
                                success: function(html)
                                {
                                        $(".areaName").html(html);
                                }
                        });
                });

        });
</script>

</head>
<body>
<div class="container_12">
	<div class="grid_12 header-repeat">
        	<div id="branding">
                	<div class="floatleft">
				<?php echo"<h1 style='color:white;'>Hello,".$_SESSION['userName']." </h1>";
                         ?>
			</div>
                	<div class="floatright">
                    		<div class="floatleft">
				</div>
                    		<div class="floatleft marginleft10">
                        		<ul class="inline-ul floatleft">
                          			<li><a href="/adminlogout">Logout</a></li>
                        		</ul>
                    	    		<br/>
                    		</div>
                	</div>
                	<div class="clear">
                	</div>
            	</div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">   
        </div>
        <div class="clear">
        </div>
        <div class="grid_2">

		<div class="box sidemenu">
                        <div id='cssmenu'>
            <ul>
		<li class='has-sub'><a href="/AdminAddCity"><span>Dashboard</span></a></li>
                      <li class='has-sub'><a href="#"><span>Add / Update Details</span></a>
                           <ul>
			<li class='active'><a href="/AdminAddCity"><span>City</span></a></li>
                    <li class='has-sub'><a href="/AdminAddArea"><span>Area</span></a></li>
                    <li class='has-sub'><a href="/AdminAddService"><span>Service</span></a></li>
                     <li class='has-sub'><a href="/AdminAddServiceCentre"><span>Service Centre</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleBrand"><span>Vehicle Brand</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleCategory"><span>Vehicle Category</span></a></li>
                    <li class='has-sub'><a href="/AdminAddVehicleModel"><span>Vehicle Model</span></a></li>

                    <!--<li class='has-sub'><a href="addpackage.html"><span>Package</span></a></li>-->

                           </ul>
                      </li>
              <li class='last'><a href="/AdminViewDeactivatedServiceCentre"><span>Activate Details</span></a></li>
                         </ul>
                        </div>
                        <div class="block" id="section-menu">
                        </div>
                </div>
        </div>

  	<div class="grid_10">
   <div class="box round first">
    <h2>Add Service Centre</h2>
   <div class="block1">
      <br>
	<a href="/AdminViewServiceCentre"><input type="button" name="viewServiceCentreButton" id="viewServiceCentreButton" value="View Service Centres"></a>
		<div class="newsletter">
        <form action="/insertservicecentre" method="post" enctype="multipart/form-data" name="addForm" onsubmit="return validateForm()">
  <table width="60%" cellspacing="2" style="margin-left:100px; margin-top:50px;">
   <tr>
    <td width="10%" align="left" valign="top" ><strong style="font-size:14px;"> City :</strong></td>
    <td width="40%" align="left" valign="top" ><select type="text" name="cityName" style="width:25%; float:left;" class="cityName" id="searchCity"><option>-- Search City --</option>
	 <?php
                                                        $url = $baseUrl."selectcity";
                                                        $ch = curl_init();
                                                        curl_setopt($ch, CURLOPT_URL,$url);
                                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                        $city = curl_exec($ch);
                                                        curl_close($ch);
                                                        $city1 = json_decode($city,true);
                                                        foreach($city1 as $key => $cityName)
                                                        {
                                                               echo" <option value=".$cityName['cityName'].">".$cityName['cityName']."</option>";
                                                        }
          ?>

   </select></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Area : </strong></td>
    <td width="40%" align="left" valign="top"><select type="text" name="areaName" style="width:25%; float:left;" class="areaName" id="searchArea"><option>-- Search Area --</option>
              <!--<option>Kothrud</option><option>Katraj</option><option>Swargate</option><option>Pune Station</option>-->
		</select></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Name : </strong></td>
    <td width="40%" align="left" valign="top"><input type="text" name="serviceCentreName" class="location11" style="width:25%; min="0" max="10" float:left;" placeholder="Name" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Contact : </strong></td>
    <td width="40%" align="left" valign="top"><input type="text" name="serviceCentreContact" class="location11" style="width:25%; min="0" maxlength="10" float:left;" placeholder="Contact" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre E-mail : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreEmail" type="text" style="width:25%; float:left;" class="location11" size="50" placeholder="E-mail" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Address : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreAddress" id="serviceCentreAddress" type="text" style="width:25%; float:left;" class="location11" size="50" placeholder="Address" ></td>
   </tr>
   <tr>
   <td><input type="button" onclick="getlatlong()" value="Get LatLong"></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Landmark : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreLandmark" type="text" style="width:25%; float:left;" class="location11" size="50" placeholder="Landmark" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Pincode : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentrePincode" type="text" style="width:25%; float:left;" class="location11" size="50" placeholder="Pincode" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Latitude : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreLat" id="serviceCentreLat" type="text" style="width:25%; float:left;" class="location11" size="100" placeholder="Latitude" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Longitude : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreLong" id="serviceCentreLong" type="text" style="width:25%; float:left;" class="location11" size="100" placeholder="Longitude" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre Speciality : </strong></td>
    <td width="40%" align="left" valign="top"><input name="serviceCentreSpeciality" type="text" style="width:35%; float:left;" class="location11" size="100" placeholder="Speciality" ></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre image 1 : </strong></td>
    <td width="40%" align="left" valign="top"><input type="file" name="serviceCentreImage1" style="width:25%; float:left;" class="location11" size="50"></td>
   </tr>
   <tr>
    <td width="10%" align="left" valign="top"><strong style="font-size:14px;"> Service Centre image 2 : </strong></td>
    <td width="40%" align="left" valign="top"><input type="file" name="serviceCentreImage2" style="width:25%; float:left;" class="location11" size="50" ></td>
   </tr>
</table>
<input type="submit" style="float:left; width:10%; margin-left:15%;" value="Submit" name="submit2" id="addServiceCentreButton">
</form>
<br/>
<!--<a href="servicecentre.html"><input type="submit" style="float:left; width:10%; margin-left:15%;" value="Submit" name="submit2" id="addServiceCentreButton"></a> -->

	   </div>

<script src="/admindoochaki/js/jquery-1.11.1.min.js"></script> 
<script>
function validateForm() {
//alert(Hello);
    var serviceCentreName = document.forms["addForm"]["serviceCentreName"].value;
    var serviceCentreAddress = document.forms["addForm"]["serviceCentreAddress"].value;
    var serviceCentreLandmark = document.forms["addForm"]["serviceCentreLandmark"].value;
    var serviceCentreLatitude = document.forms["addForm"]["serviceCentreLat"].value;
    var serviceCentreLongitude = document.forms["addForm"]["serviceCentreLong"].value;
    var image1 = document.forms["addForm"]["serviceCentreImage1"].value;
    var serviceCentreImage1 = getExt(image1);
    var image2 = document.forms["addForm"]["serviceCentreImage2"].value;
    var serviceCentreImage2 = getExt(image2);
    if (serviceCentreName == "" || serviceCentreName == " "||serviceCentreName == "  ") {
        alert("Service Centre name must be filled out");
        return false;
    }

    /*if(serviceCentreAddress == "" || serviceCentreAddress == " " || serviceCentreAddress == "  "){
	alert("Service Centre address must be filled out");
        return false;

    }

    if(serviceCentreLandmark == "" || serviceCentreLandmark == " " || serviceCentreLandmark == "  "){
        alert("Service Centre Landmark must be filled out");
        return false;

    }

   if(serviceCentreLatitude == "" || serviceCentreLatitude == " " || serviceCentreLatitude == "  "){
        alert("Service Centre Latitude must be filled out");
        return false;

    }

    if(serviceCentreLongitude == "" || serviceCentreLongitude == " " || serviceCentreLongitude == "  "){
        alert("Service Centre Longitude must be filled out");
        return false;

    }

    if (/^\w+([\.-]?\ w+)*@\w+([\.-]?\ w+)*(\.\w{2,3})+$/.test(addForm.serviceCentreEmail.value))
    {
       return true;
    }
    alert("You have entered an invalid email address!");
    return false;

    //var phoneno = /^\d{10}$/;
    if(/^\d{10}$/.test(addForm.serviceCentreContact.value))
    {
      return true;
    }
    alert("You have entered an invalid Contact");
    return false;

    if(/^\d{6}$/.test(addForm.serviceCentrePincode.value))
    {
	return true;
    }
    alert("You have entered an invalid Pincode");
    return false;*/

  if(serviceCentreImage1 == "gif" || serviceCentreImage1 == "jpg" || serviceCentreImage1=="png"){
      return true;}
   alert("Please upload .gif, .jpg and .png files only.");
   //document.getElementById("upload_file").value='';
   return false;

   if(serviceCentreImage2 == "gif" || serviceCentreImage2 == "jpg" || serviceCentreImage2=="png"){
      return true;}
   alert("Please upload .gif, .jpg and .png files only.");
   //document.getElementById("upload_file").value='';
   return false;
}


function getExt(filename) {
   var dot_pos = filename.lastIndexOf(".");
   if(dot_pos == -1)
      return "";
   return filename.substr(dot_pos+1).toLowerCase();
}
</script>
<script type="text/javascript">

// You've written here getPassword()
function getlatlong(){
var address = document.getElementById("serviceCentreAddress").value;
//alert(JSON.stringify(address));
//var url="http://doochaki.com/getlatlong/"+address;
//var xhr = $.get(url);
//xhr.onerror = function()

$.ajax({
        type: "GET",
        url: "http://doochaki.com/getlatlong/"+address,
       //  data:'address='+address,
         success: function(data)
         {

                 //$('#searchArea').html(data);
		alert(data);
		obj = JSON.parse(data);
		var array = obj.split(',');
		document.getElementById("serviceCentreLat").value=array[0];
		document.getElementById("serviceCentreLong").value=array[1];
		//alert(array[0]);

         }
	});



}

</script>

<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});

$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
  			</div>
  		</div>
 	</div>
 	<div class="clear">
	</div>
</div>
<div class="clear">
</div>
<div id="site_info">
	<p>
      		Copyright &copy;<a href="#"> Admin</a> All Rights Reserved.
      	</p>
</div>

<script>
/* $('#searchCity').change(function(){
        var val = $('#searchCity').val();
        //var examName = new Array();
        //examName = val.split(",");
        $.ajax({
        type: "POST",
        url: "http://23.94.62.184/getareaweb",
        data:'cityName='+val,
         success: function(data)
	 {

                 $('#searchArea').html(data);

         }
        //alert(val);
       });
});*/
</script>

</body>
</html>

