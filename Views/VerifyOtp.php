<?php
	//echo $userOtp.$otpFrom.$userContact.$regUsing;
	$registerObj = new Register;
	$verifyOtp = $registerObj -> otpVerification($userOtp,$userContact,$regUsing,$otpFrom);

	if($verifyOtp["success"] == true)
	{
		if($otpFrom == 'M')
		{
			echo json_encode($verifyOtp);
		}
		else
		{
			session_start();
			$_SESSION['userContact'] = $verifyOtp["userContact"];
			if($fromPage == 'I')
			{
				echo "<script type='text/javascript'>window.location.href = '/index';</script>";
			}
			else
			{
				$serviceName = $_SESSION["serviceName"];
				$serviceCentreName = $_SESSION["serviceCentreName"];
				$serviceCentreAddress = $_SESSION["serviceCentreAddress"];
				$serviceCentreContact = $_SESSION["serviceCentreContact"];
				echo "<script type='text/javascript'>window.location.href = '/bookService?Name=".$serviceCentreName."&Address=".$serviceCentreAddress."&Contact=".$serviceCentreContact."&service=".json_encode($serviceName)."';</script>";
			}
		}
	}
	else
	{
		if($otpFrom == 'M')
		{
			echo json_encode($verifyOtp);
		}
		else
		{
			echo "<script type='text/javascript'>alert('Your mobile number is not verified please re-enter mobile number and verify it.');</script>";
			echo "<script type='text/javascript'>window.location.href = '/getusermobiletrial?frPg=".$fromPage."';</script>";
		}
	}
	//echo json_encode($verifyOtp);
?>
