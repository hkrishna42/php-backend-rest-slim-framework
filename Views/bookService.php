<?php

        session_start();
        if(!isset($_SESSION['cityName']) && !isset($_SESSION['areaName']) && !isset($_SESSION['vehicleBrandName']) && !isset($_SESSION['vehicleModelName'])/* && !isset($_SESSION['userName']) && !isset($_SESSION['userContact']) && !isset($_SESSION['userEmail'])*/)
        {
                echo "<script type='text/javascript'>window.location.href = '/index';</script>";
        }
        else
        {
                $cityName = $_SESSION['cityName'];
                $areaName = $_SESSION['areaName'];
                $vehicleBrandName = $_SESSION['vehicleBrandName'];
                $vehicleModelName = $_SESSION['vehicleModelName'];
        }

	if(isset($_GET['Name']) && isset($_GET['Address']) && isset($_GET['Contact']))
	{
		$serviceCentreName = $_GET['Name'];
		$serviceCentreAddress = $_GET['Address'];
		$serviceCentreContact = $_GET['Contact'];
		$serviceName = $_GET['service'];
		$newServicesArray = json_decode($serviceName,true);
		//echo json_encode($newServicesArray);
	}

	if(isset($_SESSION['userName']) && isset($_SESSION['userEmail']) && isset($_SESSION['userContact']))
	{
		$userName = $_SESSION["userName"];
		$userEmail = $_SESSION["userEmail"];
		$userContact = $_SESSION["userContact"];
		$regUsing = $_SESSION["regUsing"];

		unset($_SESSION["serviceName"]);
		unset($_SESSION["serviceCentreName"]);
		unset($_SESSION["serviceCentreAddress"]);
		unset($_SESSION["serviceCentreContact"]);
	}
	else
	{
		$_SESSION["serviceName"] = $newServicesArray;
		$_SESSION["serviceCentreName"] = $serviceCentreName;
		$_SESSION["serviceCentreAddress"] = $serviceCentreAddress;
		$_SESSION["serviceCentreContact"] = $serviceCentreContact;
		echo "<script type='text/javascript'>alert('It Seems you are not logged in. Please login before proceeding.');</script>";
		echo "<script type='text/javascript'>window.location.href = '/login?frPg=B';</script>";

	}

?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	        <title>Doochaki</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/favicon57x57.png">

	<!-- Datepicker -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

	<script>
		function checkBlank()
		{
			var date = document.getElementById('userBookingDate').value;
			var vehicleNumber = document.getElementById('userVehicleNumber').value;

			if(date != '' && vehicleNumber != '')
			{
				return true;
			}
			else
			{
				alert('Please enter vehicle registeration number and date before submitting!');
				return false;
			}
		}
	</script>

	<script>
        $(document).ready(function()
        {
                $('[data-toggle="tooltip"]').tooltip();
        });
        </script>

    </head>

    <body>

        <!-- Top menu -->
	<nav class="navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/index">Doochaki</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="top-navbar-1">
				<img src="assets/img/DoochakiName.png" class="navbar-brand-logo-image"></img>
			</div>
		</div>
	</nav>

        <!-- Form -->
	<div class="contact-us-container">
		<div class="container">
			<div class="row">
				<form role="form" action="/bookingandroid" method="post" onsubmit="return checkBlank()">
					<div class="col-sm-6 contact-form wow fadeInLeft">
						<div class="form-group">
							<label for="userName"><strong>Name</strong></label>
							<input type="text" name="userName" onkeypress='return isAlpha(event)' value='<?php echo $userName; ?>' id="userName">
						</div>

						<div class="form-group">
							<label for="userContact"><strong>Phone</strong></label>
							<input type="text" name="userContact" id="userContact" value='<?php echo $userContact; ?>' readonly>
						</div>

						<div class="form-group">
							<label for="userEmail"><strong>Email</strong></label>
							<input type="text" name="userEmail" id="userEmail" value='<?php echo $userEmail; ?>'>
						</div>

						<div class="form-group">
							<label for="userAddress"><strong>Address</strong></label>
							<input type="text" name="userAddress" placeholder="Address" id="userAddress"><br><br>
						</div>

						<div class="form-group">
							<label for="userLandmark"><strong>Landmark</strong></label>
							<input type="text" name="userLandmark" placeholder="Landmark" id="userLandmark">
						</div>

						<div class="form-group">
							<label for="userPincode"><strong>Pincode</strong></label>
							<input type="text" name="userPincode" placeholder="Pincode" id="userPincode" onkeypress="return isNumber(event)" maxlength="6">
						</div>

						<div class="form-group">
							<label for="userVehicleNumber"><strong>Vehicle Registration Number</strong></label>
							<input type="text" name="userVehicleNumber" placeholder="Vehicle Registration Number" id="userVehicleNumber">
						</div>

					</div>

					<div class="col-sm-6 contact-form wow fadeInRight">
						<div class="form-group">
							<label for="userBookingDate"><strong>Date of Servicing</strong></label><br>
							<input type="text" name="userBookingDate" placeholder="Date of Servicing" class="form-control" id="userBookingDate">
						</div>

						<div class="form-group">
							<label for="userMake"><strong>Make</strong></label>
							<input type="text" name="userMake" placeholder="Make" id="userMake" value='<?php echo $vehicleBrandName; ?>' readonly>
						</div>

						<div class="form-group">
							<label for="userModel"><strong>Model</strong></label>
							<input type="text" name="userModel" placeholder="Model" id="userModel" value='<?php echo $vehicleModelName; ?>' readonly>
						</div>

						<div class="form-group">
							<label for="serviceCentreName"><strong>Service Centre</strong></label>
							<input type="text" name="serviceCentreName" placeholder="Service Centre" id="serviceCentreName" value='<?php echo $serviceCentreName; ?>' readonly>
						</div>

						<div class="form-group">
							<label for="serviceCentreAddress"><strong>Service Centre Address</strong></label><br>
							<input type="text" name="serviceCentreAddress" placeholder="Service Centre Address" id="serviceCentreAddress" value='<?php echo $serviceCentreAddress; ?>' readonly>
						</div>

                                                <div class="form-group">
                                                        <label for="userServices"><strong>Services</strong></label><br>
							<ul>
								<?php
								foreach($newServicesArray['serviceName'] as $service)
								{
									echo "<li>".$service."</li>";
								}
								?>
							</ul>
                                                </div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-12">
									<label style="margin-left:-3%" for="service-mode"><strong>Service Mode</strong></label>
								</div>
								<div class="col-sm-12">
									<div class="radio">
										<label class="servicingModes" data-toggle="tooltip" data-placement="top" title="Pickup">
											<input type="radio" name="userMode" id="PickUp" value="pickup">Pick Up
										</label>
									</div>
									<div class="radio">
										<label class="servicingModes" data-toggle="tooltip" data-placement="top" title="Doorstep">
											<input type="radio" name="userMode" id="Doorstep" value="doorstep">Doorstep
										</label>
									</div>
									<div class="radio">
										<label class="servicingModes" data-toggle="tooltip" data-placement="top" title="Walkin">
											<input type="radio" name="userMode" id="Walkin" value="walkin">Walk In
										</label>
									</div>
								</div>
							</div>
						</div>

						<input type="hidden" name="servicesJsonValue" id="servicesJsonValue" value='<?php echo json_encode($newServicesArray); ?>'>
						<input type="hidden" name="regUsing" id="regUsing" value='<?php echo $regUsing;?>'>
						<input type="hidden" name="userTodayDate" id="userTodayDate" value='<?php echo date("Y/m/d");?>'>
						<input type="hidden" name="bookingFrom" value="W">
						<input type="hidden" name="tag" value="service">
						<input type="hidden" name="serviceCentreContact" id="serviceCentreContact" value='<?php echo $serviceCentreContact;?>'>

						<button type="submit" class="btn btn-default" onclick="press_btn();" name="send_btn" id="send_btn">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- About Us Modal -->
                <div class="modal fade" id="aboutUsModal" role="dialog">
                        <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content1">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">About Us</h4>
                                        </div>
                                        <div class="modal-body">
                                                <h5><b>
							Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
							Doochaki was started by a team of engineering professionals from Pune, who wanted to devise intelligent solution for a common problem - service center kaha hai?
							Doochaki helps you find a large number of verified vendors in any location. All vendors are vetted by Doochaki and police-verified, to ensure the safety of you and your vehicle. Furthermore, all vendors receive performance scores for the quality and promptness of their services. The aggregate performance scores help you in selecting the best service vendors for you.
                                                </b></h5>
                                        </div>
                                        <div class="modal-footer">
                                                <!--a href="/map" class="btn btn-default">OK</a>-->
                                                <button type="submit" data-dismiss="modal" class="btn btn-default">CLOSE</button>
                                        </div>
                                </div>
                        </div>
                </div>

	<!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 footer-box wow fadeInUp">
                        <h4>About Us</h4>
                        <div class="footer-box-text">
                                <p><b>
                                        Doochaki is a vehicle repair and maintenance service aggregator Mobile Application. We help users find vehicle maintenance and support service providers in any location. Users can access our list of verified mechanics and service providers for all their vehicle maintenance needs.
                                        Doochaki was started by a team of engineering professionals from Pune, who wanted...
                                </b></p>
                                <p><b><a data-toggle="modal" href="#aboutUsModal">READ MORE</a></b></p>
                        </div>
                    </div>
                    <!--<div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Email Updates</h4>
                        <div class="footer-box-text footer-box-text-subscribe">
                                <p>Enter your email and you'll be one of the first to get new updates:</p>
                                <form role="form" action="assets/subscribe.php" method="post">
                                        <div class="form-group">
                                                <label class="sr-only" for="subscribe-email">Email address</label>
                                                <input type="text" name="email" placeholder="Email..." class="subscribe-email" id="subscribe-email">
                                        </div>
                                        <button type="submit" class="btn" disabled>Subscribe</button>
                                    </form>
                                    <p class="success-message"></p>
                                    <p class="error-message"></p>
                        </div>
                    </div>-->
                    <div class="col-sm-6 footer-box wow fadeInDown">
                        <h4>Contact Us</h4>
                        <div class="footer-box-text footer-box-text-contact">
                                <p><i class="fa fa-map-marker"></i> Address: Pune</p>
                                <p><i class="fa fa-phone"></i> Phone: 8149129955</p>
                                <p><i class="fa fa-envelope"></i> Email: info@doochaki.com</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-12 wow fadeIn">
                                <div class="footer-border"></div>
                        </div>
                </div>
                <!--<div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                       <!--<p></p>
                    </div>
                    <div class="col-sm-5 footer-social wow fadeIn">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>-->
            </div>
        </footer>

        <!-- Javascript -->
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/retina-1.1.0.min.js"></script>
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<script src="assets/flexslider/jquery.flexslider-min.js"></script>
	<script src="assets/js/jflickrfeed.min.js"></script>
	<script src="assets/js/masonry.pkgd.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="assets/js/jquery.ui.map.min.js"></script>
	<script src="assets/js/scripts.js"></script>
	<!-- Datepicker -->
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<script>
         $(document).ready(function () {
            $( "#userBookingDate" ).datepicker({
		dateFormat:"yy/mm/dd",
		minDate: new Date()
		});
         });
      </script>

	</body>

</html>
