<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="/addservice">Service</a>
      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
<?php
	if(isset($_GET['serviceName']) && isset($_GET['functionality']))
                {
                        $serviceName = $_GET['serviceName'];
                        $serviceId = $_GET['serviceId'];
                        $functionality = $_GET['functionality'];
                        if($functionality == 'edit')
                        {
                                echo" <form action='/updateservice' method='post'>
                                <div id='updateService' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                   <input id='updateServiceText1' class='mdl-textfield__input' type='text' name='serviceName' value='".$serviceName."'>
                                <label class='mdl-textfield__label' for='updateServiceText1'>Service</label>
                                <input type='text' value='".$serviceId."' name='serviceId' hidden>
                                <!-- Accent-colored raised button -->
                                </div>
<button type='Submit' id='updateServiceButton' class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-color--indigo-600'>
  Update
</button>
</form>";}}
        ?>

    </div>
  </main>
</div>


</body>
</html>

