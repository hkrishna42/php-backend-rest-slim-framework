<?php
$areaObject = new Area;
$selectedAreas = $areaObject -> selectArea();

$vehicleModelObject = new VehicleModel;
$selectedVehicleModel = $vehicleModelObject -> selectVehicleModel();

$result = array("cityArea" => $selectedAreas,"brandModel" => $selectedVehicleModel);
echo json_encode($result);
?>
