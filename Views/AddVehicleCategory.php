<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="/addservice">Service</a>
      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>
      <a class="mdl-navigation__link" href="/addvehiclecategory">Vehicle Category</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
	 <?php
                                echo" <form action='/insertvehiclecategory' method='post'>
                                <div id='insertVehicleCategory' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                                <input id='insertVehicleCategoryText' class='mdl-textfield__input' type='text' name='vehicleCategoryName'>
                                <label class='mdl-textfield__label' for='insertVehicleCategoryText'>Vehicle Category</label>
                                <!-- Accent-colored raised button -->
                                </div>
<button type='Submit' id='insertVehicleCategoryButton' class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-color--indigo-600'>
  Add
</button>
</form>";
?>

<table id ="vehicleBrandTable" class="mdl-data-table mdl-js-data-table  mdl-shadow--3dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Vehicle Brand Name</th>
      <th>Edit</th>
      <th>Deactivate</th>
    </tr>
</thead>
  <tbody>
  <?php
$url = "http://23.95.95.88/selectvehiclecategory";
$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
//echo $result;
$vehicleCategory = json_decode($result,true);
foreach($vehicleCategory as $key => $vehicleCategoryName)
{
        //$serviceName1 = str_replace( ' ','%20',$serviceName['serviceName']);
        echo"<tr>

                <td class='mdl-data-table__cell--non-numeric'>"
                .$vehicleCategoryName['vehicleCategoryName'].
                "</td>

                <td>
			<a href = '/editvehiclecategory?vehicleCategoryName=".$vehicleCategoryName['vehicleCategoryName']."&functionality=edit
&vehicleCategoryId=".$vehicleCategoryName['vehicleCategoryId']."'><i class = 'material-icons mdl-color-text--indigo-600'>edit</i></a>
                </td>
                <td>
                     <a href = /admindashboard> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a>
                </td>

        </tr>";
}
?>
  </tbody>
</table>

    </div>
  </main>
</div>


</body>
</html>

