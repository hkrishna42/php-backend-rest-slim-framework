<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="/addservice">Service</a>
      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>
      <a class="mdl-navigation__link" href="/addvehiclecategory">Vehicle Category</a>
      <a class="mdl-navigation__link" href="/addpackage">Package</a>

    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
	<?php
        if(isset($_GET['packageName']) && isset($_GET['functionality']))
                {
                        $packageName = $_GET['packageName'];
			$packagePrice = $_GET['packagePrice'];
			$packageEstimatedPrice = $_GET['packageEstimatedPrice'];
			$packagePeriod = $_GET['packagePeriod'];
			$vehicleCategoryName = $_GET['vehicleCategoryName'];
                        $packageId = $_GET['packageId'];
                        $functionality = $_GET['functionality'];
                        if($functionality == 'edit')
                        {
                                echo" <form action='/updatepackage' method='post'>
                                <div id='updatePackage' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <input id='updatePackageText1' class='mdl-textfield__input' type='text' name='packageName' value='".$packageName."'>
                                <label class='mdl-textfield__label' for='updatePackageText1'>Package</label>
				</div>
				<div id='updatePackage1' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <input id='updatePackageText1' class='mdl-textfield__input' type='text' name='packagePrice' value='".$packagePrice."'>
                                <label class='mdl-textfield__label' for='updatetPackageText1'>Package Price</label>
                                </div>
                                <div id='updatePackage2' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <input id='updatetPackageText2' class='mdl-textfield__input' type='text' name='packageEstimatedPrice' value='".$packageEstimatedPrice."'>
                                <label class='mdl-textfield__label' for='updatePackageText2'>Package Estimated Price</label>
                                </div>
                                <div id='updatePackage3' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <input id='updatePackageText3' class='mdl-textfield__input' type='text' name='packagePeriod' value='".$packagePeriod."' >
                                <label class='mdl-textfield__label' for='updatePackageText3'>Package Period</label>
                                </div>
                                <!-- Accent-colored raised button -->
                                <div id='updatePackageDropdown' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
            <select id='category' class='mdl-textfield__input' name='vehicleCategoryName' value='".$vehicleCategoryName."'>
                 <option></option>";

$url = "http://23.95.95.88/selectvehiclecategory";
$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $vehicleCategory = curl_exec($ch);
                curl_close($ch);
$vehicleCategory1 = json_decode($vehicleCategory,true);
foreach($vehicleCategory1 as $key => $vehicleCategoryName)
{
               echo" <option value=".$vehicleCategoryName['vehicleCategoryName'].">".$vehicleCategoryName['vehicleCategoryName']."</option>";
}
?>
            </select>

            <label class="mdl-textfield__label is-dirty" for="category">Select Vehicle Category</label>
         </div> 

                     <?php          echo "<input type='text' value='".$packageId."' name='packageId' hidden>"; }}?>
                                <!-- Accent-colored raised button -->
                                </div>
<button type='Submit' id='updatePackageButton' class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-color--indigo-600'>
  Update
</button>
</form>

    </div>
  </main>
</div>


</body>
</html>

