<?php
$servicesObject = new Services;
$gcmObject = new GCM;
$addOnsObject = new AddOns;
$data = json_decode($addOnsArray,true);

$transactionServiceId = $data["transactionServiceId"];

$transactionObject = new Transaction;
$userId = $transactionObject -> selectUserIdByTransaction($transactionServiceId);

foreach($data['serviceName'] as $service)
{
	//$serviceName = $service;
	$serviceId = $servicesObject -> selectServicesId($service);

	$insertAddOns = $addOnsObject -> insertAddOns($transactionServiceId,$serviceId);
}

if($insertAddOns)
{

	 $message = "AddOns recieved! Please check the AddOns section in application";
         $tag1 = "addons";
         $title = "Doochaki";
         $gcmId = $gcmObject -> selectGcmIdFromUserId($userId,"U");

         foreach($gcmId as $key => $registrationId)
         {
         	$gcmObject -> sendNotification($message,$tag1,$title,$registrationId["userDeviceGcmId"]);
         }

	$response["success"] = true;
	echo json_encode($response);
}
else
{
	$response["success"] = false;
	echo json_encode($response);
}

?>
