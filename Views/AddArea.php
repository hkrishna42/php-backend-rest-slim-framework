<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Doochaki Admin</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href = "mdl/style.css">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.indigo-pink.min.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"> </script>
</head>
<body>
<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--blue-grey">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Doochaki</span>
      <!-- Add spacer, to align navigation to the right -->
     </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Title</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/admindashboard">City</a>
      <a class="mdl-navigation__link" href="/addarea">Area</a>
      <a class="mdl-navigation__link" href="/addservice">Service</a>
      <a class="mdl-navigation__link" href="/addvehiclebrand">Vehicle Brand</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
	<form action ="/insertarea" method="POST" class="">
	   <div id='cityDropdown' class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <select id="city" class="mdl-textfield__input" name="cityName">
		 <?php
$url = "http://23.95.95.88/selectcity";
$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $city = curl_exec($ch);
                curl_close($ch);
$city1 = json_decode($city,true);
foreach($city1 as $key => $cityName)
{
               echo" <option value=".$cityName['cityName'].">".$cityName['cityName']."</option>";
}
?>
            </select>

            <label class="mdl-textfield__label is-dirty" for="example">Select City</label>
         </div>
	<button type='Submit' id='addAreaButton' class='mdl-button mdl-js-button mdl-button--raised mdl-color--indigo-600 mdl-button--accent'>
                 Add
                </button>
	<div id='autoArea' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                                <input  class='mdl-textfield__input' type='text' id='search-box' name='areaName'>
                                <label class='mdl-textfield__label' for='sample3'>Search Area</label>
				<div id='suggesstion-box'></div>
		</div>


				<!--<div id='suggesstion-box'></div>-->

	</form>

<div id="areaContainer" class="mdl-shadow--3dp">

<form action="/addarea" method="GET">
<div id='searchAreaBy' class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <select id="searchBy" class="mdl-textfield__input" name="searchAreaBy">
		<option value=""></option>
               <option value="Area">Area</option>
	       <option value="City">City</option>

            </select>
	<label class="mdl-textfield__label is-dirty" for="example">Search By</label>
         </div>
<div id='searchArea' class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label'>
                                <input id='searchAreaText' class='mdl-textfield__input' type='text' name='key'>
                                <label class='mdl-textfield__label' for='searchAreaText'>Search Area</label>
        </div>
                                <!-- Accent-colored raised button -->
<button type='Submit' id='searchAreaButton' class='mdl-button mdl-js-button mdl-button--raised mdl-color--indigo-600 mdl-button--accent'>
                 <i class="material-icons">search</i>
          </button>
</form>
     <table id="areaTable" class="mdl-data-table mdl-js-data-table">
  	<thead>
    		<tr>
	      	<th class="mdl-data-table__cell--non-numeric">Area Name</th>
	     	<th class="mdl-data-table__cell--non-numeric">City Name</th>
      		<th>Edit</th>
	      	<th>Deactivate</th>
   		</tr>
  	</thead>
  	<tbody>
		<?php
			if(isset($_GET['key']) && isset($_GET['searchAreaBy']) && !empty($_GET['key']) && !empty($_GET['searchAreaBy']))
			{

					$key = $_GET['key'];
					$searchAreaBy = $_GET['searchAreaBy'];
					$url = "http://23.95.95.88/selectareabykey/$key/$searchAreaBy";
	                                $ch = curl_init();
        	                        curl_setopt($ch, CURLOPT_URL,$url);
                	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        	        $result = curl_exec($ch);
                                	curl_close($ch);
					//echo $result;
	                                $area = json_decode($result,true);
        	                        foreach($area as $key => $areaArray)
                	                {
                        	                echo"<tr>
                                	        <td class='mdl-data-table__cell--non-numeric'>".$areaArray['areaName']."</td>
                                        	<td>".$areaArray['cityName']."</td>
	                                        <td><a href = '/editarea?cityName=".$areaArray['cityName']."&functionality=edit&areaName=".$areaArray['areaName']."&areaId=".$areaArray['areaId']."'> <i class = 'material-icons mdl-color-text--indigo-600'>edit</i></a></td>
        	                                <td><a href = /admindashboard> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a></td>
                	                        </tr>";
					}

			}
			else
			{
				$url = "http://23.95.95.88/selectarea";
				$ch = curl_init();
	        	        curl_setopt($ch, CURLOPT_URL,$url);
	                	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		                $result = curl_exec($ch);
	        	        curl_close($ch);
				$area = json_decode($result,true);
				foreach($area as $key => $areaArray)
				{
					echo"<tr>
					<td class='mdl-data-table__cell--non-numeric'>".$areaArray['areaName']."</td>
					<td>".$areaArray['cityName']."</td>
					<td><a href = '/editarea?cityName=".$areaArray['cityName']."&functionality=edit&areaName=".$areaArray['areaName']."&areaId=".$areaArray['areaId']."'> <i class = 'material-icons mdl-color-text--indigo-600'>edit</i></a></td>
					<td><a href = /admindashboard> <i class = 'material-icons mdl-color-text--indigo-600'>delete</i></a></td>
					</tr>";
				}
			}
		?>
	  </tbody>
       </table>
</div>
   </div>
  </main>
</div>



<script>

$(document).ready(function(){
//alert('Hello');
	$("#search-box").keyup(function(){
		var city = document.getElementById('city').value; 
		var term = document.getElementById('search-box').value;
		$.ajax({
		type: "POST",
		url: "http://23.95.95.88:80/getarea",

		data:{term : term, cityName : city},

		/*beforeSend: function(){
			alert('hello');
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},*/

		success: function(data){

			$("#suggesstion-box").show();

			$("#suggesstion-box").html(data);

			$("#search-box").css("background","#FFF");
			//alert(data);
		}

		});
		//window.alert(type);
	});

});



function selectCountry(val) {
//alert('hello');
$("#search-box").val(val);
$("#suggesstion-box").hide();

}

</script>
<script>
$(document).ready(function () {
    $("select").change(function () {
        if ($('#' + $(this).attr("id") + ' option:selected').val() === '') {
            $(this).parent('div').removeClass('is-dirty');
        } else {
            $(this).parent('div').addClass('is-dirty');
        }
     });
});

</script>


</body>
</html>
