<?php
	if($regUsing == "F")
	{
		$registerObject = new Register;
		$userId = $registerObject->selectUserIdFromFBId($userIdentity);
	}
	else
	{
		$registerObject = new Register;
                $userId = $registerObject->selectUserIdFromEmail($userIdentity);
	}
	$response = array();
	$gcmObject = new GCM;
	$checkIfUserGcmExist = $gcmObject -> checkIfUserGcmExist($userDeviceGcmId,$userDeviceImeiNo,$userId,$userGcmType);

	if($checkIfUserGcmExist)
	{
		$response["status"] = "remains same";
	}
	else
	{
		$ifGCMIDChanged = $gcmObject -> selectUserGcmId($userDeviceGcmId,$userDeviceImeiNo,$userId,$userGcmType);
		if($ifGCMIDChanged)
		{
			$updateUserGcmId = $gcmObject -> updateUserGcmId($userDeviceGcmId,$userDeviceImeiNo,$userId,$userGcmType);
			if($updateUserGcmId)
			{
				$response["status"] = "update successfull";
			}
			else
			{
				$response["status"] = "update failed";

			}
		}
		else
		{
			$insertUserGcmId = $gcmObject -> insertUserGcmId($userDeviceName,$userDeviceGcmId,$userDeviceImeiNo,$userId,$userGcmType);
			if($insertUserGcmId)
			{
				$response["status"] = "insert successfull";
			}
			else
			{
				$response["status"] = "insert failed";
			}
		}
	}
echo json_encode($response);
?>
