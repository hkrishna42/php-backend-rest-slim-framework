
<?php
/***** for shopperzbay *****/
$app->post('/getIndoorMapData', function() use ($app)
{
        $mallName = (null !== $app->request->post('mallName'))?$app->request->post('mallName'):'';
        $app->render('getIndoorMapData.php', array(
        'mallName' => $mallName
    ));

});

//**********************API******************//
/***** Get Transaction Service Id for Add Ons *****/
$app->post('/currentbookings', function() use ($app)
{
        $token = (null !== $app->request->post('token'))?$app->request->post('token'):'';
        $app->render('CurrentBookings.php', array(
        'token' => $token
    ));

});

/***** Get Transaction Service Id for Add Ons *****/
$app->post('/transactions', function() use ($app)
{
        $token = (null !== $app->request->post('token'))?$app->request->post('token'):'';
        $app->render('UserHistory.php', array(
        'token' => $token
    ));

});

$app->post('/selectareafromcity', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('SelectAreaFromCity.php', array(
        'cityName' => $cityName
    ));

});

//Send User Problem Message To Service Centre
$app->post('/querysms', function() use ($app)
{
        $serviceCentreName = (null !== $app->request->post('serviceCentreName'))?$app->request->post('serviceCentreName'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
	$RP = (null !== $app->request->post('RP'))?$app->request->post('RP'):'';
	$MW = (null !== $app->request->post('MW'))?$app->request->post('MW'):'';
	$messageFrom = (null !== $app->request->post('messageFrom'))?$app->request->post('messageFrom'):'';
	$message = (null !== $app->request->post('message'))?$app->request->post('message'):'';
        $app->render('QuerySms.php', array(
        'serviceCentreName' => $serviceCentreName,
        'serviceCentreContact' => $serviceCentreContact,
	'RP' => $RP,
	'MW' => $MW,
	'messageFrom' => $messageFrom,
	'message' => $message
    ));

});



//Service Centre Login
$app->post('/servicecentrelogin', function() use ($app)
{
        $serviceCentreName = (null !== $app->request->post('serviceCentreName'))?$app->request->post('serviceCentreName'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('ServiceCentreLogin.php', array(
        'serviceCentreName' => $serviceCentreName,
        'serviceCentreContact' => $serviceCentreContact
    ));

});

//Service Centre Otp Verification
$app->post('/servicecentreverifyotp', function() use ($app)
{
        $loginOtp = (null !== $app->request->post('loginOtp'))?$app->request->post('loginOtp'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('ServiceCentreVerifyOtp.php', array(
        'loginOtp' => $loginOtp,
        'serviceCentreContact' => $serviceCentreContact
    ));

});


//User Login Using Email
$app->post('/userlogin', function() use ($app)
{
        $userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
        $userPassword = (null !== $app->request->post('userPassword'))?$app->request->post('userPassword'):'';
	$loginFrom = (null !== $app->request->post('loginFrom'))?$app->request->post('loginFrom'):'';
	$fromPage = (null !== $app->request->post('fromPage'))?$app->request->post('fromPage'):'';
        $app->render('UserLogin.php', array(
        'userEmail' => $userEmail,
        'userPassword' => $userPassword,
	'loginFrom' => $loginFrom,
	'fromPage' => $fromPage
    ));

});

//Send otp for user mobile
$app->post('/getusermobiletrial', function() use ($app)
{
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
	$userFBId = (null !== $app->request->post('userFBId'))?$app->request->post('userFBId'):'';
	$fromPage = (null !== $app->request->post('fromPage'))?$app->request->post('fromPage'):'';
        $app->render('GetUserMobileTrial.php', array(
        'userContact' => $userContact,
        'userEmail' => $userEmail,
	'userFBId' => $userFBId,
	'fromPage' => $fromPage
    ));

});

//verify otp for user mobile
$app->post('/verifyuserotp', function() use ($app)
{
        $userOtp = (null !== $app->request->post('userOtp'))?$app->request->post('userOtp'):'';
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
	$otpFrom = (null !== $app->request->post('otpFrom'))?$app->request->post('otpFrom'):'';
	$regUsing = (null !== $app->request->post('regUsing'))?$app->request->post('regUsing'):'';
	$fromPage = (null !== $app->request->post('fromPage'))?$app->request->post('fromPage'):'';
        $app->render('VerifyOtp.php', array(
        'userOtp' => $userOtp,
        'userContact' => $userContact,
	'otpFrom' => $otpFrom,
	'regUsing' => $regUsing,
	'fromPage' => $fromPage
    ));

});

//request Sms for Login
/*$app->post('/requestSms', function() use ($app)
{
	$userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
    	$userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
	$userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
	$loginIdentifier = (null !== $app->request->post('loginIdentifier'))?$app->request->post('loginIdentifier'):'';
	$type = (null !== $app->request->post('type'))?$app->request->post('type'):'';
	$loginFrom = (null !== $app->request->post('loginFrom'))?$app->request->post('loginFrom'):'';
	$app->render('RequestSms.php', array(
	'userName' => $userName,
	'userContact' => $userContact,
	'userEmail' => $userEmail,
	'loginIdentifier' => $loginIdentifier,
	'type' => $type,
	'loginFrom' => $loginFrom
    ));

});*/

//Register User Email
$app->post('/registeruseremail', function() use ($app)
{
        $userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
        $userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
	$userPassword = (null !== $app->request->post('userPassword'))?$app->request->post('userPassword'):'';
        $regFrom = (null !== $app->request->post('regFrom'))?$app->request->post('regFrom'):'';
	$fromPage = (null !== $app->request->post('fromPage'))?$app->request->post('fromPage'):'';
        $app->render('RegisterUserEmail.php', array(
        'userName' => $userName,
        'userEmail' => $userEmail,
	'userPassword' => $userPassword,
        'regFrom' => $regFrom,
	'fromPage' => $fromPage
    ));

});

//Register FB details
$app->post('/registeruserfbcredentials', function() use ($app)
{
        $userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
        $userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
        $userFBId = (null !== $app->request->post('userFBId'))?$app->request->post('userFBId'):'';
        $regFrom = (null !== $app->request->post('regFrom'))?$app->request->post('regFrom'):'';
        $app->render('RegisterUserFbCredentials.php', array(
        'userName' => $userName,
        'userEmail' => $userEmail,
        'userFBId' => $userFBId,
        'regFrom' => $regFrom
    ));

});

//Register User Contact
$app->post('/registerusercontact', function() use ($app)
{
        $userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
	$userFBId = (null !== $app->request->post('userFBId'))?$app->request->post('userFBId'):'';
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $app->render('RegisterUserContact.php', array(
        'userEmail' => $userEmail,
	'userFBId' => $userFBId,
        'userContact' => $userContact
    ));

});

//otp verification
/*$app->post('/verifyOtp', function() use ($app)
{
        //$userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        //$type = (null !== $app->request->post('$type'))?$app->request->post('$type'):'';
        $loginOtp = (null !== $app->request->post('loginOtp'))?$app->request->post('loginOtp'):'';
        $app->render('VerifyOtp.php', array(
        //'userName' => $userName,
        'userContact' => $userContact,
        //'type' => $type,
        'loginOtp' => $loginOtp
    ));

});*/

//get list of areas for web based on the cityname
$app->post('/getareaweb', function() use ($app)
{
	$id = (null !== $app->request->post('id'))?$app->request->post('id'):'';
	$app->render('GetAreaWeb.php', array(
	'id' => $id
    ));

});

//Get Transaction Id on user history page
$app->post('/transactions', function() use ($app)
{
        $app->render('UserHistory.php', array(
    ));

});

//get user history of transactions
$app->post('/getuserhistory', function() use ($app)
{
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $app->render('GetUserHistory.php', array(
        'userContact' => $userContact
    ));

});

//get list of models for web based on the brandname
$app->post('/getvehiclemodelweb', function() use ($app)
{
        $id = (null !== $app->request->post('id'))?$app->request->post('id'):'';
        $app->render('GetVehicleModelWeb.php', array(
        'id' => $id
    ));

});

//get areawise and modelwise list of packages for web based on the package
$app->post('/getpackageweb', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
        $vehicleModelName = (null !== $app->request->post('vehicleModelName'))?$app->request->post('vehicleModelName'):'';
        $app->render('GetPackageWeb.php', array(
        'cityName' => $cityName,
        'areaName' => $areaName,
        'vehicleBrandName' => $vehicleBrandName,
        'vehicleModelName' => $vehicleModelName
    ));
});

//get list of services for web based on the package
$app->post('/getservicesofpackageweb', function() use ($app)
{
        $packageId = (null !== $app->request->post('packageId'))?$app->request->post('packageId'):'';
        $app->render('GetServicesOfPackageWeb.php', array(
        'packageId' => $packageId
    ));

});

$app->post('/getuserbookingdetails', function() use ($app)
{
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $app->render('GetUserBookingDetails.php', array(
        'userContact' => $userContact
    ));

});

//To store City, Area, Make and Model in session
$app->post('/index', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
	$vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
	$vehicleModelName = (null !== $app->request->post('vehicleModelName'))?$app->request->post('vehicleModelName'):'';
        $app->render('SessionVariables.php', array(
        'cityName' => $cityName,
	'areaName' => $areaName,
	'vehicleBrandName' => $vehicleBrandName,
	'vehicleModelName' => $vehicleModelName
    ));

});


//GCM Registration
$app->post('/gcmregister', function() use ($app)
{
        $userIdentity = (null !== $app->request->post('userIdentity'))?$app->request->post('userIdentity'):'';
        $regUsing = (null !== $app->request->post('regUsing'))?$app->request->post('regUsing'):'';
        $userDeviceImeiNo = (null !== $app->request->post('userDeviceImeiNo'))?$app->request->post('userDeviceImeiNo'):'';
        $userDeviceGcmId = (null !== $app->request->post('userDeviceGcmId'))?$app->request->post('userDeviceGcmId'):'';
	$userDeviceName = (null !== $app->request->post('userDeviceName'))?$app->request->post('userDeviceName'):'';
	$userGcmType = (null !== $app->request->post('userGcmType'))?$app->request->post('userGcmType'):'';
        $app->render('GcmRegister.php', array(

        'userIdentity' => $userIdentity,
        'regUsing' => $regUsing,
        'userDeviceImeiNo' => $userDeviceImeiNo,
        'userDeviceGcmId' =>  $userDeviceGcmId,
	'userDeviceName' => $userDeviceName,
	'userGcmType' => $userGcmType
    ));

});

//GCM Register Service Centre

$app->post('/gcmregisterservicecentre', function() use ($app)
{
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        //$regUsing = (null !== $app->request->post('regUsing'))?$app->request->post('regUsing'):'';
        $userDeviceImeiNo = (null !== $app->request->post('userDeviceImeiNo'))?$app->request->post('userDeviceImeiNo'):'';
        $userDeviceGcmId = (null !== $app->request->post('userDeviceGcmId'))?$app->request->post('userDeviceGcmId'):'';
        $userDeviceName = (null !== $app->request->post('userDeviceName'))?$app->request->post('userDeviceName'):'';
        $userGcmType = (null !== $app->request->post('userGcmType'))?$app->request->post('userGcmType'):'';
        $app->render('GcmRegisterServiceCentre.php', array(
	'userContact' => $userContact,
        //'userIdentity' => $userIdentity,
        //'regUsing' => $regUsing,
        'userDeviceImeiNo' => $userDeviceImeiNo,
        'userDeviceGcmId' =>  $userDeviceGcmId,
        'userDeviceName' => $userDeviceName,
        'userGcmType' => $userGcmType
    ));

});


//********************Website****************//
$app->post('/login', function() use ($app)
{
    	$userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
	$userEmail = (null !== $app->request->post('userEmail'))?$app->request->post('userEmail'):'';
	$userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
	$app->render('login.php', array(
        'userName' => $userName,
        'userEmail' => $userEmail,
        'userContact' => $userContact
    ));

});

$app->post('/verification', function() use ($app)
{
        $loginOtp = (null !== $app->request->post('loginOtp'))?$app->request->post('loginOtp'):'';
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $app->render('verification.php', array(
        'loginOtp' => $loginOtp,
        'userContact' => $userContact
    ));

});

/*//To store City, Area, Make and Model in session
$app->post('/map', function() use ($app)
{
	$RSAOrPuncture = (null !== $app->request->post('RSAOrPuncture'))?$app->request->post('RSAOrPuncture'):'';
        $app->render('map.php', array(
	'RSAOrPuncture' => $RSAOrPuncture
    ));

});*/



//****************************Admin Panel**********************//
$app->post('/checkAdminLogin', function() use ($app)
{
        $userName = (null !== $app->request->post('userName'))?$app->request->post('userName'):'';
        $userPassword = (null !== $app->request->post('userPassword'))?$app->request->post('userPassword'):'';
	//echo $userName;
	$adminObject = new Admin;
	$checkAdminExist = $adminObject->selectAdminUser($userName,$userPassword);
	if($checkAdminExist)
	{
		session_start();
		$_SESSION["userName"] = $userName;
                $_SESSION["userPassword"] = $userPassword;
		echo "<script type='text/javascript'>alert('Login Successfull');</script>";
                echo "<script type='text/javascript'>window.location.href ='/AdminAddCity';</script>";

	}
	else
	{
		echo "<script type='text/javascript'>alert('Login Failed Please Try Again');</script>";
                echo "<script type='text/javascript'>window.location.href ='/AdminLogin';</script>";
	}

});

$app->post('/insertcity', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('insertCity.php', array(
        'cityName' => $cityName
         ));

});

$app->post('/updatecity', function() use ($app)
{
        $cityId = (null !== $app->request->post('cityId'))?$app->request->post('cityId'):'';
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('UpdateCity.php', array(
        'cityId' => $cityId,
        'cityName' => $cityName
    ));

});

$app->post('/updatearea', function() use ($app)
{
        $areaId = (null !== $app->request->post('areaId'))?$app->request->post('areaId'):'';
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdateArea.php', array(
        'areaId' => $areaId,
        'cityName' => $cityName,
	'areaName' => $areaName
    ));

});

$app->post('/getarea', function() use ($app)
{
        $term = (null !== $app->request->post('term'))?$app->request->post('term'):'';
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('GetArea.php', array(
        'term' => $term,
        'cityName' => $cityName
    ));

});

$app->post('/insertarea', function() use ($app)
{
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('insertArea.php', array(
        'areaName' => $areaName,
        'cityName' => $cityName
    ));

});

$app->post('/insertservice', function() use ($app)
{
        $serviceName = (null !== $app->request->post('serviceName'))?$app->request->post('serviceName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('InsertService.php', array(
        'serviceName' => $serviceName
         ));

});



$app->post('/updateservice', function() use ($app)
{
	$serviceId = (null !== $app->request->post('serviceId'))?$app->request->post('serviceId'):'';
        $serviceName = (null !== $app->request->post('serviceName'))?$app->request->post('serviceName'):'';
        //$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdateService.php', array(
        'serviceId' => $serviceId,
        'serviceName' => $serviceName
    ));

});

$app->post('/insertvehiclebrand', function() use ($app)
{
        $vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('InsertVehicleBrand.php', array(
        'vehicleBrandName' => $vehicleBrandName
         ));

});

$app->post('/updatevehiclebrand', function() use ($app)
{
        $vehicleBrandId = (null !== $app->request->post('vehicleBrandId'))?$app->request->post('vehicleBrandId'):'';
        $vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
        //$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdateVehicleBrand.php', array(
        'vehicleBrandId' => $vehicleBrandId,
        'vehicleBrandName' => $vehicleBrandName
    ));

});

$app->post('/insertvehiclecategory', function() use ($app)
{
        $vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('InsertVehicleCategory.php', array(
        'vehicleCategoryName' => $vehicleCategoryName
         ));

});

$app->post('/updatevehiclecategory', function() use ($app)
{
        $vehicleCategoryId = (null !== $app->request->post('vehicleCategoryId'))?$app->request->post('vehicleCategoryId'):'';
        $vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        //$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdateVehicleCategory.php', array(
        'vehicleCategoryId' => $vehicleCategoryId,
        'vehicleCategoryName' => $vehicleCategoryName
    ));

});

$app->post('/updateservicecentre', function() use ($app)
{
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $cityName= (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$serviceCentreName=(null !== $app->request->post('serviceCentreName'))?$app->request->post('serviceCentreName'):'';
	$serviceCentreContact=(null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
	$serviceCentreEmail=(null !== $app->request->post('serviceCentreEmail'))?$app->request->post('serviceCentreEmail'):'';
	$serviceCentreAddress=(null !== $app->request->post('serviceCentreAddress'))?$app->request->post('serviceCentreAddress'):'';
	$serviceCentreLandmark=(null !== $app->request->post('serviceCentreLandmark'))?$app->request->post('serviceCentreLandmark'):'';
	$serviceCentrePincode=(null !== $app->request->post('serviceCentrePincode'))?$app->request->post('serviceCentrePincode'):'';
	$serviceCentreLat=(null !== $app->request->post('serviceCentreLat'))?$app->request->post('serviceCentreLat'):'';
	$serviceCentreLong=(null !== $app->request->post('serviceCentreLong'))?$app->request->post('serviceCentreLong'):'';
	$serviceCentreSpeciality=(null !== $app->request->post('serviceCentreSpeciality'))?$app->request->post('serviceCentreSpeciality'):'';
	$serviceCentreId=(null !== $app->request->post('serviceCentreId'))?$app->request->post('serviceCentreId'):'';
        $app->render('UpdateServiceCentre.php', array(
        'areaName' => $areaName,
        'cityName' => $cityName,
	'serviceCentreName'=>$serviceCentreName,
	'serviceCentreContact'=>$serviceCentreContact,
	'serviceCentreEmail'=>$serviceCentreEmail,
	'serviceCentreAddress'=>$serviceCentreAddress,
	'serviceCentreLandmark'=>$serviceCentreLandmark,
	'serviceCentrePincode'=>$serviceCentrePincode,
	'serviceCentreLat'=>$serviceCentreLat,
	'serviceCentreLong'=>$serviceCentreLong,
	'serviceCentreSpeciality'=>$serviceCentreSpeciality,
	'serviceCentreId'=>$serviceCentreId
    ));

});

$app->post('/insertvehiclemodel', function() use ($app)
{
        $vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
	$vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
	$vehicleModelName = (null !== $app->request->post('vehicleModelName'))?$app->request->post('vehicleModelName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $app->render('InsertVehicleModel.php', array(
	        'vehicleCategoryName' => $vehicleCategoryName,
		'vehicleBrandName' => $vehicleBrandName,
		'vehicleModelName' => $vehicleModelName
         ));

});

$app->post('/updatevehiclemodel', function() use ($app)
{
        $vehicleModelId = (null !== $app->request->post('vehicleModelId'))?$app->request->post('vehicleModelId'):'';
        $vehicleModelName = (null !== $app->request->post('vehicleModelName'))?$app->request->post('vehicleModelName'):'';
	$vehicleBrandName = (null !== $app->request->post('vehicleBrandName'))?$app->request->post('vehicleBrandName'):'';
	$vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        //$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdateVehicleModel.php', array(
        'vehicleModelId' => $vehicleModelId,
	'vehicleModelName' => $vehicleModelName,
	'vehicleBrandName' => $vehicleBrandName,
        'vehicleCategoryName' => $vehicleCategoryName
    ));

});

$app->post('/insertpackage', function() use ($app)
{
        $packageName = (null !== $app->request->post('packageName'))?$app->request->post('packageName'):'';
        //$cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$packagePrice = (null !== $app->request->post('packagePrice'))?$app->request->post('packagePrice'):'';
	$packageEstimatedPrice = (null !== $app->request->post('packageEstimatedPrice'))?$app->request->post('packageEstimatedPrice'):'';
	$packagePeriod = (null !== $app->request->post('packagePeriod'))?$app->request->post('packagePeriod'):'';
	$vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        $app->render('InsertPackage.php', array(
        'packageName' => $packageName,
	'packagePrice' => $packagePrice,
	'packageEstimatedPrice' => $packageEstimatedPrice,
	'packagePeriod' => $packagePeriod,
	'vehicleCategoryName' => $vehicleCategoryName
         ));

});

$app->post('/updatepackage', function() use ($app)
{
        $packageId = (null !== $app->request->post('packageId'))?$app->request->post('packageId'):'';
        $packageName = (null !== $app->request->post('packageName'))?$app->request->post('packageName'):'';
        //$areaName= (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('UpdatePackage.php', array(
        'packageId' => $packageId,
        'packageName' => $packageName
    ));

});

$app->post('/insertservicecentre', function() use ($app)
{
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
	$serviceCentreName = (null !== $app->request->post('serviceCentreName'))?$app->request->post('serviceCentreName'):'';
	$serviceCentreEmail = (null !== $app->request->post('serviceCentreEmail'))?$app->request->post('serviceCentreEmail'):'';
	$serviceCentreAddress = (null !== $app->request->post('serviceCentreAddress'))?$app->request->post('serviceCentreAddress'):'';
	$serviceCentreLandmark = (null !== $app->request->post('serviceCentreLandmark'))?$app->request->post('serviceCentreLandmark'):'';
	$serviceCentrePincode = (null !== $app->request->post('serviceCentrePincode'))?$app->request->post('serviceCentrePincode'):'';
	$serviceCentreLat = (null !== $app->request->post('serviceCentreLat'))?$app->request->post('serviceCentreLat'):'';
	$serviceCentreLong = (null !== $app->request->post('serviceCentreLong'))?$app->request->post('serviceCentreLong'):'';
	$serviceCentreSpeciality = (null !== $app->request->post('serviceCentreSpeciality'))?$app->request->post('serviceCentreSpeciality'):'';
	$serviceCentreImage1 = (null !== $app->request->post('serviceCentreImage1'))?$app->request->post('serviceCentreImage1'):'';
	$serviceCentreImage2  = (null !== $app->request->post('serviceCentreImage2'))?$app->request->post('serviceCentreImage2'):'';

	

	if($_FILES['serviceCentreImage1']['error'] === UPLOAD_ERR_OK && $_FILES['serviceCentreImage2']['error'] === UPLOAD_ERR_OK)
        {
        	$target_dir = "/var/www/html/images/";
	        $target_file = $target_dir . basename($_FILES["serviceCentreImage1"]["name"]);
		$target_file1 = $target_dir . basename($_FILES["serviceCentreImage2"]["name"]);
	        $uploadOk = 1;
        	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	        // Check if image file is a actual image or fake
                $check = getimagesize($_FILES["serviceCentreImage1"]["tmp_name"]);
		$check1 = getimagesize($_FILES["serviceCentreImage2"]["tmp_name"]);
                if($check !== false && $check1 !== false)
                {
                        $uploadOk = 1;
                        move_uploaded_file($_FILES["serviceCentreImage1"]["tmp_name"], $target_file);
			move_uploaded_file($_FILES["serviceCentreImage2"]["tmp_name"], $target_file);
                        $serviceCentreImage1 = basename($_FILES["serviceCentreImage1"]["name"]);
			$serviceCentreImage2 = basename($_FILES["serviceCentreImage2"]["name"]);
                 }
                 else
                 {
                        echo "File is not an image.";
                        $uploadOk = 0;
                 }
        }

	//$serviceCentreStatus = (null !== $app->request->post('serviceCentreStatus'))?$app->request->post('serviceCentreStatus'):'';
        $app->render('InsertServiceCentre.php', array(
        'areaName' => $areaName,
        'cityName' => $cityName,
	'serviceCentreContact' => $serviceCentreContact,
	'serviceCentreName' => $serviceCentreName,
	'serviceCentreEmail' =>$serviceCentreEmail,
	'serviceCentreAddress' => $serviceCentreAddress,
	'serviceCentreLandmark'=>$serviceCentreLandmark,
	'serviceCentrePincode'=>$serviceCentrePincode,
	'serviceCentreLat'=>$serviceCentreLat,
	'serviceCentreLong'=> $serviceCentreLong,
	'serviceCentreSpeciality'=>$serviceCentreSpeciality,
	//'serviceCentreImage1'=> $serviceCentreImage1,
	//'serviceCentreStatus'=>$serviceCentreStatus,
	//'serviceCentreImage2'=>$serviceCentreImage2
    ));

});

$app->post('/addservicesandroid', function() use ($app)
{
        $jsonValueServices = (null !== $app->request->post('jsonValueServices'))?$app->request->post('jsonValueServices'):'';
	$serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
	$vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        $app->render('AddServicesAndroid.php', array(
        'jsonValueServices' => $jsonValueServices,
	'serviceCentreContact' => $serviceCentreContact,
        'vehicleCategoryName' => $vehicleCategoryName
    ));

});

$app->post('/getservicecentresrsaandroid', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
	$makeName = (null !== $app->request->post('makeName'))?$app->request->post('makeName'):'';
        $modelName = (null !== $app->request->post('modelName'))?$app->request->post('modelName'):'';
        $app->render('GetServiceCentresRSAAndroid.php', array(
        'cityName' => $cityName,
        'areaName' => $areaName,
	'makeName' => $makeName,
	'modelName' => $modelName
    ));

});

$app->post('/getservicecentrespunctureandroid', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
        $areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $makeName = (null !== $app->request->post('makeName'))?$app->request->post('makeName'):'';
        $modelName = (null !== $app->request->post('modelName'))?$app->request->post('modelName'):'';
        $app->render('GetServiceCentresPunctureAndroid.php', array(
        'cityName' => $cityName,
        'areaName' => $areaName,
        'makeName' => $makeName,
        'modelName' => $modelName
    ));

});

$app->post('/getservicecentresservicesandroid', function() use ($app)
{
        $jsonValueServices = (null !== $app->request->post('jsonValueServices'))?$app->request->post('jsonValueServices'):'';
        /*$app->render('GetServiceCentresServicesAndroid.php', array(
        'jsonValueServices' => $jsonValueServices
    ));*/
	//echo $jsonValueServices;
	$data = json_decode($jsonValueServices,true);
	//echo json_encode($data);
	$vehicleBrandName = $data['make'];
	$vehicleModelName = $data['model'];
	$cityName = $data['city'];
	$areaName = $data['area'];

	$cityObject = new City;
	$cityId = $cityObject -> selectCityId($cityName);

	$areaObject = new Area;
	$areaId = $areaObject -> selectAreaId($cityId,$areaName);

	$vehicleBrandObject = new VehicleBrand;
	$vehicleBrandId = $vehicleBrandObject -> selectVehicleBrandId($vehicleBrandName);

	$vehicleModelObject = new VehicleModel;
	$vehicleModelId = $vehicleModelObject -> selectVehicleModelId($vehicleModelName,$vehicleBrandId);

	$vehicleCategoryObject = new VehicleCategory;
	$vehicleCategoryId = $vehicleCategoryObject -> selectVehicleCategoryIdByModelId($vehicleModelId);

	$serviceObject = new Services;
	$intersect = array();
	$servicesPrice = array();
	$serviceCentrePrice = array();
	$servicesName = array();
	foreach($data['serviceName'] as $service)
	{
		//$serviceObject = new Services;
		$serviceCentreId = array();
		$serviceId = $serviceObject -> selectServicesId($service);
		$query=mysql_query("select serviceCentreId,servicesAtServiceCentrePrice from
		tblServicesAtServiceCentre where vehicleCategoryId =
		'$vehicleCategoryId' and serviceId = '$serviceId'");
		if(mysql_num_rows($query))
		{
			while($row = mysql_fetch_assoc($query))
			{
				$serviceCentreId[] = $row['serviceCentreId'];
				$serviceCentrePrice[] = $row['servicesAtServiceCentrePrice'];
				//$serviceName[] = $service;
			}
		}
		if(empty($serviceCentreId))
		{
			$intersect[] = "-1";
		}
		else
		{
			if(!empty($intersect))
			{
				$intersect = array_intersect($intersect,$serviceCentreId);
				//echo json_encode($intersect);
			}
			else
			{
				$intersect = $serviceCentreId;
			}
		}
	}

	$newServiceCentreId = array();
	$resultNew = mysql_query("select serviceCentreId from tblServiceCentre where areaId = '$areaId'");
	while($rowNew = mysql_fetch_assoc($resultNew))
	{
		$newServiceCentreId[] = $rowNew['serviceCentreId'];
	}

	$intersectNew = array_intersect($newServiceCentreId,$intersect);

	$serviceCentreDetails = array();
	foreach($intersectNew as $serviceCentreIdNew)
	{
		$result1 = mysql_query("select serviceCentreName,
		serviceCentreAddress,serviceCentreContact,
		serviceCentreLat,serviceCentreLong,serviceCentreSpeciality
		from tblServiceCentre where serviceCentreId = '$serviceCentreIdNew' and serviceCentreStatus = 'Y'");

		while($row1 = mysql_fetch_assoc($result1))
		{
			foreach($data['serviceName']  as $service)
			{
				$serviceCentreDetails[] = $row1;
			}
		}

		foreach($data['serviceName'] as $service)
		{
			$servicesName[] = $service;
		}
	}

	$servicesPrice = array();
	foreach($intersectNew as $serviceCentreIdNew)
	{
		foreach($data['serviceName'] as $service)
		{
			$serviceId = $serviceObject -> selectServicesId($service);
			$result = mysql_query("select servicesAtServiceCentrePrice
			from tblServicesAtServiceCentre
			where serviceCentreId = '$serviceCentreIdNew'
			and serviceId = '$serviceId'");
			$row = mysql_fetch_array($result);

			$servicesPrice[] = $row[0];
		}
	}


	//$finalResponse = array();
	$finalResponse = array("serviceCentreDetails" => $serviceCentreDetails, "services" => $servicesName, "servicesPrice" => $servicesPrice);
	//$finalResponse[] = array("servicePrice" => $serviceCentrePrice['servicesAtServiceCentrePrice'],"serviceCentreName" => $serviceCentreDetails['serviceCentreName'],"serviceCentreAddress" => $serviceCentreDetails['serviceCentreAddress'],"serviceCentreContact" => $serviceCentreDetails['serviceCentreContact'],"serviceCentreLat" => $serviceCentreDetails['serviceCentreLat'],"serviceCentreLong" => $serviceCentreDetails['serviceCentreLong'],"serviceCentrePrice" => $servicePrice);
	//return $intersect;
	echo json_encode($finalResponse);

});

$app->post('/getservicecentrespackagesandroid', function() use ($app)
{
        $cityName = (null !== $app->request->post('cityName'))?$app->request->post('cityName'):'';
	$areaName = (null !== $app->request->post('areaName'))?$app->request->post('areaName'):'';
        $app->render('GetServiceCentresPackagesAndroid.php', array(
        'cityName' => $cityName,
	'areaName' => $areaName
    ));

});

$app->post('/getpackageandroid', function() use ($app)
{
        $makeName = (null !== $app->request->post('makeName'))?$app->request->post('makeName'):'';
        $modelName = (null !== $app->request->post('modelName'))?$app->request->post('modelName'):'';
        $app->render('getPackageAndroid.php', array(
        'makeName' => $makeName,
        'modelName' => $modelName
    ));

});

$app->post('/bookingandroid', function() use ($app)
{
        $userContact = (null !== $app->request->post('userContact'))?$app->request->post('userContact'):'';
        $userAddress = (null !== $app->request->post('userAddress'))?$app->request->post('userAddress'):'';
        $userLandmark = (null !== $app->request->post('userLandmark'))?$app->request->post('userLandmark'):'';
        $userPincode = (null !== $app->request->post('userPincode'))?$app->request->post('userPincode'):'';
        $userMake = (null !== $app->request->post('userMake'))?$app->request->post('userMake'):'';
        $userModel = (null !== $app->request->post('userModel'))?$app->request->post('userModel'):'';
        $userVehicleNumber = (null !== $app->request->post('userVehicleNumber'))?$app->request->post('userVehicleNumber'):'';
        $userMode = (null !== $app->request->post('userMode'))?$app->request->post('userMode'):'';
        $userBookingDate = (null !== $app->request->post('userBookingDate'))?$app->request->post('userBookingDate'):'';
        $userTodayDate = (null !== $app->request->post('userTodayDate'))?$app->request->post('userTodayDate'):'';
        $serviceCentreName = (null !== $app->request->post('serviceCentreName'))?$app->request->post('serviceCentreName'):'';
        $serviceCentreAddress = (null !== $app->request->post('serviceCentreAddress'))?$app->request->post('serviceCentreAddress'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $servicesJsonValue = (null !== $app->request->post('servicesJsonValue'))?$app->request->post('servicesJsonValue'):'';
        $packageName = (null !== $app->request->post('packageName'))?$app->request->post('packageName'):'';
        $packagePrice = (null !== $app->request->post('packagePrice'))?$app->request->post('packagePrice'):'';
        $regUsing = (null !== $app->request->post('regUsing'))?$app->request->post('regUsing'):'';
	$tag = (null !== $app->request->post('tag'))?$app->request->post('tag'):'';
	$bookingFrom = (null !== $app->request->post('bookingFrom'))?$app->request->post('bookingFrom'):'';
        $app->render('BookingAndroid.php', array(
	'userContact' => $userContact,
	'userAddress' => $userAddress,
	'userLandmark' => $userLandmark,
	'userPincode' => $userPincode,
	'userMake' => $userMake,
	'userModel' => $userModel,
	'userVehicleNumber' => $userVehicleNumber,
	'userMode' => $userMode,
	'userBookingDate' => $userBookingDate,
	'userTodayDate' => $userTodayDate,
	'serviceCentreName' => $serviceCentreName,
	'serviceCentreAddress' => $serviceCentreAddress,
	'serviceCentreContact' => $serviceCentreContact,
	'servicesJsonValue' => $servicesJsonValue,
	'packageName' => $packageName,
	'packagePrice' => $packagePrice,
	'regUsing' => $regUsing,
	'tag' => $tag,
	'bookingFrom' => $bookingFrom
        ));
});

/********** Service End **********/
// Add Employee
$app->post('/addemployee', function() use ($app)
{
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $employeeName = (null !== $app->request->post('employeeName'))?$app->request->post('employeeName'):'';
	$employeeContact = (null !== $app->request->post('employeeContact'))?$app->request->post('employeeContact'):'';
        $app->render('AddEmployee.php', array(
        'serviceCentreContact' => $serviceCentreContact,
	'employeeName' => $employeeName,
        'employeeContact' => $employeeContact
    ));

});

$app->post('/removeemployee', function() use ($app)
{
        //$serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        //$employeeName = (null !== $app->request->post('employeeName'))?$app->request->post('employeeName'):'';
        $employeeContact = (null !== $app->request->post('employeeContact'))?$app->request->post('employeeContact'):'';
        $app->render('RemoveEmployee.php', array(
        'employeeContact' => $employeeContact
    ));

});

$app->post('/selectemployee', function() use ($app)
{
        //$serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        //$employeeName = (null !== $app->request->post('employeeName'))?$app->request->post('employeeName'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('SelectEmployee.php', array(
        'serviceCentreContact' => $serviceCentreContact
    ));

});

$app->post('/getbookingdetails', function() use ($app)
{
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('GetBookingDetails.php', array(
        'serviceCentreContact' => $serviceCentreContact
    ));

});

$app->post('/getconfirmedbookingdetails', function() use ($app)
{
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('GetConfirmedBookingDetails.php', array(
        'serviceCentreContact' => $serviceCentreContact
    ));

});

$app->post('/getbookingservices', function() use ($app)
{
        $transactionId = (null !== $app->request->post('transactionId'))?$app->request->post('transactionId'):'';
        $app->render('GetBookingServices.php', array(
        'transactionId' => $transactionId
    ));

});

$app->post('/getservicesatservicecentre', function() use ($app)
{
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
	$vehicleModelName = (null !== $app->request->post('vehicleModelName'))?$app->request->post('vehicleModelName'):'';
	$type = (null !== $app->request->post('type'))?$app->request->post('type'):'';
	$transactionServiceId = (null !== $app->request->post('transactionServiceId'))?$app->request->post('transactionServiceId'):'';
        $app->render('GetServicesAtServiceCentre.php', array(
        'serviceCentreContact' => $serviceCentreContact,
	'vehicleModelName' => $vehicleModelName,
	'transactionServiceId' => $transactionServiceId,
	'type' => $type
    ));

});

$app->post('/updatetransactionservice', function() use ($app)
{
        $transactionServiceId =(null !== $app->request->post('transactionServiceId'))?$app->request->post('transactionServiceId'):'';
	$employeeContact = (null !== $app->request->post('employeeContact'))?$app->request->post('employeeContact'):'';
	$transactionServicePickUpTime = (null !== $app->request->post('transactionServicePickUpTime'))?$app->request->post('transactionServicePickUpTime'):'';
	$transactionServiceDropTime = (null !== $app->request->post('transactionServiceDropTime'))?$app->request->post('transactionServiceDropTime'):'';

        $app->render('UpdateTransactionService.php', array(
        'transactionServiceId' => $transactionServiceId,
	'employeeContact' => $employeeContact,
	'transactionServicePickUpTime' => $transactionServicePickUpTime,
	'transactionServiceDropTime' => $transactionServiceDropTime
    ));

});


$app->post('/insertaddons', function() use ($app)
{
	$addOnsArray = (null !== $app->request->post('addOnsArray'))?$app->request->post('addOnsArray'):'';
        $app->render('InsertAddOns.php', array(
	'addOnsArray' => $addOnsArray
    ));

});

//get user history of transactions
$app->post('/getselectaddons', function() use ($app)
{
        $transactionServiceId = (null !== $app->request->post('transactionServiceId'))?$app->request->post('transactionServiceId'):'';
        $app->render('SelectAddOns.php', array(
        'transactionServiceId' => $transactionServiceId
    ));

});

//update add ons
$app->post('/updateaddons', function() use ($app)
{
	$addOnsFrom = (null !== $app->request->post('addOnsFrom'))?$app->request->post('addOnsFrom'):'';
	$serviceName = (null !== $app->request->post('serviceName'))?$app->request->post('serviceName'):'';
	$transactionServiceId = (null !== $app->request->post('transactionServiceId'))?$app->request->post('transactionServiceId'):'';
       	$data = (null !== $app->request->post('data'))?$app->request->post('data'):'';
        $app->render('UpdateAddOns.php', array(
	'data' => $data,
	'addOnsFrom' => $addOnsFrom,
	'serviceName' => $serviceName,
	'transactionServiceId' => $transactionServiceId
    ));
});

//Request New Service
$app->post('/requestservice', function() use ($app)
{
        $serviceName = (null !== $app->request->post('serviceName'))?$app->request->post('serviceName'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $vehicleCategoryName = (null !== $app->request->post('vehicleCategoryName'))?$app->request->post('vehicleCategoryName'):'';
        $servicePrice = (null !== $app->request->post('servicePrice'))?$app->request->post('servicePrice'):'';
        $app->render('RequestService.php', array(
        'serviceName' => $serviceName,
        'serviceCentreContact' => $serviceCentreContact,
        'vehicleCategoryName' => $vehicleCategoryName,
        'servicePrice' => $servicePrice
    ));

});

//Update Services at Service Centre
$app->post('/updateservices', function() use ($app)
{
        $jsonValue = (null !== $app->request->post('jsonValue'))?$app->request->post('jsonValue'):'';
        $serviceCentreContact = (null !== $app->request->post('serviceCentreContact'))?$app->request->post('serviceCentreContact'):'';
        $app->render('UpdateServicePrice.php', array(
        'jsonValue' => $jsonValue,
        'serviceCentreContact' => $serviceCentreContact
    ));

});


?>
