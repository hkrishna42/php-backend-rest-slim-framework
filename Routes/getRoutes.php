<?php
/*$app->get('/ghyaData', function() use ($app)
{
    $app->render('GETDATA.php');
});*/

$app->get('/', function() use ($app)
{
    $app->render('index.php');
});
$app->get('/adminlogout', function() use ($app)
{
    $app->render('RevokeSession.php');
});

//******************Map************************//
$app->get('/map', function() use ($app)
{
    $app->render('map.php');
});

$app->get('/temp', function() use ($app)
{
    $app->render('temp.php');
});

$app->get('/listView', function() use ($app)
{
    $app->render('listView.php');
});

$app->get('/get_locations', function() use ($app)
{
    $app->render('get_locations.php');
});
$app->get('/getlatlong/:address', function($address) use ($app)
{
    $app->render('GetLatLong.php',array('address' => $address));
});

$app->get('/bookService', function() use ($app)
{
    $app->render('bookService.php');
});

$app->get('/thankyou', function() use ($app)
{
    $app->render('ThankYou.php');
});

/******************** All Types Of SMS **************************/
//Send sms for user query of Puncture Or RSA
$app->get('/getmessage/:serviceCentreName/:serviceCentreContact/:RP/:MW', function($serviceCentreName,$serviceCentreContact,$RP,$MW) use ($app)
{
    $app->render('GetMessage.php', array(
		'serviceCentreName' => $serviceCentreName,
		'serviceCentreContact' => $serviceCentreContact,
		'RP' => $RP,
		'MW' => $MW
	));
});

/********************Logout**************************/
$app->get('/logout', function() use ($app)
{
    $app->render('Logout.php');
});

/********************Login**************************/
//login page for the website
$app->get('/login', function() use ($app)
{
    $app->render('loginTrial.php');
});

$app->get('/loginCallBack', function() use ($app){

    $app->render('LoginCallBack.php');
});

//Email Verification Of User
$app->get('/verifyuser/:userEmailVerificationCode', function($userEmailVerificationCode) use ($app)
{
    $app->render('VerifyEmail.php', array('userEmailVerificationCode' => $userEmailVerificationCode));
});

//Get User's Mobile
$app->get('/getusermobiletrial', function() use ($app)
{
    $app->render('GetUserMobileTrial.php');
});

$app->get('/mailtrial', function() use ($app)
{
    //$app->render('GetUserMobileTrial.php');

$result = mail("harikrishnan.nair.42@gmail.com","A Subject Here","Hi there,\nThis email was sent using PHP's mail function.");
echo $result;
//print "Email successfully sent";
//else
//print "An error occured";


});


////////////////////////////////////////////////////////////////////////////////////

$app->get('/requestsms', function() use ($app)
{
    $app->render('RequestSms.php');
});

//otp verification page
$app->get('/verification', function() use ($app)
{
    $app->render('verification.php');
});

$app->get('/index', function() use ($app)
{
    $app->render('index.php');
});

$app->get('/services', function() use ($app)
{
    $app->render('services.php');
});

/*********************AdminPanel*********************/

//admin dashboard landing page
$app->get('/admindashboard', function() use ($app)
{
    $app->render('AdminDashboard.php');
});
$app->get('/AdminAddCity', function() use ($app)
{
    $app->render('AdminAddCity.php');
});
$app->get('/AdminEditCity', function() use ($app)
{
    $app->render('AdminEditCity.php');
});

//admin dashboard edit city
$app->get('/editcity', function() use ($app)
{
    $app->render('EditCity.php');
});

$app->get('/AdminEditArea', function() use ($app)
{
    $app->render('AdminEditArea.php');
});
//edit area
$app->get('/editarea', function() use ($app)
{
    $app->render('EditArea.php');
});

//Add Area
$app->get('/addarea', function() use ($app)
{
    $app->render('AddArea.php');
});
$app->get('/AdminAddArea', function() use ($app)
{
    $app->render('AdminAddArea.php');
});

//Add Services
$app->get('/addservice', function() use ($app)
{
    $app->render('AddService.php');
});

$app->get('/AdminAddService', function() use ($app)
{
    $app->render('AdminAddService.php');
});

//edit service
$app->get('/AdminEditService', function() use ($app)
{
    $app->render('AdminEditService.php');
});

$app->get('/editservice', function() use ($app)
{
    $app->render('EditService.php');
});

//edit Service Centre
$app->get('/AdminEditServiceCentre', function() use ($app)
{
    $app->render('AdminEditServiceCentre.php');
});

//add vehicle brand
$app->get('/addvehiclebrand', function() use ($app)
{
    $app->render('AddVehicleBrand.php');
});

$app->get('/AdminAddVehicleBrand', function() use ($app)
{
    $app->render('AdminAddVehicleBrand.php');
});

//edit vehicle brand
$app->get('/editvehiclebrand', function() use ($app)
{
    $app->render('EditVehicleBrand.php');
});

$app->get('/AdminEditVehicleBrand', function() use ($app)
{
    $app->render('AdminEditVehicleBrand.php');
});

//add vehicle category
$app->get('/addvehiclecategory', function() use ($app)
{
    $app->render('AddVehicleCategory.php');
});

$app->get('/AdminAddVehicleCategory', function() use ($app)
{
    $app->render('AdminAddVehicleCategory.php');
});

$app->get('/AdminAddServiceCentre', function() use ($app)
{
    $app->render('AdminAddServiceCentre.php');
});

$app->get('/AdminViewServiceCentre', function() use ($app)
{
    $app->render('AdminViewServiceCentre.php');
});

$app->get('/AdminViewDeactivatedServiceCentre', function() use ($app)
{
    $app->render('AdminViewDeactivatedServiceCentre.php');
});

//edit vehicle category
$app->get('/editvehiclecategory', function() use ($app)
{
    $app->render('EditVehicleCategory.php');
});

$app->get('/AdminEditVehicleCategory', function() use ($app)
{
    $app->render('AdminEditVehicleCategory.php');
});

//add vehicle model
$app->get('/AdminAddVehicleModel', function() use ($app)
{
    $app->render('AdminAddVehicleModel.php');
});
//edit vehicle model
$app->get('/AdminEditVehicleModel', function() use ($app)
{
    $app->render('AdminEditVehicleModel.php');
});

//add package
$app->get('/addpackage', function() use ($app)
{
    $app->render('AddPackage.php');
});
//edit package
$app->get('/editpackage', function() use ($app)
{
    $app->render('EditPackage.php');
});

//Action on Requested Service
$app->get('/adminactionrequestedservice/:requestedServiceId/:action', function($requestServiceId,$action) use ($app)
{
    $app->render('AdminActionRequestedService.php', array('requestedServiceId' => $requestedServiceId,'action' => $action));
});

$app->get('/updatecitystatus/:cityId/:cityStatus', function($cityId,$cityStatus) use ($app)
{
    $app->render('UpdateCityStatus.php', array('cityId' => $cityId,'cityStatus' => $cityStatus));
});

$app->get('/updateareastatus/:areaId/:areaStatus', function($areaId,$areaStatus) use ($app)
{
    $app->render('UpdateAreaStatus.php', array('areaId' => $areaId,'areaStatus' => $areaStatus));
});

$app->get('/updateservicestatus/:serviceId/:serviceStatus', function($serviceId,$serviceStatus) use ($app)
{
    $app->render('UpdateServiceStatus.php', array('serviceId' => $serviceId,'serviceStatus' => $serviceStatus));
});

$app->get('/updateservicecentrestatus/:servicecentreId/:servicecentreStatus', function($serviceCentreId,$serviceCentreStatus) use ($app)
{
    $app->render('UpdateServiceCentreStatus.php', array('serviceCentreId' => $serviceCentreId,'serviceCentreStatus' => $serviceCentreStatus));
});

$app->get('/updatevehiclecategorystatus/:vehicleCategoryId/:vehicleCatgeoryStatus', function($vehicleCategoryId,$vehicleCatgeoryStatus) use ($app)
{
    $app->render('UpdateVehicleCatgeoryStatus.php', array('vehicleCatgeoryId' => $vehicleCatgeoryId,'vehicleCatgeoryStatus' => $vehicleCatgeoryStatus));
});

$app->get('/updatevehiclebrandstatus/:vehicleBrandId/:vehicleBrandStatus', function($vehicleBrandId,$vehicleBrandStatus) use ($app)
{
    $app->render('UpdateVehicleBrandStatus.php', array('vehicleBrandId' => $vehicleBrandId,'vehicleBrandStatus' => $vehicleBrandStatus));
});
$app->get('/updatevehiclemodelstatus/:vehicleModelId/:vehicleModelStatus', function($vehicleModelId,$vehicleModelStatus) use ($app)
{
    $app->render('UpdateVehicleModelStatus.php', array('vehicleModelId' => $vehicleModelId,'vehicleModelStatus' => $vehicleModelStatus));
});



/************************API**********************/

//test notification

$app->get('/testnotif', function() use ($app)
{
    $app->render('TestNotif.php');
});

//Select City
$app->get('/selectcity', function() use ($app)
{
    $app->render('SelectCity.php');
});
//Select City By Key
$app->get('/selectcitybykey/:key', function($key) use ($app)
{
    $app->render('SelectCityByKey.php',array('key' => $key));
});
$app->get('/selectareabykey/:key/:searchAreaBy', function($key,$searchAreaBy) use ($app)
{
    $app->render('SelectAreaByKey.php',array('key' => $key,'searchAreaBy'=>$searchAreaBy));
});
$app->get('/selectservicebykey/:key', function($key) use ($app)
{
    $app->render('SelectServiceByKey.php',array('key' => $key));
});
$app->get('/selectvehiclebrandbykey/:key', function($key) use ($app)
{
    $app->render('SelectVehicleBrandByKey.php',array('key' => $key));
});
$app->get('/selectvehiclecategorybykey/:key', function($key) use ($app)
{
    $app->render('SelectVehicleCategoryByKey.php',array('key' => $key));
});

$app->get('/selectvehiclemodelbykey/:key', function($key) use ($app)
{
    $app->render('SelectVehicleModelByKey.php',array('key' => $key));
});
//select Area
$app->get('/selectarea', function() use ($app)
{
	$app->render('SelectArea.php');
});
//Select Service
$app->get('/selectservice', function() use ($app)
{
        $app->render('SelectService.php');
});
//Select Service Centre
$app->get('/selectservicecentre', function() use ($app)
{
        $app->render('SelectServiceCentre.php');
});
//deactivated Service Centre
$app->get('/selectdeactiveservicecentre', function() use ($app)
{
        $app->render('SelectDeactiveServiceCentre.php');
});
//Select Vehicle Brand
$app->get('/selectvehiclebrand', function() use ($app)
{
        $app->render('SelectVehicleBrand.php');
});
//Select Vehicle Brand By Key

//Select Vehicle Category
$app->get('/selectvehiclecategory', function() use ($app)
{
        $app->render('SelectVehicleCategory.php');
});
//Select Vehicle Model
$app->get('/selectvehiclemodel', function() use ($app)
{
        $app->render('SelectVehicleModel.php');
});


//Select Package
$app->get('/selectpackage', function() use ($app)
{
        $app->render('SelectPackage.php');
});

//Get List of Area
$app->get('/getarea', function() use ($app)
{
    $app->render('GetArea.php');
});

//Get filters for android
$app->get('/getfilterandroid', function() use ($app)
{
    $app->render('getFilterAndroid.php');
});

//Get filters for android
$app->get('/getpackageandroid', function() use ($app)
{
    $app->render('getPackageAndroid.php');
});

//Get City List for Web
$app->get('/getcityweb', function() use ($app)
{
    $app->render('GetCityWeb.php');
});

//Get Vehicle Brand List for Web
$app->get('/getvehiclebrandweb', function() use ($app)
{
    $app->render('GetVehicleBrandWeb.php');
});

//Get Service List for Web and Android
$app->get('/getservicewebandroid', function() use ($app)
{
    $app->render('GetServiceWebAndroid.php');
});

//Get Only Package List for Web
/*$app->get('/getpackageweb', function() use ($app)
{
    $app->render('GetPackageWeb.php');
});*/

//Get Vehicle Category List for Android
$app->get('/getvehiclecategoryandroid', function() use ($app)
{
    $app->render('GetVehicleCategoryAndroid.php');
});

//Remove Session variables for Web
$app->get('/removesessionvariables', function() use ($app)
{
    $app->render('RemoveSessionVariables.php');
});

//Direct to transaction page
$app->get('/transactions', function() use ($app)
{
    $app->render('UserHistory.php');
});

//Direct to Current Bookings page
$app->get('/currentbookings', function() use ($app)
{
    $app->render('CurrentBookings.php');
});

//Show new requested services
$app->get('/showrequestedservices', function() use ($app)
{
    $app->render('ShowRequestedServices.php');
});
// Display on admin panel requested services
$app->get('/requestedservices', function() use ($app)
{
    $app->render('RequestedServices.php');
});


//update the selected addons
$app->get('/updateaddons', function() use ($app)
{
    $app->render('UpdateAddOns.php');
});

/*****************Admin Login*******************/
$app->get('/AdminLogin', function() use ($app)
{
    $app->render('AdminLogin.php');
});
/*****************Check Admin Credentials*******/


?>
